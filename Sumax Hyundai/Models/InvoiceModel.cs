﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sumax.Models
{
    public class InvoiceModel
    {
        public string FileName { get; set; }
        public string PDFFile { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime ProcessDate { get; set; }
        public string Status { get; set; }
        public Division division { get; set; }
        public InvoiceHeader invoiceHeader { get; set; }
        public Address address { get; set; }
        public MCoreProduct mCoreProduct { get; set; }
        public List<BodyData> bodyData { get; set; }
        public Footer footer { get; set; }
        public LastSection lastSection { get; set; }
    }
    public class Division
    {
        public string Division_Address { get; set; }
        public string Division_Address_1 { get; set; }
        public string Division_State_Name { get; set; }
        public string Division_State_Code { get; set; }
        public string Divivsion_Email { get; set; }
        public string Division_Gstin_No { get; set; }

    }
    public class InvoiceHeader
    {
        public string VoucherType { get; set; }
        public string InvoiceNo { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string PONo { get; set; }
        public string PODate { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedTime { get; set; }
        public string Vendor_Regn_Code { get; set; }
        public string ProductCode { get; set; }
        public string Po_Item_Serial_No { get; set; }
        public string Gate_Location { get; set; }
        public string Unit_UOM_ { get; set; }
        public string Cuurency_Name { get; set; }
        public string LR_Number { get; set; }

    }
    public class Address
    {
        public string Name { get; set; }
        public string buyer_address { get; set; }
        public string GSTIN { get; set; }
        public string ContactPerson { get; set; }
        public string MobileNo { get; set; }
        public string Buyer_State_Code { get; set; }
        public string Buyer_StateName { get; set; }

        public string RName { get; set; }
        public string Delivery_Location_Address1 { get; set; }
        public string Delivery_Location_Address2 { get; set; }
        public string RStateName { get; set; }
        public string delivery_location_gstin_no { get; set; }
        public string delivery_location_state_code { get; set; }
        public string Delivery_Location_State_Name { get; set; }
        public string RPlaceofSupply { get; set; }
    }
    public class MCoreProduct
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
    public class BodyData
    {
        public string SNo { get; set; }
        public string ProductCode { get; set; }
        public string PartyProductName { get; set; }
        public string Description { get; set; }
        public string HSNCode { get; set; }
        public string Unit { get; set; }
        public float Quantity { get; set; }
        public string Rate { get; set; }
        public string GSTPerct { get; set; }
        public float Amount { get; set; }
        public string Frieght { get; set; }

        public string Discount { get; set; }
        public string CGST { get; set; }
        public string CGSTInput { get; set; }
        public string SGST { get; set; }
        public string SGSTInput { get; set; }
        public string IGST { get; set; }
        public string IGSTInput { get; set; }
        public string Cess { get; set; }
        public string CessInput { get; set; }
        public string TaxableValue { get; set; }
        public string TaxableValueInput { get; set; }
        public string TaxAmount { get; set; }
        public string TaxAmountInput { get; set; }
        public string NetRate { get; set; }
        public string NetRateInput { get; set; }
        public string Width { get; set; }
        public string Length { get; set; }


    }
    public class Footer
    {
        public string CreditDays { get; set; }
        public string Deliver_Note { get; set; }
        public string GrossAmount { get; set; }
        public float Freight { get; set; }
        public float CSTValue { get; set; }
        public float SGSTValue { get; set; }
        public float IGSTVAlue { get; set; }
        public float RoundOff { get; set; }
        public float TotalAmount { get; set; }
        public string AmountInWords { get; set; }
        public float TCS { get; set; }
        public float TCSInput { get; set; }
    }
    public class LastSection
    {
        public string DispatchTrough { get; set; }
        public string TranspoterName { get; set; }
        public string VehicleNo { get; set; }

        public string NameOfBank { get; set; }
        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string Branch { get; set; }
    }

    public class XMLFields
    {
        public string ID { get; set; }
        public string Root { get; set; }
        public string Name { get; set; }
        public int SectionId { get; set; }
    }
 
    public class DyanmicInvoiceModel
    {
        public string FileName { get; set; }
        //public string PDFFile { get; set; }
        //public DateTime ReceivedDate { get; set; }
        //public DateTime ProcessDate { get; set; }
        //public string Status { get; set; }
        public dynamic LogoSection { get; set; }
        public dynamic DivAddress { get; set; }
        public dynamic POInvoice { get; set; }
        public dynamic BuyerAddress { get; set; }
        public dynamic DelLocAddress { get; set; }
        public dynamic ProdDetSec { get; set; }
        public dynamic GrossAmount { get; set; }
        public dynamic QRSection { get; set; }
        public dynamic AmountWordsSec { get; set; }
        public dynamic TransportSection { get; set; }
        public dynamic CompanyBankDetails { get; set; }
        public dynamic DeclarationSection { get; set; }
        public dynamic AuthorizedSection { get; set; }
        public dynamic ManageQrCodeSection { get; set; }
        public dynamic ManageQrCodeProductSection { get; set; }
        //public dynamic LogoQR { get; set; }
    }
    public class EmailModelDTO
    {
        public string EmailTo { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string AttachmentPath { get; set; }
    }
}