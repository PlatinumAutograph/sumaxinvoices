﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sumax_Hyundai.Models
{
    public class TemplateModel
    {
        public TemplateModel(int _VendorId, string _InvoiceTemplate, List<TemplateDetails> _TempList)
        {
            this.InvoiceTemplate = _InvoiceTemplate;
            this.VendorId = _VendorId;
            this.TempList = _TempList;
        }
        public TemplateModel(int _VendorId,int _TempId, string _InvoiceTemplate, List<TemplateDetails> _TempList)
        {
            this.InvoiceTemplate = _InvoiceTemplate;
            this.VendorId = _VendorId;
            this.TempId = _TempId;
            this.TempList = _TempList;
        }
        public int TempId { get; set; }
        public int VendorId { get; set; }
        public string InvoiceTemplate { get; set; }

        public List<TemplateDetails> TempList { get; set; }
    }

    public class TemplateDetails
    {
        public int SectionId { get; set; }

        public string XmlFields { get; set; }

        public string DisplayFields { get; set; }

        public int SortOrder { get; set; }


    }

    public class TemplateDetailResult
    {
        public int Id { get; set; }
        public string Section { get; set; }

        public string Root { get; set; }

        public int SectionId { get; set; }
        public int TemplateOrder { get; set; }

        public string XmlField { get; set; }
        public string DisplayField { get; set; }

        public dynamic FieldValue { get; set; }

        public int SortOrder { get; set; } 

        public string CreatedOn { get; set; }
        public string ModifedOn { get; set; }
    }
}