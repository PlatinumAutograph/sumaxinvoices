﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sumax_Hyundai.Models
{
    public class FilesDataDTO
    {
        public long FileId { get; set; }
        public string FileName { get; set; }
        public byte[] Data { get; set; }
        public Nullable<System.DateTime> ImportedDate { get; set; }
        public Nullable<System.DateTime> ProcessDate { get; set; }
        public string Status { get; set; }
        public string Extension { get; set; }
        public string InvoiceNo { get; set; }
    }
}