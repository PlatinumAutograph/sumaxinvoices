﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Sumax_Hyundai.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sumax Login</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%--<link rel="icon" type="image/png" href="assets/Login/images/icons/favicon.ico" />--%>
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/fonts/iconic/css/material-design-iconic-font.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/animate/animate.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/css-hamburgers/hamburgers.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/animsition/css/animsition.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/select2/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/vendor/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/css/util.css" />
    <link rel="stylesheet" type="text/css" href="assets/Login/css/main.css" />
    <link href='https://fonts.googleapis.com/css?family=Work+Sans:400,300,700' rel='stylesheet' type='text/css' />
    <link href="assets/Login/css/style.css" rel="stylesheet" />
</head>
<body>
    <div class="limiter">
        <div class="container-login100" style="background-image: url('assets/Login/images/bg-01.jpg');">
            <div class="profile profile--open">
                <button class="profile__avatar" id="toggleProfile">
                    <h2>Sumax Login</h2>
                </button>
                <div class="profile__form">
                    <form id="form1" class="validate-form" runat="server">
                        <div class="profile__fields">
                            <div class="field validate-input" data-validate="Enter Email">
                                <asp:TextBox runat="server" ID="fieldUser" CssClass="input" autofocus="autofocus" ClientIDMode="Static" />
                                <label for="fieldUser" class="label">Email*</label>
                            </div>
                            <div class="field validate-input" data-validate="Enter password">
                                <asp:TextBox runat="server" ID="fieldPassword" CssClass="input" ClientIDMode="Static" TextMode="Password" />
                                <label for="fieldPassword" class="label">Password*</label>
                            </div>
                            <asp:Label Style="color: red" ID="ErrorLabel" runat="server" />
                            <div class="contact100-form-checkbox">
                                <input class="checkbox-primary" id="chkRememberMe" type="checkbox" name="remember-me" runat="server" />
                                Remember me
                                <a href="#" id="forgotPassword" data-toggle="modal" data-target="#myModal" style="float: right">Forgot Password</a>
                            </div>
                            <div class="profile__footer" style="">
                                <asp:Button Text="Login" CssClass="btn login100-form-btn" runat="server" ID="Loginbtn" OnClick="Loginbtn_Click" />
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/Login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="assets/Login/vendor/animsition/js/animsition.min.js"></script>
    <script src="assets/Login/vendor/bootstrap/js/popper.js"></script>
    <script src="assets/Login/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/Login/vendor/select2/select2.min.js"></script>
    <script src="assets/Login/vendor/countdowntime/countdowntime.js"></script>

    <script>
        document.getElementById('toggleProfile').addEventListener('click', function () {
            [].map.call(document.querySelectorAll('.profile'), function (el) {
                el.classList.toggle('profile--open');
            });
        });
    </script>


</body>
</html>
