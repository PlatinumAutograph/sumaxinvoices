﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SumaxHeader.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sumax_Hyundai.InvoiceManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
         .highlight {
            background-color: #cdffd5;
        }
    </style>
    <br />
    <div class="container-fluid" style="margin-top: 55px;">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 pan">
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-lg-offset-1">
                        <div class="row" style="position: relative; top: 17px;">
                            <form class="form-horizontal">
                                <div class="form-group" runat="server" id="divChooseTemplate" visible="true">
                                    <label class="col-md-3 control-label">Choose Template</label>
                                    <div class="col-md-7" style="margin-bottom: 10px">
                                        <asp:DropDownList ID="ddlTemplate" Visible="true" runat="server" CssClass="form-control" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlTemplate_SelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Upload Files from</label>
                                    <div class="col-md-7">
                                        <asp:HiddenField runat="server" ID="languageHiddenField" />
                                        <asp:FileUpload ID="fileUpload" ViewStateMode="Enabled" class="form-control" Style="padding: 0px 10px;" runat="server"></asp:FileUpload>
                                        <button runat="server" id="importBtn" type="button" class="btn btn-success btn-sm" onserverclick="importButton_Click" style="position: relative; top: -37px; left: 105%;"><i class="fa fa-file-pdf-o"></i>&nbsp;Import </button>

                                    </div>

                                </div>
                                <div class="form-group ">
                                    <label class="col-md-3">&nbsp;</label>
                                    <div class="col-md-7">
                                        <asp:Label ID="lblMessage" runat="server" Style="color: red; font-weight: bold;" />

                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="container-fluid ta">
        <div class="row pan" style="margin: 5px -15px;">
            <label class="col-sm-3 col-sm-offset-5" style="font-size: 20px;">Invoices Received</label>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="table-responsive">
                    <table id="inboundTable" class="table table-colored-full table-hover tabless">
                        <thead>
                            <tr role="row">
                                <th>File ID</th>
                                <th>File Name</th>
                                <th>Invoice No</th>
                                <th>Received Date &amp; Time</th>
                                <th>Process Date &amp; Time</th>
                                <th>Status</th>
                                <th>Action </th>
                            </tr>
                        </thead>
                        <tbody id="tbodyInvoice" runat="server">
                        </tbody>
                    </table>

                </div>
            </div>

            <br />
        </div>

    </div>

</asp:Content>
