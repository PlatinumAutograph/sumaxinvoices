﻿using Sumax_Hyundai.BAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sumax_Hyundai
{
    public partial class RenameFields : System.Web.UI.Page
    {
        private InvoiceBAL invoiceBAL = new InvoiceBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                string vendorId = Request.QueryString["VendorId"] == null ? string.Empty : Convert.ToString(Request.QueryString["VendorId"]);

                if (!IsPostBack)
                {
                    if (vendorId != "")
                    {
                        ddlVendors.SelectedValue = vendorId;
                        BindTemplateDetails(Convert.ToInt32(vendorId));
                        string vendorName = invoiceBAL.GetVendor(Convert.ToInt32(vendorId)).VendorName;
                        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallFunc", "SetMenuSelected(" + vendorId + ", '" + vendorName + "')", true);
                    }
                    LoadVendors();
                    sideMenuRename.InnerHtml = BindVendorsSideMenu();
                }
            }
        }

        private void LoadVendors()
        {
            ddlVendors.DataSource = invoiceBAL.GetVendorsList();
            ddlVendors.DataTextField = "VendorName";
            ddlVendors.DataValueField = "Id";
            ddlVendors.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Vendor" });
            ddlVendors.DataBind();
        }

        public string BindVendorsSideMenu()
        {
            StringBuilder sbData = new StringBuilder();
            List<tbl_Vendors> list = invoiceBAL.GetVendorsList();

            sbData.Append("<ul class='customer-list' id='menuRenameVendors'> ");

            foreach (var item in list)
            {
                sbData.Append("<li class='ripple'>");
                sbData.Append("<a id='' data-toggle='collapse' data-target='#" + item.Id + "'>" + item.VendorName + " Invoice&nbsp;&nbsp;&nbsp;<i class='fas fa-plus-circle'></i> </a>");
                sbData.Append(" <ul id='" + item.Id + "' class='collapse submenu'>  ");
                sbData.Append("  <li><a href='template.aspx?VendorId=" + Convert.ToString(item.Id) + "'>Add / Remove Field</a> </li> ");
                sbData.Append(" <li><a href='RenameFields.aspx?VendorId=" + Convert.ToString(item.Id) + "'>Rename Fields</a></li> </ul> ");
            }
            sbData.Append(" </ul>");
            return sbData.ToString();
        }

        public void BindTemplateDetails(int vendorId)
        {
            var res = invoiceBAL.GetTemplateDetailList(vendorId);
            StringBuilder sbData = new StringBuilder();
            try
            {
                foreach (var item in res)
                {
                    sbData.Append("<tr>");
                    sbData.Append("<td>" + item.Section + "</td>");
                    sbData.Append("<td>" + item.XmlField + "</td>");
                    sbData.Append("<td>" + item.DisplayField + "</td>");
                    sbData.Append("<td>" + item.CreatedOn + "</td>");
                    sbData.Append("<td>" + item.ModifedOn + "</td>");
                    sbData.Append("<td><a href='#' data-toggle='modal' onclick='GetTemplateDetails(" + item.Id + ");' data-target='#myModal'>Rename</a></td>");
                    sbData.Append("</tr>");
                }


                tbodytempDetail.InnerHtml = sbData.ToString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        protected void ddlVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindTemplateDetails(Convert.ToInt32(ddlVendors.SelectedValue));
        }

    }
}