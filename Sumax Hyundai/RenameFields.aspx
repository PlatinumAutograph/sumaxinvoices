﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SumaxHeader.Master" AutoEventWireup="true" CodeBehind="RenameFields.aspx.cs" Inherits="Sumax_Hyundai.RenameFields" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script src="assets/js/jquery-1.12.4.min.js"></script>



    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="customers">
            <h3>Customers</h3>
        </div>
        <hr />

        <div id="sideMenuRename" runat="server">
        </div>


        <%-- <ul class="customer-list" id="menuRenameVendors">
                    <li class="ripple">
                        <a id="" data-toggle="collapse" data-target="#hynd">Hyundai&nbsp;&nbsp;&nbsp;<i class="fas fa-minus-circle"></i>
                        </a>
                        <ul id="hynd" class="collapse in submenu">
                            <li><a href="/template.aspx">Add / Remove Field</a> </li>
                            <li><a href="/RenameFields.aspx">Rename Fields</a></li>
                        </ul>
                    </li>
                    <li class="ripple">
                        <a id="">TATA&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                        </a>
                    </li>
                    <li class="ripple">
                        <a id="">Nissan&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                        </a>
                    </li>

                </ul>--%>
        <!-- sidebar menu end-->
    </div>
    <section id="main-content">
        <section class="content-wrapper">
            <section class="panel">
                <header class="panel-heading">
                    <b><span id="">Rename Fields</span></b>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group">
                            <label class="control-label col-sm-2 col-sm-offset-2" for="chooseSection">Choose Vendor:</label>
                            <div class="col-sm-4">
                                <asp:DropDownList ID="ddlVendors" runat="server" CssClass="form-control" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVendors_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                            <div class="col-sm-4">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Section</th>
                                        <th>Field Name</th>
                                        <th>Display Name</th>
                                        <th>Created On</th>
                                        <th>Modified On</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbodytempDetail" runat="server">
                                    <%--<tr>
                                        <td>DOC NO</td>
                                        <td>INVOICE NO</td>
                                        <td><a href="#" data-toggle="modal" data-target="#myModal">Rename</a></td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </section>

        </section>
    </section>
    <div class="modal fade animated zoomIn" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: white">&times;</button>
                    <h4 class="modal-title">Rename</h4>
                </div>
                <div class="modal-body">
                    <form action="/">
                        <div class="form-group">
                            <label for="fName">Section:</label>
                            <code><span id="SName" style="font-size: 16px; font-weight: bold;"></span></code>
                        </div>
                        <div class="form-group">
                            <label for="fName">Field Name:</label>
                            <input type="text" class="form-control" id="fName" value="" disabled="disabled">
                        </div>
                        <div class="form-group">
                            <label for="dname">Display Name:</label>
                            <input type="hidden" id="hdnTemplateId" value="" />
                            <input type="text" class="form-control" id="dname">
                            <label id="lblErrorMessage" class="label label-danger" style="display: none;">
                                Please enter display name
                            </label>
                        </div>
                        <div class="form-group pull-right">

                            <button type="button" id="btnSaveTempalte" class="btn btn-success">Save</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('#btnSaveTempalte').click(function () {

            var displayName = $('#dname').val();
            var templateId = $('#hdnTemplateId').val();

            $.ajax({
                url: 'ASHX/AjaxHdlr.ashx',
                data: "TemplateDetailId=" + templateId + "&DisplayName=" + displayName,
                success: function (msg) {
                    if (msg.data == 1) {
                        $('#myModal').modal('hide');
                        window.location.reload(true);
                    }
                    else
                        $.alert("There was an error while updating template details. Try again!");
                },
                error: function (e) {
                    console.log(e);
                }
            })

        });

        function SetMenuSelected(VendorId, VendorName) {
            var selectedVendorId = $("#menuRenameVendors li a:contains('" + VendorName + "')").data('target');
            if ('#' + VendorId == selectedVendorId) {
                //$("#menuRenameVendors li a:contains('" + VendorName + "')").toggle();
                //$("#menuRenameVendors li a:contains('" + VendorName + "')").find('i').toggleClass('fa-plus-circle fa-minus-circle');
            }
        }

    </script>

</asp:Content>
