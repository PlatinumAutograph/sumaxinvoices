﻿using sumax.DAL;
using sumax.Models;
using Sumax_Hyundai.BAL;
using Sumax_Hyundai.DAL;
using Sumax_Hyundai.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using Sumax_Hyundai;
using Ionic.Zip;



namespace sumax.ASHX
{
    /// <summary>
    /// Summary description for AjaxHdlr
    /// </summary>
    public class AjaxHdlr : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        HttpContext context;
        public void ProcessRequest(HttpContext context)
        {
            this.context = context;
            object username = context.Session["UserEmail"];
            if (username == null)
            {
                return;
            }
            object TempDetId = context.Request["TemplateDetailId"];
            string DisplayField = context.Request["DisplayName"];
            if (TempDetId != null && DisplayField != null)
            {
                UpdateTemplateDetails(Convert.ToInt32(TempDetId), DisplayField);
            }
            object TemplateDetailId = context.Request["TemplateDetailId"];
            if (TemplateDetailId != null)
            {
                GetTemplateDetails(Convert.ToInt32(TemplateDetailId));
            }
            object VendorName = context.Request["VendorName"];
            if (VendorName != null)
            {
                InsertVendor(Convert.ToString(VendorName));
            }
            object IsGeneratePdf = context.Request["Generatepdf"];
            if (IsGeneratePdf != null)
            {
                GeneratePdf();
                return;
            }
            object DownloadAll_id = context.Request["DownloadAll_id"];
            if (DownloadAll_id != null)
            {
                DownloadAll();
                return;
            }
            object Email = context.Request["Email"];
            if (Email != null)
            {
                AddUser();
                return;
            }
            object deluserid = context.Request["deluserid"];
            if (deluserid != null)
            {
                DeleteUser(deluserid.ToString());
            }
            object currentpassword = context.Request["CurrentPassword"];
            object newpassword = context.Request["changepassword"];
            object ConfrimPassword = context.Request["ConfrimPassword"];
            if (currentpassword != null && newpassword != null && ConfrimPassword != null)
            {
                Changepassword_Click(currentpassword.ToString(), newpassword.ToString(), ConfrimPassword.ToString());
            }

            object fName = context.Request["fName"];
            object LName = context.Request["LName"];
            object userid = context.Request["userid"];
            object TemplateId = context.Request["TemplateId"];
            if (userid != null && fName != null && LName != null && TemplateId != null)
            {
                Edituser_Click(userid.ToString(), fName.ToString(), LName.ToString(), TemplateId.ToString());
            }

        }

        public void GeneratePdf()
        {
            //InvoiceModel invoiceModel = context.Session["InvoiceMode"] as InvoiceModel;

            long FileId = Convert.ToInt64(context.Request["Generatepdf"]);
            bool flag = false;
            if (FileId != 0)
            {
                //flag = new Utility().GenerateInvoice(context.Server.MapPath("~/assets\\images\\"),ref invoiceModel);
                //flag = new Utility().GenerateInvoice(FileId);
                flag = new InvoiceBAL().GenerateInvoiceForTemplate(FileId);
            }
            RespondWithJSON(flag.ToString().ToLower());
        }
        public void DownloadAll()
        {
            long FileId = Convert.ToInt64(context.Request["DownloadAll_id"]);
            string FileName = context.Request["FileName"];
            string InvoiceNo = context.Request["InvoiceNo"];
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    //zip.AddDirectoryByName("Files_" + FileName);
                    string pdfpath = HttpContext.Current.Server.MapPath("~/pdf");

                    foreach (string invoiceType in InvoiceTypes.InvoiceType)
                    {
                        string filePath = string.Format("{4}/{0}_{1}_{3}_{2}.pdf", FileName, InvoiceNo, invoiceType, FileId, pdfpath);
                        zip.AddFile(filePath, "Files_" + FileName);
                    }

                    string zipName = String.Format("{0}_{1}.zip", "All", FileId);

                    zip.Save(Path.Combine(pdfpath, zipName));
                    //   JSON.RespondWithJSON("success", context);
                    RespondWithJSON("true");

                }
            }
            catch (Exception ex)
            {
                RespondWithJSON("false");

            }


        }
        private void AddUser()
        {
            string Firstname = context.Request["FirstName"];
            string LastName = context.Request["LastName"];
            string Email = context.Request["Email"];
            string TemplateId = context.Request["TemplateId"];
            string UserPassword = context.Request["UserPassword"];
            string res = "error";
            try
            {
                if (!string.IsNullOrEmpty(Firstname) &&
                    !string.IsNullOrEmpty(LastName) &&
                    !string.IsNullOrEmpty(Email) &&
                    !string.IsNullOrEmpty(TemplateId) &&
                    !string.IsNullOrEmpty(UserPassword)
                    )
                {
                    EntityDAL entityDAL = new EntityDAL();
                    if (entityDAL.CheckUser(Email))
                    {
                        if (entityDAL.AddUser(Firstname, LastName, Email, TemplateId, UserPassword))
                            res = "success";
                    }
                    else
                        res = "exists";
                }
            }
            catch (Exception ex)
            {
            }
            RespondWithJSON("{\"data\":\"" + res + "\"}");
            // JSON.RespondWithJSON("{\"data\":" + res + "}", context);
        }
        public void DeleteUser(string DeleteUserId)
        {
            long UserId = 0;
            string res = "error";
            if (long.TryParse(DeleteUserId, out UserId))
            {
                EntityDAL entityDAL = new EntityDAL();
                if (entityDAL.DeleteUser(UserId))
                    res = "success";

            }
            JSON.RespondWithJSON("{\"data\":" + res + "}", context);

        }

        public void InsertVendor(string VendorName)
        {
            InvoiceBAL invoiceBAL = new InvoiceBAL();
            var res = invoiceBAL.InsertVendor(VendorName);

            JSON.RespondWithJSON("{\"data\":" + res + "}", context);
        }

        public void GetTemplateDetails(int TemplateDetailId)
        {
            InvoiceBAL invoiceBAL = new InvoiceBAL();
            var res = invoiceBAL.GetTemplateDetails(TemplateDetailId);

            JSON.RespondWithJSON("{\"data\":" + res + "}", context);
        }

        public void UpdateTemplateDetails(int TemplateDetailId, string DisplayField)
        {
            InvoiceBAL invoiceBAL = new InvoiceBAL();
            var res = invoiceBAL.UpdateTemplateDetails(TemplateDetailId, DisplayField);

            JSON.RespondWithJSON("{\"data\":" + res + "}", context);
        }


        //changepassword

        public void Changepassword_Click(string CurrentPassword, string changepassword, string ConfrimPassword)
        {
            sumaxEntities db = new sumaxEntities();
            string res = "error";
            string passwordHash = Helper.GetPasswordHash(CurrentPassword);
            tbl_Users user = db.tbl_Users.Where(x => x.Password.Equals(passwordHash)).FirstOrDefault();
            if (user != null)
            {
                string Hash = Helper.GetPasswordHash(changepassword);
                user.Password = Hash;
                db.SaveChanges();
                res = "success";
            }

            RespondWithJSON("{\"data\":\"" + res + "\"}");
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public void RespondWithJSON(string value)
        {
            try
            {
                context.Response.ContentType = "application/json";
                context.Response.Write(value);
                context.Response.End();
            }
            catch (ThreadAbortException ex)
            {
                //Do nothing
            }
            catch (Exception ex)
            {

            }
        }

        public void Edituser_Click(string userid, string fName, string LName, string TemplateId)
        {
            sumaxEntities db = new sumaxEntities();
            string res = "error";
            int id = Convert.ToInt32(userid);
            tbl_Users user = db.tbl_Users.Where(x => x.UserId.Equals(id)).FirstOrDefault();
            TemplateId = TemplateId.Trim().Replace(',', '|');
            if (user != null)
            {
                user.FirstName = fName;
                user.LastName = LName;
                user.TemplateId = TemplateId;
                db.SaveChanges();
                res = "success";
            }
            user.Password = "";
            RespondWithJSON("{\"data\":\"" + res + "\"}");
        }
    }
}