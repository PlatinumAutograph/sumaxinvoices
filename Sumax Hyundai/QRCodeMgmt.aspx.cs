﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sumax_Hyundai
{
    public partial class QRCodeMgmt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                bool isAdmin = Convert.ToString(Session["UserType"]).Equals("A", StringComparison.InvariantCultureIgnoreCase);
                if (!isAdmin)
                    Response.Redirect("Default.aspx");
            }

        }
    }
}