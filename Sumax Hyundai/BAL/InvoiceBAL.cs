﻿using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using sumax;
using sumax.DAL;
using sumax.Models;
using Sumax_Hyundai.DAL;
using Sumax_Hyundai.Models;
using Sumax_Hyundai.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml;

namespace Sumax_Hyundai.BAL
{
    public class InvoiceBAL
    {

        InvoiceDAL objInvoiceDAL;
        EntityDAL entityDAL;

        public InvoiceBAL()
        {
            objInvoiceDAL = new InvoiceDAL();
            entityDAL = new EntityDAL();
        }

        JavaScriptSerializer _jsSerial = new JavaScriptSerializer();

        public static List<XMLFields> GetXmlFields()
        {
            List<XMLFields> list = new List<XMLFields>();
            string applicationPath = HttpContext.Current.Server.MapPath("~/Data/");
            using (StreamReader sr = new StreamReader(applicationPath + "XMLFields.json"))
            {
                string json = sr.ReadToEnd();
                list = JSON.Deserialize<List<XMLFields>>(json);
                list = list.OrderBy(r => r.Name).ToList();
            }
            return list;
        }

        public static List<XMLFields> GetDefaultXmlFields()
        {
            List<XMLFields> list = new List<XMLFields>();
            string applicationPath = HttpContext.Current.Server.MapPath("~/Data/");
            using (StreamReader r = new StreamReader(applicationPath + "DefaultFields.json"))
            {
                string json = r.ReadToEnd();
                list = JSON.Deserialize<List<XMLFields>>(json);
            }
            return list;
        }

        public XMLFields GetXMLField(string Name)
        {
            var list = GetXmlFields();

            XMLFields res = list.Where(a => a.Name == Name).FirstOrDefault();

            return res;
        }

        public List<XMLFields> GetLeftedFields(int VendorId, int SectionId)
        {
            List<XMLFields> finalFields = new List<XMLFields>();
            int TempId = CheckIfTemplateDetailsExistsOrNot(VendorId);
            if (TempId == 0)
            {
                List<XMLFields> allXmlFields = GetXmlFields();
                var defaultFields = GetDefaultXmlFields().Where(a => a.SectionId == SectionId).ToList();
                foreach (var item in defaultFields)
                {
                    var itemToRemove = allXmlFields.Where(a => a.Name == item.Name).FirstOrDefault();
                    allXmlFields.Remove(itemToRemove);
                }
                finalFields = allXmlFields;
            }
            else
            {
                var mappedList = objInvoiceDAL.GetTemplateDetailList(VendorId).Where(a => a.SectionId == SectionId).ToList();
                List<XMLFields> allXmlFields = GetXmlFields();
                if (mappedList.Count() > 0)
                {
                    foreach (var item in mappedList)
                    {
                        var itemToRemove = allXmlFields.Where(a => a.Name == item.XmlField).FirstOrDefault();
                        allXmlFields.Remove(itemToRemove);
                    }
                }
                else
                {
                    var defaultFields = GetDefaultXmlFields().Where(a => a.SectionId == SectionId).ToList();
                    foreach (var item in defaultFields)
                    {
                        var itemToRemove = allXmlFields.Where(a => a.Name == item.Name).FirstOrDefault();
                        allXmlFields.Remove(itemToRemove);
                    }
                }
                finalFields = allXmlFields;
            }
            return finalFields;
        }

        public List<XMLFields> GetMappedFields(int VendorId, int SectionId)
        {
            List<XMLFields> finalFields = new List<XMLFields>();
            int TempId = CheckIfTemplateDetailsExistsOrNot(VendorId);
            if (TempId == 0)
            {
                finalFields = GetDefaultXmlFields().Where(a => a.SectionId == SectionId).ToList();
            }
            else
            {
                var mappedList = objInvoiceDAL.GetTemplateDetailList(VendorId).Where(a => a.SectionId == SectionId).ToList();
                if (mappedList.Count() > 0)
                {
                    foreach (var item in mappedList)
                    {
                        finalFields.Add(GetXMLField(item.XmlField));
                    }
                }
                else
                    finalFields = GetDefaultXmlFields().Where(a => a.SectionId == SectionId).ToList();
            }

            return finalFields;
        }

        public List<tbl_sections> GetSectionsList()
        {
            return (objInvoiceDAL.GetSectionsList());
        }

        public List<tbl_Vendors> GetVendorsList()
        {
            return (objInvoiceDAL.GetVendorsList());
        }

        public tbl_Vendors GetVendor(int VendorId)
        {
            return objInvoiceDAL.GetVendor(VendorId);
        }

        public string InsertVendor(string VendorName)
        {
            var model = objInvoiceDAL.InsertVendor(VendorName);
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
        }

        public int GetSectionId(string section)
        {
            return objInvoiceDAL.GetSectionId(section);
        }


        #region Templates
        public List<tbl_templates> GetAllTemplates(string TemplatesIds = "")
        {
            return objInvoiceDAL.GetAllTemplates(TemplatesIds);
        }
        public int CheckIfTemplateDetailsExistsOrNot(int VendorId)
        {
            return objInvoiceDAL.CheckIfTemplateDetailsExistsOrNot(VendorId);
        }
        public string GetTemplateDetails(int TemplatedDetailId)
        {
            var model = objInvoiceDAL.GetTemplateDetails(TemplatedDetailId);
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
        }
        public List<TemplateDetailResult> GetTemplateDetailList(int VendorId)
        {
            return objInvoiceDAL.GetTemplateDetailList(VendorId);
        }

        public string UpdateTemplateDetails(int TemplateDetailId, string DisplayField)
        {
            var model = objInvoiceDAL.UpdateTemplateDetails(TemplateDetailId, DisplayField);
            return JsonConvert.SerializeObject(model, Newtonsoft.Json.Formatting.None, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            });
        }

        public int InsertTemplate(TemplateModel templateModel)
        {
            tbl_templates ObjTemplates = new tbl_templates();
            string userName = Convert.ToString(HttpContext.Current.Session["username"]);
            ObjTemplates.VendorId = templateModel.VendorId;
            ObjTemplates.InvoiceTemplate = templateModel.InvoiceTemplate;
            ObjTemplates.CreatedBy = userName;
            ObjTemplates.CreatedOn = DateTime.Now;

            List<tbl_template_details> list = new List<tbl_template_details>();

            foreach (var item in templateModel.TempList)
            {
                list.Add(new tbl_template_details() { DisplayField = item.DisplayFields, SectionId = item.SectionId, SortOrder = item.SortOrder, XmlField = item.XmlFields, CreatedOn = DateTime.Now, CreatedBy = userName });
            }

            return objInvoiceDAL.InsertTemplate(ObjTemplates, list);
        }

        public int UpdateTemplate(TemplateModel templateModel)
        {
            tbl_templates ObjTemplates = new tbl_templates();
            string userName = Convert.ToString(HttpContext.Current.Session["username"]);
            ObjTemplates.Id = templateModel.TempId;
            ObjTemplates.ModifiedBy = userName;
            ObjTemplates.ModifiedOn = DateTime.Now;

            List<tbl_template_details> list = new List<tbl_template_details>();

            foreach (var item in templateModel.TempList)
            {
                list.Add(new tbl_template_details() { DisplayField = item.DisplayFields, SectionId = item.SectionId, SortOrder = item.SortOrder, XmlField = item.XmlFields, CreatedOn = DateTime.Now, CreatedBy = userName });
            }

            return objInvoiceDAL.UpdateTemplate(ObjTemplates, list);
        }

        public List<TemplateDetailResult> GetTemplateDetailsByVendor(int VendorId)
        {
            List<TemplateDetailResult> res = GetTemplateDetailList(VendorId);
            var xmlFields = GetXmlFields();

            foreach (var item in res)
            {
                var rootForItem = xmlFields.Where(a => a.Name == item.XmlField).Select(a => a.Root).FirstOrDefault();
                item.Root = rootForItem;
            }
            return res;
        }


        #endregion


        public bool GenerateInvoiceForTemplate(long FileId)
        {
            bool flag = false;

            tbl_FilesData tbl_Files = entityDAL.GetSingleFileData(FileId);
            if (tbl_Files != null)
            {
                DyanmicInvoiceModel invoiceModel;
                string Filename = string.Format("{0}_{1}", tbl_Files.FileName, tbl_Files.InvoiceNo);
                invoiceModel = ReadFile(tbl_Files, Filename, FileId);
                string applicationPath = HttpContext.Current.Server.MapPath("~/");
                if (invoiceModel != null)
                    flag = GeneratePDFInvoice(Filename, FileId, Path.Combine(applicationPath, "assets\\images\\"), invoiceModel);
                if (flag)
                {
                    //update status completed
                    entityDAL.UpdateStatusFile(FileId, "Completed");
                }
            }
            return flag;
        }

        private DyanmicInvoiceModel ReadFile(tbl_FilesData FileData, string Filename, long FileId)
        {
            DyanmicInvoiceModel invoiceModel = new DyanmicInvoiceModel();
            entityDAL.UpdateStatusFile(FileId, "In - Progress");

            string applicationPath = HttpContext.Current.Server.MapPath("~/PDF");

            Directory.CreateDirectory(applicationPath);

            Filename = Path.Combine(applicationPath, Filename);
            int TemplateId = FileData.TemplateId ?? 0;
            int VendorId = objInvoiceDAL.GetVendorIdByTemplate(TemplateId);
            //int VendorId = Convert.ToInt32(FileData.tbl_templates.VendorId);
            List<TemplateDetailResult> objModel = GetTemplateDetailsByVendor(VendorId);

            if (objModel != null)
            {
                invoiceModel = GetTemplateEntity(objModel);

                //invoiceModel.Status = "Received";
                invoiceModel.FileName = Filename;
                //invoiceModel.ReceivedDate = DateTime.UtcNow;

                try
                {
                    XmlDocument doc = new XmlDocument();
                    string text = System.Text.Encoding.UTF8.GetString(FileData.Data);
                    
                    doc.LoadXml(text);

                    for (int i = 0; i < invoiceModel.LogoSection.Count; i++)
                    {
                        //invoiceModel.LogoSection[i].FieldValue = GetLogoName(invoiceModel.LogoSection[i].Root, invoiceModel.LogoSection[i].XmlField);
                        //invoiceModel.LogoSection[1].FieldValue = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ0NDQwNUM3ODFFNDgyNTA3MkIzNENBNEY4QkRDNjA2Qzg2QjU3MjAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJSRVFGeDRIa2dsQnlzMHlrLUwzR0JzaHJWeUEifQ.eyJkYXRhIjoie1wiU2VsbGVyR3N0aW5cIjpcIjA2QUFFQ1M1NTAwTjFaQVwiLFwiQnV5ZXJHc3RpblwiOlwiMDZBQUVGSjkxNjlQMVpLXCIsXCJEb2NOb1wiOlwiTU5TLTIwLTIxLTQ5ODBcIixcIkRvY1R5cFwiOlwiSU5WXCIsXCJEb2NEdFwiOlwiMjQvMDMvMjAyMVwiLFwiVG90SW52VmFsXCI6NDk1NjAuMCxcIkl0ZW1DbnRcIjoyLFwiTWFpbkhzbkNvZGVcIjpcIjM5MTk5MDEwXCIsXCJJcm5cIjpcImZlN2I1ZDU3MDc4MjZiMWZkYmRjODdkYjVjZmE0ZjYzNTg0OWYzZTFiZjhlMWJjNmQ4NmIxNDkxYWU0ODk2OGRcIixcIklybkR0XCI6XCIyMDIxLTAzLTI0IDExOjUxOjAwXCJ9IiwiaXNzIjoiTklDIn0.VrVCIiR66UUvqb3GRvRL0u7ssYlRTSc7J_tvvbJmsbs_db-tE3LM5CZpYu_MY6mwYY7VJ7brBRaYJdzvHj7ifFJAiI03VQR0_UrdFFMAELMUVxzC0zlT5meuWIZgMFv2An9-fffNr8Aey61zJndu11-yF6J_Vpbw_DdpJeOFNVYEmKX1VxoY-0qOiJcawcTMhsbuOxP_N6a4qq1kkO6Y8rXVCmPKcJZdbhv5H-2a5aTib4XfNW04ZvTMDKBNTeRscMOG-qy2vzCumnuW_HMIldUpV3N1d3qM10Y3a8mln09SBxxhQ-teTDHCpUDIb2JihR5hD5k19bGeKwIi5Khrrw";// GetFieldValueByRoot(invoiceModel.LogoSection[1].Root, invoiceModel.LogoSection[1].XmlField, doc);
                        if (i > 0)
                        {
                            invoiceModel.LogoSection[i].FieldValue = GetFieldValueByRoot(invoiceModel.LogoSection[i].Root, invoiceModel.LogoSection[i].XmlField, doc);
                            //invoiceModel.LogoSection[1].FieldValue = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ0NDQwNUM3ODFFNDgyNTA3MkIzNENBNEY4QkRDNjA2Qzg2QjU3MjAiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJSRVFGeDRIa2dsQnlzMHlrLUwzR0JzaHJWeUEifQ.eyJkYXRhIjoie1wiU2VsbGVyR3N0aW5cIjpcIjA2QUFFQ1M1NTAwTjFaQVwiLFwiQnV5ZXJHc3RpblwiOlwiMDZBQUVGSjkxNjlQMVpLXCIsXCJEb2NOb1wiOlwiTU5TLTIwLTIxLTQ5ODBcIixcIkRvY1R5cFwiOlwiSU5WXCIsXCJEb2NEdFwiOlwiMjQvMDMvMjAyMVwiLFwiVG90SW52VmFsXCI6NDk1NjAuMCxcIkl0ZW1DbnRcIjoyLFwiTWFpbkhzbkNvZGVcIjpcIjM5MTk5MDEwXCIsXCJJcm5cIjpcImZlN2I1ZDU3MDc4MjZiMWZkYmRjODdkYjVjZmE0ZjYzNTg0OWYzZTFiZjhlMWJjNmQ4NmIxNDkxYWU0ODk2OGRcIixcIklybkR0XCI6XCIyMDIxLTAzLTI0IDExOjUxOjAwXCJ9IiwiaXNzIjoiTklDIn0.VrVCIiR66UUvqb3GRvRL0u7ssYlRTSc7J_tvvbJmsbs_db-tE3LM5CZpYu_MY6mwYY7VJ7brBRaYJdzvHj7ifFJAiI03VQR0_UrdFFMAELMUVxzC0zlT5meuWIZgMFv2An9-fffNr8Aey61zJndu11-yF6J_Vpbw_DdpJeOFNVYEmKX1VxoY-0qOiJcawcTMhsbuOxP_N6a4qq1kkO6Y8rXVCmPKcJZdbhv5H-2a5aTib4XfNW04ZvTMDKBNTeRscMOG-qy2vzCumnuW_HMIldUpV3N1d3qM10Y3a8mln09SBxxhQ-teTDHCpUDIb2JihR5hD5k19bGeKwIi5Khrrw";// GetFieldValueByRoot(invoiceModel.LogoSection[i].Root, invoiceModel.LogoSection[i].XmlField, doc);
                        }
                        else
                        {
                            invoiceModel.LogoSection[i].FieldValue = GetLogoName(invoiceModel.LogoSection[i].Root, invoiceModel.LogoSection[i].XmlField);
                        }
                    }

                    for (int i = 0; i < invoiceModel.DivAddress.Count; i++)
                        invoiceModel.DivAddress[i].FieldValue = GetFieldValueByRoot(invoiceModel.DivAddress[i].Root, invoiceModel.DivAddress[i].XmlField, doc, invoiceModel.DivAddress[i].DisplayField);

                    for (int i = 0; i < invoiceModel.POInvoice.Count; i++)
                        invoiceModel.POInvoice[i].FieldValue = GetFieldValueByRoot(invoiceModel.POInvoice[i].Root, invoiceModel.POInvoice[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.BuyerAddress.Count; i++)
                        invoiceModel.BuyerAddress[i].FieldValue = GetFieldValueByRoot(invoiceModel.BuyerAddress[i].Root, invoiceModel.BuyerAddress[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.DelLocAddress.Count; i++)
                        invoiceModel.DelLocAddress[i].FieldValue = GetFieldValueByRoot(invoiceModel.DelLocAddress[i].Root, invoiceModel.DelLocAddress[i].XmlField, doc);


                    invoiceModel.ProdDetSec = GetProductDetailSec(invoiceModel.ProdDetSec, doc);

                    for (int i = 0; i < invoiceModel.GrossAmount.Count; i++)
                        invoiceModel.GrossAmount[i].FieldValue = GetFieldValueByRoot(invoiceModel.GrossAmount[i].Root, invoiceModel.GrossAmount[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.QRSection.Count; i++)
                        invoiceModel.QRSection[i].FieldValue = GetFieldValueByRoot(invoiceModel.QRSection[i].Root, invoiceModel.QRSection[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.AmountWordsSec.Count; i++)
                        invoiceModel.AmountWordsSec[i].FieldValue = GetFieldValueByRoot(invoiceModel.AmountWordsSec[i].Root, invoiceModel.AmountWordsSec[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.TransportSection.Count; i++)
                        invoiceModel.TransportSection[i].FieldValue = GetFieldValueByRoot(invoiceModel.TransportSection[i].Root, invoiceModel.TransportSection[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.CompanyBankDetails.Count; i++)
                        invoiceModel.CompanyBankDetails[i].FieldValue = GetFieldValueByRoot(invoiceModel.CompanyBankDetails[i].Root, invoiceModel.CompanyBankDetails[i].XmlField, doc);


                    for (int i = 0; i < invoiceModel.AuthorizedSection.Count; i++)
                        invoiceModel.AuthorizedSection[i].FieldValue = GetFieldValueByRoot(invoiceModel.AuthorizedSection[i].Root, invoiceModel.AuthorizedSection[i].XmlField, doc, invoiceModel.AuthorizedSection[i].DisplayField);



                    for (int i = 0; i < invoiceModel.ManageQrCodeSection.Count; i++)
                        invoiceModel.ManageQrCodeSection[i].FieldValue = GetFieldValueByRoot(invoiceModel.ManageQrCodeSection[i].Root, invoiceModel.ManageQrCodeSection[i].XmlField, doc);


                    invoiceModel.ManageQrCodeProductSection = GetProductDetailSec(invoiceModel.ManageQrCodeProductSection, doc);

                }
                catch (Exception ex)
                {
                    Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "ReadFile", ex.Message + " | " + ex.StackTrace);
                    entityDAL.UpdateStatusFile(FileId, "Error");
                    invoiceModel = null;
                }
            }

            return invoiceModel;
        }

        public bool GeneratePDFInvoice(string Filename, long FileId, string images, DyanmicInvoiceModel invoiceModel)
        {
            bool flag = false;
            try
            {
                string Filepath = invoiceModel.FileName;
                string QRCodeText = string.Empty;

                Dictionary<string, int> dict = new Dictionary<string, int>();
                //Stream fileStream;

                #region Fonts
                //TODO: increase font size
                iTextSharp.text.Font shrinkFont = null; //  dynamic font based on string length

                Font font7 = FontFactory.GetFont(FontFactory.HELVETICA, 7);
                Font font8 = FontFactory.GetFont(FontFactory.HELVETICA, 8);
                Font font09 = FontFactory.GetFont(FontFactory.HELVETICA, 9);
                Font font09Bold = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
                Font font09BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font11BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 11, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font12 = FontFactory.GetFont(FontFactory.HELVETICA, 12);
                Font font12Bold = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
                Font font12BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font18Bold = FontFactory.GetFont(FontFactory.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);
                Font font18 = FontFactory.GetFont(FontFactory.HELVETICA, 18);
                Font font14 = FontFactory.GetFont(FontFactory.HELVETICA, 14);
                Font fontAnchor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, BaseColor.BLUE);
                Chunk bullet = new Chunk("\u2022", font18);

                #endregion


                foreach (string invoiceType in InvoiceTypes.InvoiceType)
                {
                    #region File
                    Filename = string.Format("{0}_{2}_{1}.pdf", Filepath, invoiceType, FileId);
                    var mem = new MemoryStream();
                    //fileStream = new System.IO.FileStream(Filename, System.IO.FileMode.Create);

                    string assemblyName = System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.ToString());
                    string assemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    string imagepath = images;

                    Document document = new Document(PageSize.A4, 0f, 0f, 9f, 15f);
                    //document.AddHeader("Test","Test Header");

                    PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, mem);
                    writer.PageEvent = new MyPdfPageEventHelper(invoiceModel, invoiceType, imagepath);

                    document.AddTitle(ApplicationSettings.CompanyInvoice);
                    document.AddSubject(ApplicationSettings.CompanyInvoice);
                    document.AddKeywords(ApplicationSettings.CompanyHeader);
                    document.AddCreator(".NET Assembly:" + assemblyName);
                    document.AddAuthor(ApplicationSettings.LogAppName);
                    document.AddProducer();
                    document.Open();

                    #endregion

                    Phrase phrase = new Phrase();
                    PdfPCell cell = new PdfPCell();
                    float[] widths = { 1.2f, 0.3f, 2.5f };

                    PdfPTable Address = new PdfPTable(2);
                    Address.LockedWidth = true;
                    Address.TotalWidth = InvoiceSettings.TotalWidth;

                    #region Buyer Address Section

                    PdfPTable BuyerAddress = new PdfPTable(3);
                    BuyerAddress.DefaultCell.HorizontalAlignment = 0;
                    BuyerAddress.DefaultCell.Padding = 5f;
                    BuyerAddress.SetWidths(widths);

                    var buyerAddressSec = (List<TemplateDetailResult>)invoiceModel.BuyerAddress;
                    if (buyerAddressSec.Count() > 0)
                    {
                        cell = new PdfPCell(new Paragraph("Buyer", font11BoldUnderline));
                        cell.Border = 0;
                        cell.Colspan = 3;
                        BuyerAddress.AddCell(cell);

                        string[] BuyerlNameList = new string[] { "Tags~3", "67111443|Delivery_Location_Address2", "MasterIdNameCode|BuyerName" };

                        var BuyerNameLst = buyerAddressSec.Where(a => BuyerlNameList.Contains(a.XmlField)).FirstOrDefault();
                        if (BuyerNameLst != null)
                        {
                            cell = new PdfPCell(new Phrase(string.Format("{0}", BuyerNameLst.FieldValue), font09Bold));
                            cell.Border = 0;
                            cell.Colspan = 3;
                            cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                            BuyerAddress.AddCell(cell);
                        }

                        shrinkFont = font09;
                        //SHIVA CHANGED CODE
                        string[] addressList1 = new string[] { "67111409|Buyer_Address", "67111442|Delivery_Location_Address1", "67111443|Delivery_Location_Address2" };
                        var BuyerAddressDet = buyerAddressSec.Where(a => addressList1.Contains(a.XmlField)).FirstOrDefault();
                        if (BuyerAddressDet != null)
                        {
                            if (!string.IsNullOrEmpty(BuyerAddressDet.FieldValue) && BuyerAddressDet.FieldValue.Length > 140 && BuyerAddressDet.FieldValue.Length < 170)
                                shrinkFont = font8;
                            else if (!string.IsNullOrEmpty(BuyerAddressDet.FieldValue) && BuyerAddressDet.FieldValue.Length >= 170)
                                shrinkFont = font7;
                            cell = new PdfPCell(new Phrase(string.Format("{0}", BuyerAddressDet.FieldValue), shrinkFont));
                            cell.Border = 0;
                            cell.Colspan = 3;
                            cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                            BuyerAddress.AddCell(cell);
                        }
                        shrinkFont = font09;
                        string[] arry1 = new string[] { "67111410|Buyer_Gstin_No", "67111411|Buyer_State_Code" };
                        var BuyerState = buyerAddressSec.Where(a => arry1.Contains(a.XmlField)).ToList();

                        if (BuyerState.Count() == 2)
                        {
                            cell = new PdfPCell(new Phrase(BuyerState[0].DisplayField, font09Bold));
                            cell.Border = 0;
                            BuyerAddress.AddCell(cell);

                            //cell = new PdfPCell(new Phrase(":", font09Bold));
                            //cell.Border = 0;
                            //BuyerAddress.AddCell(cell);

                            phrase = new Phrase(string.Format(": {0}", BuyerState[0].FieldValue), font09);
                            phrase.Add(new Chunk("  " + BuyerState[1].DisplayField + ": ", font09Bold));
                            phrase.Add(new Chunk(BuyerState[1].FieldValue, font09));
                            cell = new PdfPCell(phrase);
                            cell.Border = 0;
                            cell.Colspan = 2;

                            BuyerAddress.AddCell(cell);
                        }

                        foreach (var item in buyerAddressSec)
                        {
                            if (!addressList1.Contains(item.XmlField))
                            {
                                if (!arry1.Contains(item.XmlField) && !BuyerlNameList.Contains(item.XmlField))
                                {
                                    cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                    cell.Border = 0;
                                    BuyerAddress.AddCell(cell);

                                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                                    //cell.Border = 0;
                                    //BuyerAddress.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(string.Format(": {0}", item.FieldValue), font09));
                                    cell.Border = 0;
                                    cell.Colspan = 2;
                                    BuyerAddress.AddCell(cell);
                                }
                            }
                        }

                        Address.AddCell(BuyerAddress);
                    }




                    #endregion

                    #region Delivery Location Section

                    PdfPTable DelAddress = new PdfPTable(3);
                    DelAddress.DefaultCell.HorizontalAlignment = 0;
                    DelAddress.SetWidths(widths);

                    var DelAddressSec = (List<TemplateDetailResult>)invoiceModel.DelLocAddress;
                    if (DelAddressSec.Count() > 0)
                    {
                        cell = new PdfPCell(new Paragraph("Delivery Location", font11BoldUnderline));
                        cell.Border = 0;
                        cell.Colspan = 3;
                        DelAddress.AddCell(cell);

                        string[] DelNameList = new string[] { "Tags~3", "MasterIdNameCode|BuyerName" };

                        var DelNameLst = DelAddressSec.Where(a => DelNameList.Contains(a.XmlField)).FirstOrDefault();
                        if (DelNameLst != null)
                        {
                            cell = new PdfPCell(new Phrase(string.Format("{0}", DelNameLst.FieldValue), font09Bold));
                            cell.Border = 0;
                            cell.Colspan = 3;
                            cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                            DelAddress.AddCell(cell);
                        }

                        shrinkFont = font09;
                        string[] addressList1 = new string[] { "67111442|Delivery_Location_Address1", "67111443|Delivery_Location_Address2" };
                        var DelAddressDet = DelAddressSec.Where(a => addressList1.Contains(a.XmlField)).FirstOrDefault();
                        if (DelAddressDet != null)
                        {
                            if (!string.IsNullOrEmpty(DelAddressDet.FieldValue) && DelAddressDet.FieldValue.Length > 140 && DelAddressDet.FieldValue.Length < 170)
                                shrinkFont = font8;
                            else if (!string.IsNullOrEmpty(DelAddressDet.FieldValue) && DelAddressDet.FieldValue.Length >= 170)
                                shrinkFont = font7;
                            cell = new PdfPCell(new Phrase(string.Format("{0}", DelAddressDet.FieldValue), shrinkFont));
                            cell.Border = 0;
                            cell.Colspan = 3;
                            cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                            DelAddress.AddCell(cell);

                            shrinkFont = font09;
                        }
                        string[] arry1 = new string[] { "67111419|Delivery_Location_Gstin_No", "67111418|Delivery_Location_State_Code" };
                        var DelState = DelAddressSec.Where(a => arry1.Contains(a.XmlField)).ToList();

                        if (DelState.Count() == 2)
                        {
                            cell = new PdfPCell(new Phrase(DelState[0].DisplayField, font09Bold));
                            cell.Border = 0;
                            DelAddress.AddCell(cell);

                            //cell = new PdfPCell(new Phrase(":", font09Bold));
                            //cell.Border = 0;
                            //DelAddress.AddCell(cell);

                            phrase = new Phrase(string.Format(": {0}", DelState[0].FieldValue), font09);
                            phrase.Add(new Chunk("  " + DelState[1].DisplayField + ": ", font09Bold));
                            phrase.Add(new Chunk(DelState[1].FieldValue, font09));
                            cell = new PdfPCell(phrase);
                            cell.Border = 0;
                            cell.Colspan = 2;

                            DelAddress.AddCell(cell);
                        }

                        foreach (var item in DelAddressSec)
                        {
                            if (!addressList1.Contains(item.XmlField))
                            {
                                if (!arry1.Contains(item.XmlField) && !DelNameList.Contains(item.XmlField))
                                {
                                    cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                    cell.Border = 0;
                                    DelAddress.AddCell(cell);

                                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                                    //cell.Border = 0;
                                    //DelAddress.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(string.Format(": {0}", item.FieldValue), font09));
                                    cell.Border = 0;
                                    cell.Colspan = 2;
                                    DelAddress.AddCell(cell);
                                }
                            }
                        }

                        Address.AddCell(DelAddress);
                    }

                    #endregion

                    document.Add(Address);

                    #region Produts Details

                    DataTable ProdDet = invoiceModel.ProdDetSec;

                    if (ProdDet != null)
                    {
                        int productColumnCount = ProdDet.Columns.Count + 1;
                        PdfPTable ProductDetails = new PdfPTable(productColumnCount);
                        ProductDetails.TableEvent = new LineaBottom();
                        ProductDetails.LockedWidth = true;
                        if (productColumnCount == 8)
                            ProductDetails.SetWidths(InvoiceSettings.BodyWidths);
                        else if (productColumnCount > 0)
                        {
                            float[] bodyWidths = InvoiceSettings.BodyWidths;
                            int bodyWidthsCount = productColumnCount - bodyWidths.Count();
                            if (bodyWidthsCount > 0)
                            {
                                List<float> _lstBodywidths = bodyWidths.ToList();
                                for (int i = 0; i < bodyWidthsCount; i++)
                                    _lstBodywidths.Add(1f);
                                bodyWidths = _lstBodywidths.ToArray();
                            }
                            ProductDetails.SetWidths(bodyWidths);
                        }
                        else
                        {
                            List<float> _lstBodywidths = new List<float>();
                            for (int i = 0; i < productColumnCount; i++)
                                _lstBodywidths.Add(1f);
                            ProductDetails.SetWidths(_lstBodywidths.ToArray());
                        }
                        ProductDetails.TotalWidth = InvoiceSettings.TotalWidth;
                        ProductDetails.HeaderRows = 1;

                        // Table Headers
                        cell = new PdfPCell(new Phrase("S.No", font8));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        ProductDetails.AddCell(cell);

                        for (int i = 0; i < ProdDet.Columns.Count; i++)
                        {
                            cell = new PdfPCell(new Phrase(ProdDet.Columns[i].ColumnName, font8));
                            cell.Border = 0;
                            cell.HorizontalAlignment = 1;
                            cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                            cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                            cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                            cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            ProductDetails.AddCell(cell);
                        }

                        // Table Body
                        int SerialNo = 0;
                        foreach (DataRow row in ProdDet.Rows)
                        {
                            cell = new PdfPCell(new Phrase(string.Format("{0}", ++SerialNo), font09));
                            cell.Border = 0;
                            cell.HorizontalAlignment = 1;
                            cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                            cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                            cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                            ProductDetails.AddCell(cell);


                            for (int i = 0; i < ProdDet.Columns.Count; i++)
                            {
                                string colValue = Convert.ToString(row[ProdDet.Columns[i]]);
                                cell = new PdfPCell(new Phrase(string.Format("{0}", colValue), font09));
                                cell.Border = 0;
                                //cell.HorizontalAlignment = 1;
                                cell.HorizontalAlignment = 0;
                                if (i > 1 && i <= 3)
                                    cell.HorizontalAlignment = 1;
                                else if (i > 3)
                                    cell.HorizontalAlignment = 2;

                                cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                                cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                                cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                                ProductDetails.AddCell(cell);

                            }
                        }
                        float productTableHeight = ProductDetails.TotalHeight;
                        //Empty space in table
                        float FixedHeight = 250;

                        string strFixedHeight = Convert.ToString(ConfigurationSettings.AppSettings["productTableheight"]);
                        float.TryParse(strFixedHeight, out FixedHeight);

                        while (productTableHeight < FixedHeight)
                        {
                            for (int j = 0; j < productColumnCount; j++)
                            {
                                cell = new PdfPCell(new Phrase(" ", font09));
                                cell.Border = 0;
                                cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                                if (j == 0)
                                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                                ProductDetails.AddCell(cell);
                            }
                            productTableHeight = ProductDetails.TotalHeight;
                        }

                        document.Add(ProductDetails);
                    }


                    #endregion

                    PdfPTable FooterTable = new PdfPTable(3)
                    {
                        TableEvent = new LineaBottom(),
                        LockedWidth = true,
                        TotalWidth = InvoiceSettings.TotalWidth
                    };
                    FooterTable.SetWidths(InvoiceSettings.FooterWidths);

                    #region Gross Amount 

                    PdfPTable GrossAmountTable = new PdfPTable(3);
                    //GrossAmountTable.TableEvent = new LineaBottom();
                    float[] qrCode = { 1.4f, 0.3f, 0.8f };
                    GrossAmountTable.SetWidths(qrCode);
                    GrossAmountTable.HorizontalAlignment = 2;
                    GrossAmountTable.TotalWidth = InvoiceSettings.FooterTotalWidth;
                    GrossAmountTable.LockedWidth = true;

                    var GrossAmountSec = (List<TemplateDetailResult>)invoiceModel.GrossAmount;
                    if (GrossAmountSec.Count() > 0)
                    {
                        foreach (var item in GrossAmountSec)
                        {
                            cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                            cell.Border = 0;
                            GrossAmountTable.AddCell(cell);

                            cell = new PdfPCell(new Phrase(":", font09Bold));
                            cell.Border = 0;
                            GrossAmountTable.AddCell(cell);

                            cell = new PdfPCell(new Phrase(string.Format("{0:f2}", item.FieldValue), font09Bold));
                            cell.Border = 0;
                            cell.HorizontalAlignment = 2;
                            GrossAmountTable.AddCell(cell);
                        }

                        cell = new PdfPCell(GrossAmountTable);
                        cell.HorizontalAlignment = 2;
                        cell.Border = 0;
                        cell.PaddingTop = 2f;// InvoiceSettings.AmountInWordsPadding;
                        cell.PaddingBottom = 2f;// InvoiceSettings.AmountInWordsPadding;
                        cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                        cell.Colspan = 3;
                        FooterTable.AddCell(cell);
                    }


                    #endregion

                    #region QRCode Section 

                    var QRCodeSection = (List<TemplateDetailResult>)invoiceModel.QRSection;
                    var ManageQRCodeFields = (List<TemplateDetailResult>)invoiceModel.ManageQrCodeSection;
                    DataTable QRCodeProducts = invoiceModel.ManageQrCodeProductSection;

                    if (QRCodeSection.Count() > 0)
                    {
                        string leftFields = Convert.ToString(System.Web.Configuration.WebConfigurationManager.AppSettings["QrCodeSectionLeft"]);
                        //string[] arry1 = new string[] { "67110579|Credit_Days", "67111413|Deliver_Note", "67111435|Gate_Location", "67111698|Transporter_Name" };
                        string[] arry1 = leftFields.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

                        var QRCodeSectionDet = QRCodeSection.Where(a => arry1.Contains(a.XmlField)).ToList();
                        if (QRCodeSectionDet != null && QRCodeSection.Count() > 0)
                        {

                            //string[] arr2 = new string[] { "67110579|Credit_Days", "67111435|Gate_Location", "67111698|Transporter_Name" };
                            PdfPTable QRCodeSection1 = new PdfPTable(2);
                            if (ManageQRCodeFields == null || ManageQRCodeFields.Count() == 0)
                                QRCodeSection1.SetWidths(new float[] { .4f, 1.2f });
                            else
                                QRCodeSection1.SetWidths(new float[] { 1.2f, 1f });

                            //bool isQRCodeSection1 = false;
                            foreach (var item in QRCodeSectionDet)
                            {
                                //if (arr2.Contains(item.XmlField))
                                {
                                    //isQRCodeSection1 = true;
                                    //phrase = new Phrase(item.DisplayField + " : ", font09Bold);
                                    //phrase.Add(new Chunk(item.FieldValue, font09));

                                    cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                    cell.HorizontalAlignment = 0;
                                    cell.Border = 0;
                                    QRCodeSection1.AddCell(cell);


                                    cell = new PdfPCell(new Phrase(" : " + item.FieldValue, font09));
                                    cell.HorizontalAlignment = 0;
                                    cell.Border = 0;
                                    cell.PaddingRight = 20f;
                                    QRCodeSection1.AddCell(cell);
                                }
                            }
                            //if (isQRCodeSection1)
                            //{
                            cell = new PdfPCell(QRCodeSection1);
                            cell.HorizontalAlignment = 0;
                            cell.Border = 0;
                            if (ManageQRCodeFields == null || ManageQRCodeFields.Count() == 0)
                                cell.Colspan = 2;
                            cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                            FooterTable.AddCell(cell);
                            //}
                            //foreach (var item in QRCodeSectionDet)
                            //{
                            //    if (!arr2.Contains(item.XmlField))
                            //    {
                            //        phrase = new Phrase(item.DisplayField + " : ", font09Bold);
                            //        phrase.Add(new Chunk(item.FieldValue, font09));
                            //        cell = new PdfPCell(phrase);
                            //        cell.HorizontalAlignment = 0;
                            //        cell.Border = 0;
                            //        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                            //        FooterTable.AddCell(cell);
                            //    }
                            //}
                        }

                        // begin of code : Adding QR Image

                        if (ManageQRCodeFields != null && ManageQRCodeFields.Count() > 0)
                        {
                            QRCodeText = string.Empty;
                            var Delimiter = ManageQRCodeFields.Where(r => "QRCODE_DELIMITER".Equals(r.Root)).FirstOrDefault();
                            var ProductDelimiter = ManageQRCodeFields.Where(r => "QRCODE_PRODUCT_DELIMITER".Equals(r.Root)).FirstOrDefault();

                            string strQRDelimiter = "\t";
                            string strQRProductDelimiter = "\t";

                            if (Delimiter != null)
                                strQRDelimiter = Delimiter.DisplayField;

                            if (ProductDelimiter != null)
                                strQRProductDelimiter = ProductDelimiter.DisplayField;

                            if (strQRDelimiter == null || "QRCODE_DELIMITER".Equals(strQRDelimiter))
                                strQRDelimiter = "\t";

                            if (strQRProductDelimiter == null || "QRCODE_PRODUCT_DELIMITER".Equals(strQRProductDelimiter))
                                strQRProductDelimiter = strQRDelimiter;

                            strQRDelimiter = System.Text.RegularExpressions.Regex.Unescape(strQRDelimiter);
                            strQRProductDelimiter = System.Text.RegularExpressions.Regex.Unescape(strQRProductDelimiter);

                            ManageQRCodeFields = ManageQRCodeFields.Where(r => !"QRCODE_DELIMITER".Equals(r.Root) && !"QRCODE_PRODUCT_DELIMITER".Equals(r.Root)).ToList();


                            foreach (var item in ManageQRCodeFields)
                            {
                                if ("QRCODE_PRODUCT_DETAILS".Equals(item.Root))
                                {
                                    string strQRCodeProductText = string.Empty;
                                    foreach (DataRow row in QRCodeProducts.Rows)
                                    {
                                        strQRCodeProductText = string.Format("{0}{1}", strQRCodeProductText, strQRProductDelimiter);
                                        for (int i = 0; i < QRCodeProducts.Columns.Count; i++)
                                        {
                                            string colValue = Convert.ToString(row[QRCodeProducts.Columns[i]]);
                                            if (i == 0)
                                                strQRCodeProductText = string.Format("{0}{1}", strQRCodeProductText, colValue);

                                            else
                                                strQRCodeProductText = string.Format("{0}{1}{2}", strQRCodeProductText, strQRDelimiter, colValue);
                                        }
                                    }
                                    strQRCodeProductText = string.Format("{0}{1}", strQRCodeProductText, strQRProductDelimiter);

                                    QRCodeText = string.Format("{0}{1}", QRCodeText, strQRCodeProductText);
                                }
                                else
                                {

                                    if (item.XmlField.Contains("Date"))
                                    {
                                        string strDateFormat = "yymmdd";
                                        if (!item.XmlField.Equals(item.DisplayField))
                                            strDateFormat = item.DisplayField;

                                        item.FieldValue = Helper.ConvertNumberToDate(item.FieldValue, strDateFormat);
                                    }

                                    if (string.IsNullOrEmpty(QRCodeText))
                                        QRCodeText = item.FieldValue;
                                    //else if ("QRCODE_PRODUCT_DELIMITER".Equals(item.Root))
                                    //    QRCodeText = string.Format("{0}{1}", QRCodeText, "QRCODE_PRODUCT_DELIMITER".Equals(item.DisplayField) ? strQRDelimiter : item.DisplayField);
                                    else
                                        QRCodeText = string.Format("{0}{1}{2}", QRCodeText, strQRDelimiter, item.FieldValue);
                                }

                            }

                            Image QRCode = Image.GetInstance(Helper.GenerateQRCoder(QRCodeText), System.Drawing.Imaging.ImageFormat.Png);
                            QRCode.ScaleAbsoluteWidth(85f);
                            QRCode.ScaleAbsoluteHeight(85f);

                            cell = new PdfPCell(QRCode);
                            cell.HorizontalAlignment = 2;
                            cell.VerticalAlignment = 1;
                            cell.PaddingTop = 2f;
                            cell.PaddingRight = 20f;
                            cell.Border = 0;
                            FooterTable.AddCell(cell);
                        }

                        // end of code 

                        PdfPTable QRCodeSectionRightTable = new PdfPTable(3);
                        //QRCodeSectionRightTable.TableEvent = new LineaBottom();
                        float[] qrCode1 = { 1.4f, 0.3f, 0.8f };
                        //QRCodeSectionRightTable.SetWidths(InvoiceSettings.LastSectionWidths);
                        QRCodeSectionRightTable.SetWidths(qrCode1);
                        QRCodeSectionRightTable.HorizontalAlignment = 2;
                        QRCodeSectionRightTable.TotalWidth = InvoiceSettings.FooterTotalWidth;
                        QRCodeSectionRightTable.LockedWidth = true;

                        foreach (var item in QRCodeSection)
                        {
                            if (!arry1.Contains(item.XmlField))
                            {
                                cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                cell.Border = 0;
                                cell.PaddingTop = InvoiceSettings.footerCellPadding;
                                cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                                cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                                QRCodeSectionRightTable.AddCell(cell);

                                cell = new PdfPCell(new Phrase(":", font09Bold));
                                cell.Border = 0;
                                cell.PaddingTop = InvoiceSettings.footerCellPadding;
                                QRCodeSectionRightTable.AddCell(cell);

                                cell = new PdfPCell(new Phrase(string.Format("{0:f2}", item.FieldValue), font09Bold));
                                cell.Border = 0;
                                cell.PaddingTop = InvoiceSettings.footerCellPadding;
                                cell.HorizontalAlignment = 2;
                                QRCodeSectionRightTable.AddCell(cell);
                            }
                        }
                    }

                    #endregion

                    #region Amount in words

                    var AmountInWordsSection = (List<TemplateDetailResult>)invoiceModel.AmountWordsSec;

                    if (AmountInWordsSection.Count() > 0)
                    {
                        //foreach (var item in AmountInWordsSection)
                        //{
                        //    phrase = new Phrase(item.DisplayField + " :   ", font09Bold);

                        //    phrase.Add(new Chunk(string.Format("Rupees {0} Only", string.IsNullOrEmpty(item.FieldValue) ? "Zero" : item.FieldValue), font09));
                        //    phrase.Add(new Chunk(string.Format("\nIRN Number      :", font09Bold)));
                        //    phrase.Add(new Chunk(string.Format("12345689123456789123456789", font8)));
                        //    cell = new PdfPCell(phrase);
                        //    cell.HorizontalAlignment = 0;
                        //    cell.Colspan = 3;
                        //    cell.PaddingTop = InvoiceSettings.AmountInWordsPadding;
                        //    cell.PaddingBottom = InvoiceSettings.AmountInWordsPadding;
                        //    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        //    FooterTable.AddCell(cell);
                        //}
                        phrase = new Phrase();
                        for (int i=0;i< AmountInWordsSection.Count;i++)
                        {
                            if (i == 0)
                            {
                                phrase.Add(new Chunk(AmountInWordsSection[i].DisplayField + " :   ", font09Bold));
                                phrase.Add(new Chunk(string.Format("Rupees {0} Only", string.IsNullOrEmpty(AmountInWordsSection[i].FieldValue) ? "Zero" : AmountInWordsSection[i].FieldValue), font09));
                                continue;
                            }
                            else
                            {
                                phrase.Add(new Chunk(string.Format("\n"+AmountInWordsSection[i].DisplayField+"    :", font09Bold)));
                                phrase.Add(new Chunk(string.Format(AmountInWordsSection[i].FieldValue, font8)));
                            }
                            cell = new PdfPCell(phrase);
                            cell.HorizontalAlignment = 0;
                            cell.Colspan = 3;
                            cell.PaddingTop = InvoiceSettings.AmountInWordsPadding;
                            cell.PaddingBottom = InvoiceSettings.AmountInWordsPadding;
                            cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            FooterTable.AddCell(cell);
                        }

                    }

                    #endregion

                    document.Add(FooterTable);

                    PdfPTable LastSection = new PdfPTable(2);
                    LastSection.TableEvent = new LineaBottom();
                    LastSection.LockedWidth = true;
                    LastSection.TotalWidth = InvoiceSettings.TotalWidth;

                    #region Transport Section 

                    PdfPTable TransSectionTable = new PdfPTable(3);
                    //TransSectionTable.TableEvent = new LineaBottom();
                    TransSectionTable.SetWidths(InvoiceSettings.LastSectionWidths);
                    TransSectionTable.HorizontalAlignment = 0;

                    var TransportSection = (List<TemplateDetailResult>)invoiceModel.TransportSection;

                    if (TransportSection.Count() > 0)
                    {
                        foreach (var item in TransportSection)
                        {
                            cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                            cell.Border = 0;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            TransSectionTable.AddCell(cell);

                            cell = new PdfPCell(new Phrase(":", font09Bold));
                            cell.Border = 0;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            TransSectionTable.AddCell(cell);

                            bool isColspan = false;
                            if (item.FieldValue != null)
                            {
                                if (Convert.ToString(item.FieldValue).ToString().Length > 35)
                                {
                                    cell = new PdfPCell(new Phrase("", font09));
                                    cell.Border = 0;
                                    cell.PaddingLeft = 6f;
                                    TransSectionTable.AddCell(cell);
                                    isColspan = true;
                                }
                            }

                            cell = new PdfPCell(new Phrase(string.Format("{0}", item.FieldValue), font09));
                            cell.Border = 0;
                            if (isColspan)
                                cell.Colspan = 3;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            TransSectionTable.AddCell(cell);
                        }

                        cell = new PdfPCell(TransSectionTable);
                        cell.Border = 0;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        cell.PaddingTop = InvoiceSettings.CellPadding;
                        LastSection.AddCell(cell);
                    }

                    #endregion

                    #region Company Bank Details
                    string[] UPIQRCODE = new string[] { "UPI_QRCODE" };
                    var CompanyBankSection = (List<TemplateDetailResult>)invoiceModel.CompanyBankDetails;

                    PdfPTable BankSectionTable = null;

                    if (CompanyBankSection.Count() > 0)
                    {
                        if (CompanyBankSection.Any(r => UPIQRCODE.Contains(r.XmlField)))
                        {
                            BankSectionTable = new PdfPTable(2);
                            BankSectionTable.TableEvent = new LineaBottom();
                            BankSectionTable.HorizontalAlignment = 0;
                            float[] CompanyDetailSectionWidths = { 1.2f, 1f };
                            BankSectionTable.SetWidths(CompanyDetailSectionWidths);

                            cell = new PdfPCell(new Phrase("Company's Bank Details", font09BoldUnderline));
                            cell.Border = 0;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            BankSectionTable.AddCell(cell);

                            cell = new PdfPCell(new Phrase("Scan and Pay", font09Bold));
                            cell.Border = 0;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            cell.HorizontalAlignment = 1;
                            BankSectionTable.AddCell(cell);

                            PdfPTable BankDetailsTable = new PdfPTable(3);
                            //BankDetailsTable.TableEvent = new LineaBottom();
                            float[] BankDetailSectionWidths = { 1f, 0.1f, 1.1f };
                            BankDetailsTable.SetWidths(BankDetailSectionWidths);
                            BankDetailsTable.HorizontalAlignment = 0;

                            foreach (var item in CompanyBankSection)
                            {
                                if (!UPIQRCODE.Contains(item.XmlField) && !"UPI_ID".Equals(item.XmlField))
                                {
                                    cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                    cell.Border = 0;
                                    BankDetailsTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(":", font09Bold));
                                    cell.Border = 0;
                                    BankDetailsTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(string.Format("{0}", item.FieldValue), font09));
                                    cell.Border = 0;
                                    cell.HorizontalAlignment = 0;
                                    BankDetailsTable.AddCell(cell);
                                }
                            }


                            cell = new PdfPCell(BankDetailsTable);
                            cell.Border = 0;
                            //cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            BankSectionTable.AddCell(cell);

                            string strUpiImagePath = Path.Combine(imagepath, "citibank.jpeg");
                            Image UpiQrCode = Image.GetInstance(strUpiImagePath);
                            UpiQrCode.ScaleAbsoluteWidth(100f);
                            UpiQrCode.ScaleAbsoluteHeight(90f);

                            cell = new PdfPCell(UpiQrCode);
                            cell.HorizontalAlignment = 1;
                            cell.VerticalAlignment = 1;
                            cell.PaddingRight = 1f;
                            cell.PaddingBottom = 1f;
                            cell.PaddingTop = -1f;
                            cell.Border = 0;
                            cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            BankSectionTable.AddCell(cell);
                            if (CompanyBankSection.Any(r => "UPI_ID".Equals(r.XmlField)))
                            {
                                cell = new PdfPCell();
                                cell.Border = 0;
                                BankSectionTable.AddCell(cell);

                                cell = new PdfPCell(new Phrase("UPI ID : sumax@citigold", font09Bold));
                                cell.Border = 0;
                                cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                                cell.HorizontalAlignment = 1;
                                BankSectionTable.AddCell(cell);
                            }
                        }
                        else
                        {
                            BankSectionTable = new PdfPTable(3);
                            BankSectionTable.TableEvent = new LineaBottom();
                            float[] BankDetailSectionWidths = { 1f, 0.1f, 1.1f };
                            BankSectionTable.SetWidths(BankDetailSectionWidths);
                            BankSectionTable.HorizontalAlignment = 0;

                            cell = new PdfPCell(new Phrase("Company's Bank Details", font09BoldUnderline));
                            cell.Border = 0;
                            cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                            cell.Colspan = 3;
                            BankSectionTable.AddCell(cell);

                            foreach (var item in CompanyBankSection)
                            {
                                if (!"UPI_ID".Equals(item.XmlField))
                                {
                                    cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                                    cell.Border = 0;
                                    BankSectionTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(":", font09Bold));
                                    cell.Border = 0;
                                    BankSectionTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(string.Format("{0}", item.FieldValue), font09));
                                    cell.Border = 0;
                                    BankSectionTable.AddCell(cell);
                                }
                                else
                                {
                                    cell = new PdfPCell(new Phrase("UPI ID", font09Bold));
                                    cell.Border = 0;
                                    BankSectionTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase(":", font09Bold));
                                    cell.Border = 0;
                                    BankSectionTable.AddCell(cell);

                                    cell = new PdfPCell(new Phrase("sumax@citigold", font09));
                                    cell.Border = 0;
                                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                                    BankSectionTable.AddCell(cell);

                                }
                            }
                        }

                        cell = new PdfPCell(BankSectionTable);
                        cell.Border = 0;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        LastSection.AddCell(cell);


                        #endregion

                        #region Declarations

                        PdfPTable DeclarSection = new PdfPTable(1);
                        //DeclarSection.TableEvent = new LineaBottom();

                        DeclarSection.HorizontalAlignment = 0;
                        cell = new PdfPCell(new Phrase(ApplicationSettings.Declaration, font09BoldUnderline));
                        cell.Border = 0;
                        DeclarSection.AddCell(cell);
                        new Phrase();

                        cell = new PdfPCell(new Phrase(ApplicationSettings.Declarations, font09));
                        cell.Border = 0;
                        cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                        DeclarSection.AddCell(cell);

                        cell = new PdfPCell(DeclarSection);
                        cell.Border = 0;
                        cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        cell.PaddingBottom = InvoiceSettings.CellPadding;
                        LastSection.AddCell(cell);


                        #endregion

                        #region Authorized signed

                        PdfPTable AuthorizedTable = new PdfPTable(1);
                        AuthorizedTable.HorizontalAlignment = 0;
                        AuthorizedTable.TableEvent = new LineaBottom();
                        string CompanyName = ApplicationSettings.CompanyName;

                        var AuthorizedSection = (List<TemplateDetailResult>)invoiceModel.AuthorizedSection;
                        if (AuthorizedSection != null && AuthorizedSection.Count > 0)
                        {
                            CompanyName = AuthorizedSection[0].FieldValue;
                        }

                        cell = new PdfPCell(new Phrase(CompanyName, font09Bold));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        AuthorizedTable.AddCell(cell);
                        int k = 0;
                        while (k < 3)
                        {
                            k++;
                            cell = new PdfPCell(new Phrase(" ", font09Bold));
                            cell.Border = 0;
                            cell.HorizontalAlignment = 1;
                            AuthorizedTable.AddCell(cell);
                        }

                        cell = new PdfPCell(new Phrase(ApplicationSettings.AuthSign, font09Bold));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        AuthorizedTable.AddCell(cell);

                        cell = new PdfPCell(AuthorizedTable);
                        cell.Border = 0;
                        cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                        cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        LastSection.AddCell(cell);

                        #endregion

                        document.Add(LastSection);
                        document.Close();

                        //byte[] bytes = File.ReadAllBytes(@Filename);
                        byte[] bytes = mem.ToArray();
                        try
                        {
                            using (MemoryStream stream = new MemoryStream())
                            {
                                PdfReader reader = new PdfReader(bytes);
                                using (PdfStamper stamper = new PdfStamper(reader, stream))
                                {
                                    int pages = reader.NumberOfPages;
                                    for (int i = 1; i <= pages; i++)
                                    {
                                        if (i < pages)
                                            ColumnText.ShowTextAligned(stamper.GetUnderContent(i), Element.ALIGN_RIGHT, new Phrase(string.Format("Cont’d on page {0}", i + 1), font09), 568f, 12f, 0);
                                    }
                                }
                                bytes = stream.ToArray();
                            }
                            File.WriteAllBytes(@Filename, bytes);
                            flag = true;

                        }
                        catch (Exception ex)
                        {
                            flag = false;
                            Helper.WriteLog(ApplicationLogType.ERROR.ToString(), MethodBase.GetCurrentMethod().Name, ex.Message + " | " + ex.StackTrace.ToString());
                            entityDAL.UpdateStatusFile(FileId, "Error");
                        }
                        //string Filename1 = Path.Combine(Path.GetDirectoryName(Filename), Path.GetFileNameWithoutExtension(Filename) + Path.GetExtension(Filename));

                    }
                    if (flag)
                    {
                        try
                        {
                            using (ZipFile zip = new ZipFile())
                            {
                                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                                string pdfpath = HttpContext.Current.Server.MapPath("~/pdf");

                                foreach (string invoiceType1 in InvoiceTypes.InvoiceType)
                                {
                                    string filePath = string.Format("{0}_{2}_{1}.pdf", Filepath, invoiceType1, FileId);
                                    //string filePath = string.Format("{4}/{0}_{1}_{3}_{2}.pdf", FileName, InvoiceNo, invoiceType, FileId, pdfpath);
                                    zip.AddFile(filePath, "Files_");
                                }

                                string zipName = String.Format("{0}_{1}.zip", "All", FileId);

                                zip.Save(Path.Combine(pdfpath, zipName));


                            }
                        }
                        catch (Exception ex)
                        {
                            Helper.WriteLog(ApplicationLogType.ERROR.ToString(), MethodBase.GetCurrentMethod().Name, ex.Message + " | " + ex.StackTrace.ToString());
                            //entityDAL.UpdateStatusFile(FileId, "Error");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                flag = false;
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), MethodBase.GetCurrentMethod().Name, ex.Message + " | " + ex.StackTrace.ToString());
                entityDAL.UpdateStatusFile(FileId, "Error");

            }

            return flag;
        }

        private string GetFieldValueByRootInBody(XmlDocument doc, string root, string xmlfield)
        {
            string res = string.Empty;
            string val = string.Empty;
            XmlNodeList nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/BodyData/TransBody");
            if (nodelist.Count > 0)
            {
                XmlNode node = nodelist[0];

                if (root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/BatchNo"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Tags/IdNamePair"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BodyExtra/IdNamePair"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/BillBaseInfo"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Links/DataLink"
                || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Flags"
                || root.Equals("ArrayOfTransaction/Transaction/BodyData/TransBody/Amounts")
                || root.Equals("ArrayOfTransaction/Transaction/BodyData/TransBody/Book"))
                {
                    try
                    {
                        switch (root)
                        {
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales":
                                val = node.SelectSingleNode("Sales").SelectSingleNode(xmlfield).InnerText;
                                if (xmlfield.Equals("Unit", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    res = InvoiceDefaults.UOM(val);
                                }
                                else
                                {
                                    double iVal;
                                    res = double.TryParse(val, out iVal) ? string.Format("{0:f2}", iVal) : val;
                                }
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/BatchNo":
                                if (xmlfield == "MfgDate" || xmlfield == "ExpDate")
                                {
                                    var node1 = node.SelectSingleNode("Sales/BatchNo");
                                    string day = node1.SelectSingleNode(xmlfield).SelectSingleNode("Day").InnerText;
                                    string month = node1.SelectSingleNode(xmlfield).SelectSingleNode("Month").InnerText;
                                    string year = node1.SelectSingleNode(xmlfield).SelectSingleNode("Year").InnerText;
                                    res = day + "/" + month + "/" + year;
                                }
                                else
                                    res = node.SelectSingleNode("Sales/BatchNo").SelectSingleNode(xmlfield).InnerText;
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData":
                                //res = GetScreensData(node.SelectNodes("Sales/ScreenData/ScreenData"), xmlfield);
                                res = GetScreensData(doc.SelectNodes("ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData"), xmlfield);
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody":
                                res = node.SelectSingleNode(xmlfield).InnerText;
                                break;

                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Tags/IdNamePair":
                                res = GetTagsIdNamePair(node.SelectNodes("Tags/IdNamePair"), xmlfield, doc);
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/BodyExtra/IdNamePair":
                                res = GetBodyExtraIdNamePair(node.SelectNodes("BodyExtra/IdNamePair"), xmlfield);
                                break;

                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/BillBaseInfo":
                                res = node.SelectSingleNode("BillwiseData/BillAdjustment/BillBaseInfo").SelectSingleNode(xmlfield).InnerText;
                                break;

                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment":
                                val = node.SelectSingleNode("BillwiseData/BillAdjustment").SelectSingleNode(xmlfield).InnerText;
                                double dVal;
                                res = double.TryParse(val, out dVal) ? string.Format("{0:f2}", dVal) : val;
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Links/DataLink":
                                string field = xmlfield;
                                if (field.Contains("~"))
                                {
                                    field = field.Split('~')[2];
                                    res = node.SelectSingleNode("Links/DataLink").SelectSingleNode(field).InnerText;
                                }
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Flags":
                                string field1 = xmlfield;
                                if (field1.Contains("~"))
                                {
                                    field1 = field1.Split('~')[1];
                                    res = node.SelectSingleNode("Flags").SelectSingleNode(field1).InnerText;
                                }
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Amounts":
                                res = GetAmountsData(doc, root, xmlfield);
                                break;
                            case "ArrayOfTransaction/Transaction/BodyData/TransBody/Book":
                                res = GetMastersName(node, xmlfield, doc);
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        Helper.WriteLog("Ex", "GetFieldValueByRootInBody", ex.Message + " | " + ex.StackTrace);

                    }
                }
            }
            return res;
        }
        private string GetFieldValueByRootInFooter(XmlDocument doc, string root, string xmlfield)
        {
            string res = string.Empty;

            try
            {
                XmlNodeList nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/Footer/FooterData");

                double val = 0;
                foreach (XmlNode node in nodelist)
                {
                    string fieldName = node.SelectSingleNode("FieldName").InnerText;
                    if (xmlfield.Equals(fieldName))
                    {
                        res = node.SelectSingleNode("Value").InnerText;
                        if (double.TryParse(res, out val))
                        {
                            res = string.Format("{0:f2}", val);
                        }
                    }
                }


                //switch (root)
                //{
                //    case "ArrayOfTransaction/Transaction/Footer/FooterData":

                //        break;
                //    default:
                //        break;
                //}
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "GetFieldValueByRootInFooter", ex.Message + " | " + ex.StackTrace);
            }

            return res;
        }
        private DataTable GetProductDetailSec(List<TemplateDetailResult> ProdDetSec, XmlDocument doc)
        {
            DataTable dt = new DataTable();
            XmlNodeList nodelist;
            string strCode = string.Empty;
            foreach (var item in ProdDetSec)
            {
                //item.DisplayField = string.Format("{0}~~~{1}", item.DisplayField.Replace("|", string.Empty), item.XmlField);
                item.DisplayField = string.Format("{0}", item.DisplayField.Replace("|", string.Empty));
                dt.Columns.Add(item.DisplayField, typeof(System.String));
            }
            nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/BodyData/TransBody");
            if (nodelist.Count > 0)
            {
                foreach (XmlNode node in nodelist)
                {
                    DataRow dr = dt.NewRow();
                    foreach (var item in ProdDetSec)
                    {
                        string root = item.Root;
                        if (root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales" 
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/BatchNo"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody" 
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Tags/IdNamePair"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BodyExtra/IdNamePair"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/BillBaseInfo"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Links/DataLink"
                            || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Flags"
                            || root.Equals("ArrayOfTransaction/Masters/MastersExport/MastersData/arrMasterIdNameCode/MasterIdNameCode"))
                        {
                            switch (root)
                            {
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales":
                                    string val = node.SelectSingleNode("Sales").SelectSingleNode(item.XmlField).InnerText;
                                    if (item.XmlField.Equals("Unit"))
                                    {
                                        dr[item.DisplayField] = InvoiceDefaults.UOM(val);
                                    }
                                    else
                                    {
                                        double fval;
                                        dr[item.DisplayField] = double.TryParse(val, out fval) ? string.Format("{0:f2}", fval) : val;
                                    }
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/BatchNo":
                                    if (item.XmlField == "MfgDate" || item.XmlField == "ExpDate")
                                    {
                                        var node1 = node.SelectSingleNode("Sales/BatchNo");
                                        string day = node1.SelectSingleNode(item.XmlField).SelectSingleNode("Day").InnerText;
                                        string month = node1.SelectSingleNode(item.XmlField).SelectSingleNode("Month").InnerText;
                                        string year = node1.SelectSingleNode(item.XmlField).SelectSingleNode("Year").InnerText;
                                        dr[item.DisplayField] = day + "-" + month + "-" + year;
                                    }
                                    else
                                        dr[item.DisplayField] = node.SelectSingleNode("Sales/BatchNo").SelectSingleNode(item.XmlField).InnerText;
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData":
                                    dr[item.DisplayField] = GetScreensData(node.SelectNodes("Sales/ScreenData/ScreenData"), item.XmlField);
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody":
                                    dr[item.DisplayField] = node.SelectSingleNode(item.XmlField).InnerText;
                                    break;

                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Tags/IdNamePair":
                                    dr[item.DisplayField] = GetTagsIdNamePair(node.SelectNodes("Tags/IdNamePair"), item.XmlField, doc);
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/BodyExtra/IdNamePair":
                                    dr[item.DisplayField] = GetBodyExtraIdNamePair(node.SelectNodes("BodyExtra/IdNamePair"), item.XmlField);
                                    break;

                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/BillBaseInfo":
                                    dr[item.DisplayField] = node.SelectSingleNode("BillwiseData/BillAdjustment/BillBaseInfo").SelectSingleNode(item.XmlField).InnerText;
                                    break;

                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment":
                                    dr[item.DisplayField] = node.SelectSingleNode("BillwiseData/BillAdjustment").SelectSingleNode(item.XmlField).InnerText;
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Links/DataLink":
                                    string field = item.XmlField;
                                    if (field.Contains("~"))
                                    {
                                        field = field.Split('~')[2];
                                        dr[item.DisplayField] = node.SelectSingleNode("Links/DataLink").SelectSingleNode(field).InnerText;
                                    }
                                    break;
                                case "ArrayOfTransaction/Transaction/BodyData/TransBody/Flags":
                                    string field1 = item.XmlField;
                                    if (field1.Contains("~"))
                                    {
                                        field1 = field1.Split('~')[1];
                                        dr[item.DisplayField] = node.SelectSingleNode("Flags").SelectSingleNode(field1).InnerText;
                                    }
                                    break;
                                case "ArrayOfTransaction/Masters/MastersExport/MastersData/arrMasterIdNameCode/MasterIdNameCode":
                                    string productForMasterData = node.SelectSingleNode("Sales").SelectSingleNode("Product").InnerText;
                                    dr[item.DisplayField] = GetMastersData(productForMasterData, doc, item.XmlField, true);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    dt.Rows.Add(dr);
                }
            }
            string CodeDisplayField = ProdDetSec.Where(r => ProductDetailsSec.Code.Contains(r.XmlField)).Select(r => r.DisplayField).FirstOrDefault();
            string QuantityDisplayField = ProdDetSec.Where(r => ProductDetailsSec.Quantity.Contains(r.XmlField)).Select(r => r.DisplayField).FirstOrDefault();
            string RateDisplayField = ProdDetSec.Where(r => ProductDetailsSec.Rate.Contains(r.XmlField)).Select(r => r.DisplayField).FirstOrDefault();
            string AmountDisplayField = ProdDetSec.Where(r => ProductDetailsSec.Gross.Contains(r.XmlField)).Select(r => r.DisplayField).FirstOrDefault();

            if (dt.Rows.Count > 0 &&
                !string.IsNullOrEmpty(CodeDisplayField) &&
                !string.IsNullOrEmpty(QuantityDisplayField) &&
                !string.IsNullOrEmpty(RateDisplayField) &&
                !string.IsNullOrEmpty(AmountDisplayField))

                dt = RemoveDuplicateProductCodeExists(CodeDisplayField.Replace("|", string.Empty), QuantityDisplayField.Replace("|", string.Empty), RateDisplayField.Replace("|", string.Empty), AmountDisplayField.Replace("|", string.Empty), dt);

            else if (dt.Rows.Count > 0 && !string.IsNullOrEmpty(CodeDisplayField) && !string.IsNullOrEmpty(QuantityDisplayField))
            {
                dt = RemoveDuplicateProductCodeExists(CodeDisplayField.Replace("|", string.Empty), QuantityDisplayField.Replace("|", string.Empty), string.Empty, string.Empty, dt);
            }
            return dt;
        }

        private DataTable RemoveDuplicateProductCodeExists(string CodeDisplayField, string QuantityDisplayField, string RateDisplayField, string AmountDisplayField, DataTable dt)
        {
            DataTable tempDt = new DataTable();
            try
            {
                foreach (DataColumn item in dt.Columns)
                {
                    tempDt.Columns.Add(item.ColumnName, typeof(System.String));
                }
                List<string> _lstCode = dt.AsEnumerable().Select(r => r.Field<string>(CodeDisplayField)).Distinct().ToList();

                foreach (var item in _lstCode)
                {
                    DataRow row = tempDt.NewRow();
                    //String sExpression = CodeDisplayField + " = '" + item + "'";
                    String sExpression = string.Format("[{0}]='{1}'", CodeDisplayField, item);
                    DataRow[] drFound;
                    drFound = dt.Select(sExpression);

                    if (drFound.Count() > 1)
                    {
                        double Qty = 0, Rate = 0, Amount = 0;
                        foreach (DataRow dr in drFound)
                        {
                            foreach (DataColumn dc in dt.Columns)
                            {
                                try
                                {
                                    row[dc.ColumnName] = dr[dc.ColumnName];
                                    if (dc.ColumnName == QuantityDisplayField)
                                    {
                                        if (dr[dc.ColumnName] != null && dr[dc.ColumnName].ToString() != string.Empty)
                                            Qty = Qty + Convert.ToDouble(dr[dc.ColumnName]);
                                    }
                                    else if (dc.ColumnName == RateDisplayField)
                                    {
                                        //if (dr[dc.ColumnName] != null && dr[dc.ColumnName].ToString() != string.Empty)
                                        Rate = Convert.ToDouble(dr[dc.ColumnName]);
                                    }
                                    else if (dc.ColumnName == AmountDisplayField)
                                    {
                                        if (dr[dc.ColumnName] != null && dr[dc.ColumnName].ToString() != string.Empty)
                                            Amount = Amount + Convert.ToDouble(dr[dc.ColumnName]);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "RemoveDuplicateProductCodeExists ", ex.Message + "  |  " + ex.StackTrace);
                                }
                            }
                        }

                        if (!string.IsNullOrEmpty(QuantityDisplayField))
                            row[QuantityDisplayField] = string.Format("{0:f2}", Qty);

                        if (!string.IsNullOrEmpty(RateDisplayField))
                            row[RateDisplayField] = string.Format("{0:f2}", Rate);

                        if (!string.IsNullOrEmpty(AmountDisplayField))
                            row[AmountDisplayField] = string.Format("{0:f2}", Amount);

                        tempDt.Rows.Add(row);
                    }
                    else if (drFound.Count() == 1)
                    {
                        //row = drFound[0];
                        tempDt.ImportRow(drFound[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "RemoveDuplicateProductCodeExists", ex.Message + " | " + ex.StackTrace);

            }

            return tempDt;
        }

        private string GetBodyExtraIdNamePair(XmlNodeList nodeList, string XMLfield)
        {
            string res = string.Empty;

            try
            {
                foreach (XmlNode item in nodeList)
                {
                    string fieldName = item.SelectSingleNode("Name").InnerText;

                    if (fieldName.Trim() == XMLfield.Trim())
                    {
                        res = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(item.SelectSingleNode("Tag").InnerText));
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetBodyExtraIdNamePair", ex.Message + " | " + ex.StackTrace);

            }
            return res;
        }

        private string GetTagsIdNamePair(XmlNodeList nodeList, string XMLfield, XmlDocument doc)
        {
            string res = string.Empty;
            if (XMLfield.Contains("~"))
            {
                string field = XMLfield.Split('~')[1];
                foreach (XmlNode item in nodeList)
                {
                    string fieldName = item.SelectSingleNode("ID").InnerText;

                    if (fieldName.Trim() == field.Trim())
                    {
                        res = HttpUtility.HtmlDecode(HttpUtility.HtmlDecode(item.SelectSingleNode("Tag").InnerText));
                        if (!string.IsNullOrEmpty(res))
                            res = GetMastersData(res, doc, "Name", false);
                        break;
                    }
                }
            }
            return res;
        }
        private string GetMastersName(XmlNode node, string XMLfield, XmlDocument doc)
        {
            string res = string.Empty;

            if (XMLfield.Equals("MasterIdNameCode|BuyerName"))
            {
                string BuyerId = node.SelectSingleNode("Book").InnerText;
                if (!string.IsNullOrEmpty(BuyerId) && !string.IsNullOrWhiteSpace(BuyerId))
                {
                    res = GetMastersData(BuyerId, doc, "Name", false);
                }

            }
            return res;
        }
        private string GetScreensData(XmlNodeList nodeList, string XMLfield)
        {
            string res = string.Empty;
            double TotalVal = 0;
            try
            {
                foreach (XmlNode node1 in nodeList)
                {
                    string FieldName = node1.SelectSingleNode("FieldName").InnerText;
                    //string Value = node1.SelectSingleNode("Value").InnerText;
                    //string Input = node1.SelectSingleNode("Input").InnerText;
                    if (FieldName.Trim() == XMLfield.Trim())
                    {
                        string val = node1.SelectSingleNode("Value").InnerText;
                        double dVal;
                        if (double.TryParse(val, out dVal))
                        {
                            TotalVal += dVal;
                            res = string.Format("{0:f2}", TotalVal);
                        }
                        else
                            res = val;
                        // break;
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetScreensData", ex.Message + " | " + ex.StackTrace);

            }
            return res;
        }

        private string GetFieldValueByRoot(string root, string XmlField, XmlDocument xmlDoc, string DisplayField = "")
        {
            string res = string.Empty;
            try
            {

                if (root == "ArrayOfTransaction/Transaction/HeaderExtra/IdNamePair")
                {
                    res = SearchFieldTextByXMLTag(xmlDoc, root, XmlField);
                }
                else if (root.Equals("DivisionName") || root.Equals("AuthorisedSignatureSectionCompanyName"))
                {
                    res = DisplayField;
                }
                else if (root.Equals("ArrayOfTransaction/Transaction/Footer/FooterData"))
                {
                    res = GetFieldValueByRootInFooter(xmlDoc, root, XmlField);
                }
                else if (root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/BatchNo"
                    || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Sales/ScreenData/ScreenData"
                    || root == "ArrayOfTransaction/Transaction/BodyData/TransBody" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Tags/IdNamePair"
                    || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BodyExtra/IdNamePair" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody"
                    || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/BillBaseInfo"
                    || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment" || root == "ArrayOfTransaction/Transaction/BodyData/TransBody/Links/DataLink"
                    || root.Equals("ArrayOfTransaction/Transaction/BodyData/TransBody/Amounts")
                    || root.Equals("ArrayOfTransaction/Transaction/BodyData/TransBody/Book"))
                {
                    res = GetFieldValueByRootInBody(xmlDoc, root, XmlField);
                }
                else if (root.Equals("ArrayOfTransaction/Transaction/BodyData/TransBody/BillwiseData/BillAdjustment/OriginalAmountTC"))
                {
                    XmlNode node = xmlDoc.SelectSingleNode(root);
                    if (node != null)
                    {
                        res = node.InnerText;
                        if (double.TryParse(res, out double OriginalAmount))
                            res = NumberToWords.ConvertNumberToWords(OriginalAmount);
                    }
                }
                else if ("ONEBYONE".Equals(root))
                    res = "1/1";
                else if ("ZERO_CONSTANT".Equals(root))
                    res = "0.00";
                else if ("A".Equals(root))
                    res = "A";
                else if ("B".Equals(root))
                    res = "B";
                else if ("R".Equals(root))
                    res = "R";
                else if ("T".Equals(root))
                    res = "T";
                else
                {
                    if (root != null&&!root.Equals("QRCODE_DELIMITER"))
                    {
                        XmlNode node = xmlDoc.SelectSingleNode(root).SelectSingleNode(XmlField);
                        double dVal = 0;

                        if (node != null)
                            res = node.InnerText;
                        if (XmlField.Equals("Net") && double.TryParse(res, out dVal))
                        {
                            dVal = (0 > dVal) ? (dVal * -1) : dVal;
                            res = string.Format("{0:f2}", dVal);
                        }
                       
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetFieldValueByRoot", ex.Message + " | " + ex.StackTrace);
            }
            return res;
        }
        private string GetMastersData(string productForMasterData, XmlDocument doc, string field, bool IsProduct)
        {
            string res = string.Empty;

            try
            {
                XmlDocument masters = new XmlDocument();
                //string strMastersData = System.Net.WebUtility.HtmlDecode(HttpUtility.HtmlDecode(doc.SelectSingleNode("ArrayOfTransaction/Masters").InnerXml));

                string strMastersData = HttpUtility.HtmlDecode(doc.SelectSingleNode("ArrayOfTransaction/Masters").InnerXml);

                //masters.LoadXml("<MastersExport>\r\n <MastersData>\r\n" + strMastersData);

                masters.LoadXml("<MastersExport><MastersData>" + strMastersData);

                XmlNodeList nodeList = masters.SelectNodes("MastersExport/MastersData");
                foreach (XmlNode node in nodeList)
                {
                    if (IsProduct)
                    {
                        if ("mCore_Product".Equals(node.SelectSingleNode("strMasterName").InnerText))
                        {
                            XmlNodeList nodeList1 = node.SelectNodes("arrMasterIdNameCode/MasterIdNameCode");
                            foreach (XmlNode node1 in nodeList1)
                            {
                                string iMasterId = node1.SelectSingleNode("iMasterId").InnerText;
                                if (productForMasterData.Equals(iMasterId))
                                {
                                    res = System.Net.WebUtility.HtmlDecode(node1.SelectSingleNode(field).InnerText);
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        XmlNodeList nodeList1 = node.SelectNodes("arrMasterIdNameCode/MasterIdNameCode");
                        foreach (XmlNode node1 in nodeList1)
                        {
                            string iMasterId = node1.SelectSingleNode("iMasterId").InnerText;
                            if (productForMasterData.Equals(iMasterId))
                            {
                                res = System.Net.WebUtility.HtmlDecode(node1.SelectSingleNode(field).InnerText);
                                break;
                            }
                        }
                    }
                    if (!string.IsNullOrEmpty(res))
                        break;
                }


            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetMastersData", ex.Message + " | " + ex.StackTrace);
                res = string.Empty;
            }

            return res;
        }
        private string GetAmountsData(XmlDocument xmlDocument, string root, string xmlField)
        {
            string res = string.Empty;
            XmlNode node = null;
            try
            {
                node = xmlDocument.SelectSingleNode(root);
                XmlNodeList nodeList = node.SelectNodes("decimal");
                if (nodeList.Count > 1)
                {
                    string val = string.Empty;
                    if (xmlField.Equals("decimal|Gross Amount"))
                        val = nodeList.Item(0).InnerText;
                    else
                        val = nodeList.Item(1).InnerText;
                    double dVal;
                    res = double.TryParse(val, out dVal) ? string.Format("{0:f2}", dVal) : val;
                    res = res.Replace("-", string.Empty);
                }
            }
            catch (Exception ex)
            {

                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetAmountsData", ex.Message + " | " + ex.StackTrace);
                res = string.Empty;
            }

            return res;
        }
        private string GetLogoName(string root, string XmlField)
        {
            string res = string.Empty;

            switch (XmlField)
            {
                case "Sumax Logo":
                    res = "sumax-logo-invoice.png";
                    break;
                case "Autokrom Logo":
                    res = "Autokrom.bmp";
                    break;
                default:
                    res = XmlField;
                    break;
            }

            return res;
        }

        private DyanmicInvoiceModel GetTemplateEntity(List<TemplateDetailResult> objModel)
        {
            DyanmicInvoiceModel invoiceModel = new DyanmicInvoiceModel();

            // Logo Section
            var logSec = new List<TemplateDetailResult>();
            logSec.AddRange(objModel.Where(r => r.SectionId == 1).OrderBy(r => r.SortOrder).ToList());
            //logSec.Add(new TemplateDetailResult() { FieldValue = "sumax-logo-invoice.png", DisplayField = "logo", SortOrder = 1, SectionId = 1 });
            invoiceModel.LogoSection = logSec;

            //Division Address Section
            var divAddSec = new List<TemplateDetailResult>();
            divAddSec.AddRange(objModel.Where(a => a.SectionId == 2).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.DivAddress = divAddSec;

            //Invoice PO Section
            var POInvoiceSec = new List<TemplateDetailResult>();
            POInvoiceSec.AddRange(objModel.Where(a => a.SectionId == 3).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.POInvoice = POInvoiceSec;

            // Buyer Address Section
            var BuyerAddSec = new List<TemplateDetailResult>();
            BuyerAddSec.AddRange(objModel.Where(a => a.SectionId == 4).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.BuyerAddress = BuyerAddSec;

            // Delivery Location Address Section
            var DelLocAddSec = new List<TemplateDetailResult>();
            DelLocAddSec.AddRange(objModel.Where(a => a.SectionId == 5).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.DelLocAddress = DelLocAddSec;

            // Product Details Section
            var ProdDetSec = new List<TemplateDetailResult>();
            ProdDetSec.AddRange(objModel.Where(a => a.SectionId == 6).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.ProdDetSec = ProdDetSec;

            // Gross Amount Section
            var GrossAmount = new List<TemplateDetailResult>();
            GrossAmount.AddRange(objModel.Where(a => a.SectionId == 7).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.GrossAmount = GrossAmount;

            // QR Code Section
            var QRSection = new List<TemplateDetailResult>();
            QRSection.AddRange(objModel.Where(a => a.SectionId == 8).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.QRSection = QRSection;

            // Amount In Words Section
            var AmountWordsSec = new List<TemplateDetailResult>();
            AmountWordsSec.AddRange(objModel.Where(a => a.SectionId == 9).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.AmountWordsSec = AmountWordsSec;

            // Transportation Details Section
            var TransportSection = new List<TemplateDetailResult>();
            TransportSection.AddRange(objModel.Where(a => a.SectionId == 10).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.TransportSection = TransportSection;

            // Compoany Bank Details Section
            var CompanyBankDetails = new List<TemplateDetailResult>();
            CompanyBankDetails.AddRange(objModel.Where(a => a.SectionId == 11).OrderBy(a => a.SortOrder).ToList());
            invoiceModel.CompanyBankDetails = CompanyBankDetails;

            // Declaration Section
            var DeclarationSection = new List<TemplateDetailResult>();
            DeclarationSection.Add(new TemplateDetailResult()
            {
                SectionId = 12,
                DisplayField = ApplicationSettings.Declaration,
                FieldValue = ApplicationSettings.Declarations,
                SortOrder = 1
            });
            invoiceModel.DeclarationSection = DeclarationSection;


            // Authorised Signature Section
            // for new company name
            var AuthorizedSection = new List<TemplateDetailResult>();
            AuthorizedSection.AddRange(objModel.Where(r => r.SectionId == 13).OrderBy(r => r.SortOrder).ToList());
            invoiceModel.AuthorizedSection = AuthorizedSection;

            //AuthorizedSection.Add(new TemplateDetailResult()
            //{
            //    SectionId = 13,
            //    FieldValue = ApplicationSettings.CompanyName,
            //    SortOrder = 1
            //});
            //AuthorizedSection.Add(new TemplateDetailResult()
            //{
            //    SectionId = 13,
            //    FieldValue = ApplicationSettings.AuthSign,
            //    SortOrder = 1
            //});

            //QrCode fields
            var ManageQrCodeSection = new List<TemplateDetailResult>();
            ManageQrCodeSection.AddRange(objModel.Where(r => r.SectionId == 14).OrderBy(r => r.SortOrder).ToList());
            invoiceModel.ManageQrCodeSection = ManageQrCodeSection;

            var ManageQrCodeProductSection = new List<TemplateDetailResult>();
            ManageQrCodeProductSection.AddRange(objModel.Where(r => r.TemplateOrder == 15).OrderBy(r => r.SortOrder).ToList());
            invoiceModel.ManageQrCodeProductSection = ManageQrCodeProductSection;

            return invoiceModel;
        }

        private string GetFieldValueIdNamePairTag(XmlDocument xmlDoc, string root, string nodeName)
        {
                string res = string.Empty;
            try
            {
                XmlNodeList nodelist = xmlDoc.SelectNodes(root);

                foreach (XmlNode node in nodelist)
                {
                    string tag = node.SelectSingleNode("Tag").InnerText;
                    string fieldId = node.SelectSingleNode("ID").InnerText;
                    string fieldName = node.SelectSingleNode("Name").InnerText;

                    string[] noDecimalCode = Convert.ToString(ConfigurationManager.AppSettings["nodecimalCode"]).Split('|');
                    string[] noDecimalText = Convert.ToString(ConfigurationManager.AppSettings["nodecimalText"]).Split('|');

                    if (fieldId.Trim() == nodeName.Trim() || fieldName.Trim() == nodeName.Trim())
                    {
                        res = tag;
                        if (fieldId == "67111444" || fieldName.ToLower() == "buyer_statename")
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId == "67110577" || fieldName.ToLower() == "placeofsupply")
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId == "67111424" || fieldName.ToLower() == "delivery_location_state_name")
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId == "67110597" || fieldName.ToLower() == "dispatch_through")
                        {
                            res = InvoiceDefaults.DispatchThrough(tag.Trim());
                        }
                        else if (fieldId == "67110822" || fieldName.ToLower() == "chargespercentage")
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId == "67111039" || fieldName.ToLower() == "transmode")
                        {
                            res = InvoiceDefaults.DispatchThrough(tag.Trim());
                        }
                        else if (fieldId == "67111428" || fieldName.ToLower() == "division_state_name")
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId == "67111549" || "buyer_state_name".Equals(fieldName.ToLower()))
                        {
                            res = InvoiceDefaults.StateName(tag.Trim());
                        }
                        else if (fieldId.Equals("67111411")
                            || fieldName.Equals("buyer_state_code", StringComparison.InvariantCultureIgnoreCase)
                            || fieldId.Equals("67111418")
                            || fieldName.Equals("delivery_location_state_code", StringComparison.InvariantCultureIgnoreCase))
                        {
                            foreach (XmlNode item in nodelist)
                            {
                                tag = item.SelectSingleNode("Tag").InnerText;
                                fieldId = item.SelectSingleNode("ID").InnerText;
                                fieldName = item.SelectSingleNode("Name").InnerText;

                                if (fieldId.Trim().Equals("67111411") || fieldName.Trim().Equals("Buyer_Gstin_No")
                                    || fieldId.Trim().Equals("67111419") || fieldName.Trim().Equals("Delivery_Location_Gstin_No"))
                                {
                                    res = !String.IsNullOrWhiteSpace(tag) && tag.Length >= 5 ? tag.Substring(0, 2) : "";
                                }
                            }

                        }
                        //else if (fieldId.Equals("67111391") || fieldName.Equals("Contact_Mobile_No") ||
                        //        fieldId.Equals("67110589") || fieldName.Equals("Customer_PO_No") ||
                        //        fieldId.Equals("67110590") || fieldName.Equals("Customer_PO_Date") ||
                        //        fieldId.Equals("67111402") || fieldName.Equals("Company_Account_No") ||
                        //        fieldId.Equals("67111403") || fieldName.Equals("Company_Ifsc_Code") ||
                        //        fieldId.Equals("67111040") || fieldName.Equals("VehicleNo") ||
                        //        fieldId.Equals("67110579") || fieldName.Equals("Credit_Days"))
                        else if (noDecimalCode.Contains(fieldId) || noDecimalText.Contains(fieldName))
                        {
                            res = tag;
                        }
                        //else if(fieldId == "67111399" || fieldName.ToLower() == "Round_Off_Value")
                        //{
                        //    double iTag;
                        //    res = double.TryParse(tag.Trim(), out iTag) ? string.Format("{0:f2}", iTag) : tag;
                        //}
                        else
                        {
                            double iTag;
                            res = double.TryParse(tag.Trim(), out iTag) ? string.Format("{0:f2}", iTag) : tag;
                        }
                        break;
                    }
                }
                if (!string.IsNullOrEmpty(res))
                    res = res.Replace(",,", ",").Replace(",\r\n,", ",\r\n").Replace("CIN", "\nCIN");
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetFieldValueIdNamePairTag", ex.Message + " | " + ex.StackTrace);
            }
            return res;
        }

        private string SearchFieldTextByXMLTag(XmlDocument xmlDoc, string root, string nodeName)
        {
            string Fields = nodeName;
            string FieldVal = string.Empty;
            try
            {
                if (Fields.Contains("|"))
                {
                    if (Fields.Equals("67111408|Amount_In_Words"))
                    {
                        string val = GetAmountsData(xmlDoc, "ArrayOfTransaction/Transaction/BodyData/TransBody/Amounts", "decimal|Total Amount");
                        double dVal = 0;
                        if (double.TryParse(val, out dVal))
                            FieldVal = NumberToWords.ConvertNumberToWords(dVal);
                        else
                            FieldVal = val;
                    }
                    else
                        FieldVal = GetFieldValueIdNamePairTag(xmlDoc, root, Fields.Split('|')[1]);

                    if (string.IsNullOrEmpty(FieldVal))
                    {
                        FieldVal = GetFieldValueIdNamePairTag(xmlDoc, root, Fields.Split('|')[0]);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "GetFieldValueIdNamePairTag", ex.Message + " | " + ex.StackTrace);
            }
            return FieldVal;
        }

        #region "Unused code"
        //private string GetFieldValueIdNamePairTag(XmlDocument xmlDoc, string root, string nodeName)
        //{
        //    string res = string.Empty;
        //    XmlNodeList nodelist = xmlDoc.SelectNodes(root);

        //    foreach (XmlNode node in nodelist)
        //    {
        //        string tag = node.SelectSingleNode("Tag").InnerText;
        //        if (!string.IsNullOrEmpty(nodeName))
        //            switch (nodeName.Trim().ToLower())
        //            {
        //                case "snarration":
        //                case "67110576":
        //                    res = tag;
        //                    break;
        //                case "customer_po_no":
        //                case "67110589":
        //                    res = tag;
        //                    break;

        //                case "customer_po_date":
        //                case "67110590":
        //                    res = tag;
        //                    break;

        //                case "po_valid_date":
        //                case "67110591":
        //                    res = tag;
        //                    break;

        //                case "dc_no":
        //                case "67110592":
        //                    res = tag;
        //                    break;

        //                case "dc_date":
        //                case "67110593":
        //                    res = tag;
        //                    break;
        //                case "vendor_regn__code":
        //                case "67110594":
        //                    res = tag;
        //                    break;

        //                case "buyer_address":
        //                case "67111409":
        //                    res = tag;
        //                    break;

        //                case "buyer_gstin_no":
        //                case "67111410":
        //                    res = tag;
        //                    break;

        //                case "buyer_state_code":
        //                case "67111411":
        //                    res = tag;
        //                    break;

        //                case "buyer_statename":
        //                case "67111444":
        //                    res = InvoiceDefaults.StateName(tag.Trim());
        //                    break;

        //                case "contact_person":
        //                case "67110595":
        //                    res = tag;
        //                    break;

        //                case "freight_applicable":
        //                case "67110596":
        //                    res = tag;
        //                    break;

        //                case "contact_mobile_no":
        //                case "67111391":
        //                    res = tag;
        //                    break;

        //                case "isigst":
        //                case "67110578":
        //                    res = tag;
        //                    break;

        //                case "placeofsupply":
        //                case "67110577":
        //                    res = InvoiceDefaults.StateName(tag.Trim());
        //                    break;

        //                case "delivery_location_address1":
        //                case "67111414":
        //                    res = tag;
        //                    break;

        //                case "delivery_location_address2":
        //                case "67111415":
        //                    res = tag;
        //                    break;

        //                case "delivery_location_gstin_no":
        //                case "67111419":
        //                    res = tag;
        //                    break;

        //                case "delivery_location_state_code":
        //                case "67111418":
        //                    res = tag;
        //                    break;



        //                case "transporter":
        //                case "67110587":
        //                    res = tag;
        //                    break;

        //                case "vehicleno":
        //                case "67111040":
        //                    res = tag;
        //                    break;

        //                case "approxkm":
        //                case "67111041":
        //                    res = tag;
        //                    break;

        //                case "ewaybillno":
        //                case "67111042":
        //                    res = tag;
        //                    break;

        //                case "transname":
        //                case "67111043":
        //                    res = tag;
        //                    break;

        //                case "transid":
        //                case "67111044":
        //                    res = tag;
        //                    break;

        //                case "transdate":
        //                case "67111046":
        //                    res = tag;
        //                    break;

        //                case "transdocno":
        //                case "67111045":
        //                    res = tag;
        //                    break;

        //                case "supplytype":
        //                case "67111047":
        //                    res = tag;
        //                    break;

        //                case "subtype":
        //                case "67111048":
        //                    res = tag;
        //                    break;

        //                case "doctype":
        //                case "67111049":
        //                    res = tag;
        //                    break;

        //                case "refdoc":
        //                case "67111228":
        //                    res = tag;
        //                    break;

        //                case "refdate":
        //                case "67111229":
        //                    res = tag;
        //                    break;

        //                case "company_account_no":
        //                case "67111402":
        //                    res = tag;
        //                    break;

        //                case "comapny_bank_details":
        //                case "67111392":
        //                    res = tag;
        //                    break;

        //                case "contact_person_name":
        //                case "67111397":
        //                    res = tag;
        //                    break;

        //                case "fright_value":
        //                case "67111398":
        //                    res = tag;
        //                    break;

        //                case "round_off_value":
        //                case "67111399":
        //                    res = tag;
        //                    break;

        //                case "company_ifsc_code":
        //                case "67111403":
        //                    res = tag;
        //                    break;

        //                case "company_bank_branch_name":
        //                case "67111404":
        //                    res = tag;
        //                    break;

        //                case "GSt_":
        //                case "67111406":
        //                    res = tag;
        //                    break;

        //                case "branch":
        //                case "67111407":
        //                    res = tag;
        //                    break;

        //                case "amount_in_words":
        //                case "67111408":
        //                    res = tag;
        //                    break;


        //                case "credit_days":
        //                case "67110579":
        //                    res = tag;
        //                    break;

        //                case "businesstypes":
        //                case "67110580":
        //                    res = tag;
        //                    break;

        //                case "payment_type":
        //                case "67110583":
        //                    res = tag;
        //                    break;

        //                case "deliver_note":
        //                case "67111413":
        //                    res = tag;
        //                    break;

        //                case "po_item_serial_no":
        //                case "67111432":
        //                    res = tag;
        //                    break;

        //                case "gate_location":
        //                case "67111435":
        //                    res = tag;
        //                    break;

        //                case "unit_uom_":
        //                case "67111436":
        //                    res = tag;
        //                    break;

        //                case "cuurency_name":
        //                case "67111437":
        //                    res = tag;
        //                    break;

        //                case "units":
        //                case "67111438":
        //                    res = tag;
        //                    break;

        //                case "division_address":
        //                case "67111426":
        //                    res = tag;
        //                    break;

        //                case "division_address_1":
        //                case "67111427":
        //                    res = tag;
        //                    break;

        //                case "division_state_name":
        //                case "67111428":
        //                    res = InvoiceDefaults.StateName(tag);
        //                    break;

        //                case "division_state_code":
        //                case "67111429":
        //                    res = tag;
        //                    break;

        //                case "divivsion_email":
        //                case "67111430":
        //                    res = tag;
        //                    break;
        //                case "division_gstin_no":
        //                case "67111441":
        //                    res = tag;
        //                    break;

        //                case "lr_number":
        //                case "67110584":
        //                    res = tag;
        //                    break;

        //                case "reference_no_":
        //                case "67110586":
        //                    res = tag;
        //                    break;

        //                case "lr_date":
        //                case "67110585":
        //                    res = tag;
        //                    break;

        //                default:
        //                    break;
        //            }
        //    }
        //    return res;
        //}

        #endregion
    }
}