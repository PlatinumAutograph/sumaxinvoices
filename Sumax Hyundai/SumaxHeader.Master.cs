﻿using sumax.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sumax_Hyundai
{
    public partial class SumaxHeader : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string useremail = Convert.ToString(HttpContext.Current.Session["UserEmail"]);
            if (string.IsNullOrEmpty(useremail) || !HttpContext.Current.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                Session.Clear();
                Session.Abandon();

                // clear authentication cookie
                HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
                cookie1.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie1);

                // clear session cookie (not necessary, but i would recommend to do it anyway)
                SessionStateSection sessionStateSection = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
                HttpCookie cookie2 = new HttpCookie(sessionStateSection.CookieName, "");
                cookie2.Expires = DateTime.Now.AddYears(-1);
                Response.Cookies.Add(cookie2);

                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                welcomeuser.InnerText = Convert.ToString(Session["UserName"]);
                bool isAdmin = Convert.ToString(Session["UserType"]) == "A";
                if(isAdmin)
                {
                    //liqrcodemgment.Visible = true;
                    liusermgment.Visible = true;
                    litemplatemgment.Visible = true;
                }
                //check if admin and display invoice management
            }
        }

        protected void Logout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();

        }
    }
}