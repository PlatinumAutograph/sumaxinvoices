﻿using sumax.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sumax.Models;
using System.Text;
using System.Drawing;
using System.Web.Services;
using Sumax_Hyundai.DAL;
using Sumax_Hyundai.BAL;
using sumax;
using System.Web.Security;

namespace Sumax_Hyundai
{
    public partial class InvoiceManagement : System.Web.UI.Page
    {
        EntityDAL entityDAL = null;
        InvoiceBAL objInvoiceBAL = null;
        bool isAdmin = false;
        string TemplateId = "0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                //string ss = Helper.GetPasswordHash();
                isAdmin = Convert.ToString(Session["UserType"]).Equals("A", StringComparison.InvariantCultureIgnoreCase);
                TemplateId = Convert.ToString(Session["TemplateId"]);
                entityDAL = new EntityDAL();
                objInvoiceBAL = new InvoiceBAL();

                if (!IsPostBack)
                {
                    //if (isAdmin)
                    BindTemplates();

                    //else
                    //{
                    //string Templateid = Convert.ToString(Session["TemplateId"]);
                    // if (!string.IsNullOrEmpty(Templateid))
                    //tbodyInvoice.InnerHtml = BindInvoiceTable(Convert.ToInt32(Templateid));
                    //}
                }
            }
        }
        protected void importButton_Click(object sender, EventArgs e)
        {
            string Templateid = string.Empty;
            int UserId = 0;
            if (Session["UserId"] != null)
            {
                string strUserId = Convert.ToString(Session["UserId"]);
                int.TryParse(strUserId, out UserId);
            }

            // if (isAdmin)
            Templateid = ddlTemplate.SelectedValue;
            // else
            // Templateid = Convert.ToString(Session["TemplateId"]);
            if (!string.IsNullOrEmpty(Templateid))
            {
                lblMessage.Text = string.Empty;
                if (fileUpload.HasFile)
                {
                    List<tbl_FilesData> _lstFilesData = new List<tbl_FilesData>();
                    Utility utility = new Utility();

                    tbl_FilesData file = null;
                    int iUploadedCnt = 0;
                    HttpFileCollection hfc = Request.Files;

                    for (int i = 0; i < hfc.Count; i++)
                    {
                        file = new tbl_FilesData();
                        HttpPostedFile hpf = hfc[i];
                        try
                        {
                            if (hpf.ContentLength > 0)
                            {
                                byte[] data = null;
                                using (var binaryReader = new BinaryReader(hpf.InputStream))
                                    data = binaryReader.ReadBytes(hpf.ContentLength);

                                string InvoiceNumber = utility.ReadDataFromBytes(data);

                                file.TemplateId = Convert.ToInt32(Templateid);
                                file.FileName = Path.GetFileNameWithoutExtension(hpf.FileName);
                                file.Extension = Path.GetExtension(hpf.FileName);
                                file.InvoiceNo = InvoiceNumber;
                                file.Status = "Received";
                                file.ImportedDate = DateTime.UtcNow;
                                file.Data = data;
                                file.UserId = UserId;

                                _lstFilesData.Add(file);

                                iUploadedCnt += 1;
                            }
                        }
                        catch (Exception ex)
                        {
                            lblMessage.Text = " Could not upload file - " + hpf.FileName;
                            Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "importButton_Click", ex.StackTrace);
                        }
                    }
                    if (_lstFilesData != null && _lstFilesData.Count > 0)
                    {
                        bool flag = entityDAL.InsertFileData(_lstFilesData);
                        if (flag)
                        {
                            lblMessage.Text = " Total " + iUploadedCnt + " File(s) Uploaded Successfully";
                            if (!string.IsNullOrEmpty(Templateid))
                                tbodyInvoice.InnerHtml = BindInvoiceTable(Convert.ToInt32(Templateid));
                        }
                        else
                            lblMessage.Text = "There was an Error in importing File(s)";
                    }
                    else
                        lblMessage.Text = "There was an Error in importing File(s)";
                }
                else
                {
                    lblMessage.Text = "No file(s) selected.";
                }
            }
            else
            {
                lblMessage.Text = "Please select template";
                ddlTemplate.Focus();
            }
        }

        public string BindInvoiceTable(int TemplateId)
        {
            StringBuilder sbData = new StringBuilder();
            int UserId = 0;
            if (Session["UserId"] != null)
            {
                string strUserId = Convert.ToString(Session["UserId"]);
                int.TryParse(strUserId, out UserId);
            }


            List<tbl_FilesData> _lstFiles = entityDAL.GetListFiles(TemplateId, UserId, isAdmin);

            string DomainName = HttpContext.Current.Request.Url.Host;

            string host = "";

           

            foreach (tbl_FilesData file in _lstFiles)
            {
                sbData.Append("<tr>");
                sbData.Append("<td>F" + file.FileId + "</td><td>" + Path.GetFileNameWithoutExtension(file.FileName));
                sbData.Append("</td><td>" + file.InvoiceNo + "</td><td>" + file.ImportedDate + "</td><td>" + file.ProcessDate + "</td>");
                sbData.Append("<td>" + file.Status + "</td>");

                switch (file.Status.ToLower())
                {
                    case "received":
                        sbData.Append("<td><button type='button' onclick='GeneratePDF(" + file.FileId + ")' class='btn btn-success btn-xs'>Generate Invoice</button></td>");
                        break;
                    case "in - progress":
                        sbData.Append("<td><button type='button' class='btn btn-info btn-xs'>N/A(In Progress)</button></td>");
                        break;
                    case "error":
                        sbData.Append("<td><button type='button' class='btn btn-danger btn-xs'>Error</button>");
                        sbData.Append("&nbsp;<button type='button' onclick='GeneratePDF(" + file.FileId + ")' class='btn btn-success btn-xs'>Re-Generate Invoice</button></td>");
                        break;
                    case "completed":
                        sbData.Append("<td>");
                        //sbData.Append("<a type='button' class='btn btn-primary btn-xs' onclick='Download(" + file.FileId + ")' href='#'>Download</a>");

                        foreach (string invoiceType in InvoiceTypes.InvoiceType)
                            sbData.Append("&nbsp;<a type='button' title='print' class='btn btn-primary btn-xs'  href='javascript: w = window.open(\"" + string.Format("pdf/{0}_{1}_{3}_{2}.pdf", file.FileName, file.InvoiceNo, invoiceType, file.FileId) + "\"); w.print(); '>Print " + invoiceType + "</a>");
                        string pdfpath = HttpContext.Current.Server.MapPath("~/pdf");
                        string zipName = string.Format("{0}_{1}.zip", "All", file.FileId);
                        if (File.Exists(Path.Combine(pdfpath, zipName)))
                        {
                            sbData.Append("<a type='button' title='download' class='btn btn-success btn-xs'  href='javascript: w = window.open(\"" + string.Format("pdf/{0}_{1}.zip", "All", file.FileId) + "\"); w.print(); '>Download All</a>");
                        }
                        else
                        {
                            sbData.Append("<button type='button' class='btn btn-success btn-xs' onclick='DownloadAll(" + file.FileId + ",\"" + file.FileName + "\",\"" + file.InvoiceNo + "\")'>Generate Zip</button>");
                        }

                        sbData.Append("</td>");
                        break;
                    default:
                        sbData.Append("<td></td>");
                        break;
                }

                sbData.Append("</tr>");
            }


            return sbData.ToString();
        }

     

        private void BindTemplates()
        {

            List<tbl_templates> _lstTemplates = new List<tbl_templates>();
            if (isAdmin)
                _lstTemplates = objInvoiceBAL.GetAllTemplates();
            else
            {
                string strTemplateIds = Convert.ToString(Session["TemplateId"]);
                if (!string.IsNullOrEmpty(strTemplateIds))
                    _lstTemplates = objInvoiceBAL.GetAllTemplates(strTemplateIds);
            }

            ddlTemplate.DataSource = _lstTemplates;
            ddlTemplate.DataTextField = "InvoiceTemplate";
            ddlTemplate.DataValueField = "Id";
            //ddlTemplate.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Template" });
            ddlTemplate.DataBind();

            if (Session["Selectedtemplate"] != null)
                ddlTemplate.SelectedValue = Convert.ToString(Session["Selectedtemplate"]);
            //ddlTemplate.Visible = true;
            //divChooseTemplate.Visible = true;
            string Templateid = string.Empty;
            //if (isAdmin)
            Templateid = ddlTemplate.SelectedValue;


            //else
            //Templateid = Convert.ToString(Session["TemplateId"]);
            if (!string.IsNullOrEmpty(Templateid))
                tbodyInvoice.InnerHtml = BindInvoiceTable(Convert.ToInt32(Templateid));
        }

        protected void ddlTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            string Templateid = string.Empty;
            //if (isAdmin)
            Templateid = ddlTemplate.SelectedValue;
            Session["Selectedtemplate"] = Templateid;
            //else
            // Templateid = Convert.ToString(Session["TemplateId"]);
            if (!string.IsNullOrEmpty(Templateid))
                tbodyInvoice.InnerHtml = BindInvoiceTable(Convert.ToInt32(Templateid));
        }
    }
}