﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using sumax;
using sumax.DAL;
using sumax.Models;
using Sumax_Hyundai.BAL;
using Sumax_Hyundai.Models;

namespace Sumax_Hyundai
{
    public partial class template : System.Web.UI.Page
    {
        private InvoiceBAL invoiceBAL = new InvoiceBAL();
        Utility Utilities = new Utility();

        ArrayList ar1 = new ArrayList();
        ArrayList ar2 = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                bool isAdmin = Convert.ToString(Session["UserType"]).Equals("A", StringComparison.InvariantCultureIgnoreCase);
                if (isAdmin)
                {
                    string vendorId = Request.QueryString["VendorId"] == null ? string.Empty : Convert.ToString(Request.QueryString["VendorId"]);

                    bool btnEnable = false;

                    if (ddlVendors.SelectedIndex > 0 && ddlSection.SelectedIndex > 0)
                        btnEnable = true;

                    btnAllRight.Enabled = btnEnable;
                    btnRight.Enabled = btnEnable;
                    btnLeft.Enabled = btnEnable;
                    btnAllLeft.Enabled = btnEnable;
                    btnUp.Enabled = btnEnable;
                    btnDown.Enabled = btnEnable;


                    if (!IsPostBack)
                    {
                        if (vendorId != "")
                        {
                            ddlVendors.SelectedValue = vendorId;
                        }
                        LoadVendors();
                        BindSections();
                        BindXMLFields();

                        //invoiceBAL.GenerateInvoiceForTemplate(1);
                    }
                }
                else
                    Response.Redirect("/Default.aspx");
            }
        }

        #region Methods

        private void BindSections()
        {
            try
            {
                ddlSection.DataSource = invoiceBAL.GetSectionsList();
                ddlSection.DataTextField = "Section";
                ddlSection.DataValueField = "SectionImgUrl";
                ddlSection.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Section" });
                ddlSection.DataBind();
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "BindSections", ex.Message + " | " + ex.StackTrace);

            }
        }

        private void LoadVendors()
        {
            try
            {
                ddlVendors.DataSource = invoiceBAL.GetVendorsList();
                ddlVendors.DataTextField = "VendorName";
                ddlVendors.DataValueField = "Id";
                ddlVendors.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Vendor" });
                ddlVendors.DataBind();
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "LoadVendors", ex.Message + " | " + ex.StackTrace);

            }
        }

        private void BindXMLFields()
        {
            try
            {
                lstBox1.DataSource = InvoiceBAL.GetXmlFields();
                lstBox1.DataTextField = "Name";
                lstBox1.DataValueField = "ID";
                lstBox1.DataBind();
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "BindXMLFields", ex.Message + " | " + ex.StackTrace);

            }
        }

        private void BindXmlFieldsToControl(ListBox lstBox, List<XMLFields> list)
        {
            try
            {
                lstBox.DataSource = list;
                lstBox.DataTextField = "Name";
                lstBox.DataValueField = "ID";
                lstBox.DataBind();
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "BindXmlFieldsToControl", ex.Message + " | " + ex.StackTrace);

            }
        }

        public string BindVendors()
        {
            StringBuilder sbData = new StringBuilder();
            try
            {
                List<tbl_Vendors> list = invoiceBAL.GetVendorsList();

                sbData.Append("<ul class='customer-list' id='menuVendors'>  ");
                //sbData.Append("<li class='ripple'>");
                //sbData.Append("<a id='' data-toggle='collapse' data-target='#ri'>Regular Invoice&nbsp;&nbsp;&nbsp;<i class='fas fa-minus-circle'></i> </a>");
                //sbData.Append(" <ul id='ri' class='collapse in submenu'>  <li><a href='#'>Add / Remove Field</a> </li> ");
                //sbData.Append("<li><a href='/RenameFields.aspx'>Rename Fields</a></li> </ul> </li> ");

                foreach (var item in list)
                {
                    sbData.Append("<li class='ripple'>");
                    sbData.Append("<a id='' data-toggle='collapse' data-target='#" + item.Id + "'>" + item.VendorName + " Invoice&nbsp;&nbsp;&nbsp;<i class='fas fa-plus-circle'></i> </a>");
                    sbData.Append(" <ul id='" + item.Id + "' class='collapse submenu'>  ");
                    sbData.Append("  <li><a href='template.aspx?VendorId=" + Convert.ToString(item.Id) + "'>Add / Remove Field</a> </li> ");
                    sbData.Append(" <li><a href='RenameFields.aspx?VendorId=" + Convert.ToString(item.Id) + "'>Rename Fields</a></li> </ul> ");
                }
                sbData.Append(" </ul>");
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "BindVendors", ex.Message + " | " + ex.StackTrace);

            }
            return sbData.ToString();
        }

        private void ResetControls()
        {
            ddlVendors.ClearSelection();
            ddlSection.ClearSelection();
            lstBox1.Items.Clear();
            lstBox2.Items.Clear();
        }

        private void MoveUp(ListBox myListBox)
        {
            try
            {
                int selectedIndex = myListBox.SelectedIndex;
                if (selectedIndex > 0)
                {
                    myListBox.Items.Insert(selectedIndex - 1, myListBox.Items[selectedIndex]);
                    myListBox.Items.RemoveAt(selectedIndex + 1);
                    myListBox.SelectedIndex = selectedIndex - 1;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "MoveUp", ex.Message + " | " + ex.StackTrace);
            }
        }

        private void MoveDown(ListBox myListBox)
        {
            try
            {
                int selectedIndex = myListBox.SelectedIndex;
                if (selectedIndex < myListBox.Items.Count - 1 & selectedIndex != -1)
                {
                    myListBox.Items.Insert(selectedIndex + 2, myListBox.Items[selectedIndex]);
                    myListBox.Items.RemoveAt(selectedIndex);
                    myListBox.SelectedIndex = selectedIndex + 1;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "MoveDown", ex.Message + " | " + ex.StackTrace);

            }
        }

        #endregion

        #region Button events

        protected void btnAllRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSection.SelectedIndex > 0)
                {
                    while (lstBox1.Items.Count != 0)
                    {
                        for (int i = 0; i < lstBox1.Items.Count; i++)
                        {
                            lstBox2.Items.Add(lstBox1.Items[i]);
                            lstBox1.Items.Remove(lstBox1.Items[i]);
                        }
                    }
                }
                else
                {
                    //lblMesage.InnerText = "Please select section";
                    ddlSection.Focus();
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "ddlVendors_SelectedIndexChanged", ex.Message + " | " + ex.StackTrace);
            }
        }

        protected void btnRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSection.SelectedIndex > 0)
                {
                    for (int i = 0; i < lstBox1.Items.Count; i++)
                    {
                        if (lstBox1.Items[i].Selected)
                        {
                            if (!ar1.Contains(lstBox1.Items[i]))
                            {
                                ar1.Add(lstBox1.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < ar1.Count; i++)
                    {
                        if (!lstBox2.Items.Contains(((ListItem)ar1[i])))
                        {
                            lstBox2.Items.Add(((ListItem)ar1[i]));
                        }
                        lstBox1.Items.Remove(((ListItem)ar1[i]));
                    }
                    lstBox2.SelectedIndex = -1;
                }
                else
                {
                    //lblMesage.InnerText = "Please select section";
                    ddlSection.Focus();
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "ddlVendors_SelectedIndexChanged", ex.Message + " | " + ex.StackTrace);

            }
        }

        protected void btnAllLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSection.SelectedIndex > 0)
                {
                    while (lstBox2.Items.Count != 0)
                    {
                        for (int i = 0; i < lstBox2.Items.Count; i++)
                        {
                            lstBox1.Items.Add(lstBox2.Items[i]);
                            lstBox2.Items.Remove(lstBox2.Items[i]);
                        }
                    }
                }
                else
                {
                    //lblMesage.InnerText = "Please select section";
                    ddlSection.Focus();
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "btnAllLeft_Click", ex.Message + " | " + ex.StackTrace);

            }
        }

        protected void btnLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlSection.SelectedIndex > 0)
                {
                    for (int i = 0; i < lstBox2.Items.Count; i++)
                    {
                        if (lstBox2.Items[i].Selected)
                        {
                            if (!ar2.Contains(lstBox2.Items[i]))
                            {
                                ar2.Add(lstBox2.Items[i]);
                            }
                        }
                    }
                    for (int i = 0; i < ar2.Count; i++)
                    {
                        if (!lstBox1.Items.Contains(((ListItem)ar2[i])))
                        {
                            lstBox1.Items.Add(((ListItem)ar2[i]));
                        }
                        lstBox2.Items.Remove(((ListItem)ar2[i]));
                    }
                    lstBox1.SelectedIndex = -1;
                }
                else
                {
                    //lblMesage.InnerText = "Please select section";
                    ddlSection.Focus();
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "btnLeft_Click", ex.Message + " | " + ex.StackTrace);

            }
        }


        #endregion;

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlVendors.SelectedIndex > 0 && ddlSection.SelectedIndex > 0)
                {
                    int VendorId = Convert.ToInt32(ddlVendors.SelectedValue);

                    int SectionId = invoiceBAL.GetSectionId(ddlSection.SelectedItem.Text);

                    List<TemplateDetails> list = new List<TemplateDetails>();

                    if (lstBox2.Items.Count > 0)
                    {
                        for (int i = 0; i < lstBox2.Items.Count; i++)
                        {
                            list.Add(new TemplateDetails()
                            {
                                DisplayFields = lstBox2.Items[i].Text,
                                XmlFields = lstBox2.Items[i].Text,
                                SectionId = SectionId,
                                SortOrder = i + 1
                            });
                        }
                    }
                    tbl_Vendors _Vendors = invoiceBAL.GetVendor(VendorId);
                    if (_Vendors != null)
                    {
                        int TempId = invoiceBAL.CheckIfTemplateDetailsExistsOrNot(VendorId);
                        if (TempId != 0)
                        {
                            TemplateModel templateModel = new TemplateModel(VendorId, TempId, _Vendors.VendorName + " Invoice Template", list);
                            invoiceBAL.UpdateTemplate(templateModel);
                        }
                        else
                        {
                            TemplateModel templateModel = new TemplateModel(VendorId, _Vendors.VendorName + " Invoice Template", list);
                            invoiceBAL.InsertTemplate(templateModel);
                        }

                        ResetControls();

                        Response.Redirect("RenameFields.aspx?VendorId=" + VendorId);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "btnSave_Click", ex.Message + " | " + ex.StackTrace);
                //throw;
            }
        }

        protected void ddlVendors_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ddlSection.ClearSelection();
                while (lstBox2.Items.Count != 0)
                {
                    for (int i = 0; i < lstBox2.Items.Count; i++)
                    {
                        lstBox1.Items.Add(lstBox2.Items[i]);
                        lstBox2.Items.Remove(lstBox2.Items[i]);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "ddlVendors_SelectedIndexChanged", ex.Message + " | " + ex.StackTrace);

            }
        }

        protected void ddlSection_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                sectionImage.Src = ddlSection.SelectedValue;
                while (lstBox2.Items.Count != 0)
                {
                    for (int i = 0; i < lstBox2.Items.Count; i++)
                    {
                        lstBox1.Items.Add(lstBox2.Items[i]);
                        lstBox2.Items.Remove(lstBox2.Items[i]);
                    }
                }

                if (ddlVendors.SelectedIndex > 0 && ddlSection.SelectedIndex > 0)
                {
                    int SectionId = invoiceBAL.GetSectionId(ddlSection.SelectedItem.Text);
                    var leftedXmlFields = invoiceBAL.GetLeftedFields(Convert.ToInt32(ddlVendors.SelectedValue), SectionId);
                    var mappedXmlFields = invoiceBAL.GetMappedFields(Convert.ToInt32(ddlVendors.SelectedValue), SectionId);
                    BindXmlFieldsToControl(lstBox1, leftedXmlFields);
                    BindXmlFieldsToControl(lstBox2, mappedXmlFields);
                    //btnAllRight.Enabled = true;
                    //btnRight.Enabled = true;
                    //btnLeft.Enabled = true;
                    //btnAllLeft.Enabled = true;
                    //btnUp.Enabled = true;
                    //btnDown.Enabled = true;

                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Ex", "ddlSection_SelectedIndexChanged", ex.Message + " | " + ex.StackTrace);
            }
        }

        protected void btnUp_Click(object sender, EventArgs e)
        {
            MoveUp(lstBox2);
        }

        protected void btnDown_Click(object sender, EventArgs e)
        {
            MoveDown(lstBox2);
        }
    }
}