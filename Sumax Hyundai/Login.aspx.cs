﻿using Sumax_Hyundai.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sumax_Hyundai
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string passwordHash = sumax.DAL.Helper.GetPasswordHash("12345");
            if (!IsPostBack)
            {
                if (Request.Cookies["UserMail"] != null)
                {
                    fieldUser.Text = Request.Cookies["UserMail"].Value;
                    //fieldPassword.Text = Request.Cookies["UserPassword"].Value;
                    chkRememberMe.Checked = true;
                }
            }
            if (this.Page.User.Identity.IsAuthenticated)
            {
                if (Session["UserEmail"] != null)
                    FormsAuthentication.RedirectFromLoginPage(Convert.ToString(Session["UserEmail"]), true);
                else
                {
                    FormsAuthentication.SignOut();
                    FormsAuthentication.RedirectToLoginPage();
                }

            }
           
        }
        protected void Loginbtn_Click(object sender, EventArgs e)
        {
            string email = fieldUser.Text.Trim();
            string password = fieldPassword.Text;
            if (!string.IsNullOrEmpty(email) && !string.IsNullOrEmpty(password))
            {
                if (chkRememberMe.Checked)
                    Response.Cookies["UserMail"].Expires = DateTime.Now.AddMonths(1);
                else
                    Response.Cookies["UserMail"].Expires = DateTime.Now.AddDays(-1);
                Response.Cookies["UserMail"].Value = fieldUser.Text.Trim();

                tbl_Users _Users = null;

                if (new EntityDAL().ValidateLogin(email, password, out _Users))
                {
                    Session["UserEmail"] = _Users.Email;
                    Session["UserId"] = _Users.UserId;
                    Session["UserName"] = string.Format("{0} {1}", _Users.FirstName, _Users.LastName);
                    Session["UserType"] = _Users.UserType;
                    Session["TemplateId"] = _Users.TemplateId;
                    FormsAuthentication.RedirectFromLoginPage(_Users.Email, true);
                }
                else
                {
                    ErrorLabel.Text = "Invalid Login";
                }
            }
            else
            {
                if (string.IsNullOrEmpty(email))
                    ErrorLabel.Text = "Email should not be blank";
                else if (string.IsNullOrEmpty(password))
                    ErrorLabel.Text = "Password should not be blank";
                else
                {
                    ErrorLabel.Text = "Invalid Login";
                }
            }
        }
    }
}