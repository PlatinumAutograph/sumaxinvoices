﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SumaxHeader.Master" AutoEventWireup="true" CodeBehind="template.aspx.cs" Inherits="Sumax_Hyundai.template" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script src="assets/js/jquery-1.12.4.min.js"></script>

     <div id="sidebar" class="nav-collapse">
                <!-- sidebar menu start-->
        
                    <%= BindVendors() %>

                    <%-- <ul class="customer-list" id="menuVendors">
                    <li class="ripple">
                        <a id="" data-toggle="collapse" data-target="#ri">Regular Invoice&nbsp;&nbsp;&nbsp;<i class="fas fa-minus-circle"></i>
                        </a>
                        <ul id="ri" class="collapse in submenu">
                            <li><a href="#">Add / Remove Field</a> </li>
                            <li><a href="/RenameFields.aspx">Rename Fields</a></li>
                        </ul>
                    </li>
                    <li class="ripple">
                        <a id="" data-toggle="collapse" data-target="#hynd">Hyundai Invoice&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                        </a>
                        <ul id="hynd" class="collapse submenu">
                            <li><a href="#">Add / Remove Field</a> </li>
                            <li><a href="/RenameFields.aspx">Rename Fields</a></li>
                        </ul>
                    </li>
                    <li class="ripple">
                        <a id="">TATA Invoice&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                        </a>
                    </li>
                    <li class="ripple">
                        <a id="">Nissan Invoice&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                        </a>
                    </li>

                </ul>--%>
                <!-- sidebar menu end-->
            </div>
    <section id="main-content">
        <section class="content-wrapper">
            <div id="">

                <section class="panel">
                    <header class="panel-heading">
                        <b><span id="">Add New Field</span></b>
                    </header>
                    <div class="panel-body">
                        
                            <form class="form-horizontal" action="/action_page.php">
                                <div class="row">
                                <div class="form-group">
                                <label class="control-label col-sm-2 col-sm-offset-2" for="chooseSection">Choose Vendor:</label>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlVendors" runat="server" CssClass="form-control" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVendors_SelectedIndexChanged" AutoPostBack="true" >
                                    </asp:DropDownList>
                                   
                                </div>
                                 <div class="col-sm-4">
                                    <button type="button" id="CreateNewVendor" class="btn btn-link" data-toggle="modal" data-target="#myModal2" style="text-decoration:underline;font-size: 11px;"><b>Create New Vendor</b></button>
                                </div>
                            </div>
                                    </div>
                                <div class="row">
                            <div class="form-group">
                                <label class="control-label col-sm-2 col-sm-offset-2" for="chooseSection">Choose Section:</label>
                                <div class="col-sm-4">

                                    <asp:DropDownList ID="ddlSection" runat="server" CssClass="form-control" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlSection_SelectedIndexChanged" AutoPostBack="true" >
                                    </asp:DropDownList>
                                   
                                </div>
                                 <div class="col-sm-4">
                                    <button type="button" id="preview" class="btn btn-link" data-toggle="modal" data-target="#myModal1" style="text-decoration:underline;font-size: 11px;"><b>Preview Selected Section</b></button>
                                </div>
                            </div></div>
                                </form>
                             
                        <div class="row">
                            <div class="row style-select">
                                <div class="col-md-12">
                                    <div class="subject-info-box-1">
                                        <label class="fields-label">Available Fields</label>
                                        
                                        <asp:ListBox ID="lstBox1" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                                    </div>

                                    <div class="subject-info-arrows text-center">
                                        <br />
                                        <br />
                                        <asp:Button ID="btnAllRight" runat="server" Text=">>"  CssClass="btn btn-default" OnClick="btnAllRight_Click"  /> <br />
                                        <asp:Button ID="btnRight" runat="server" Text=">" CssClass="btn btn-default" OnClick="btnRight_Click"  /> <br />
                                        <asp:Button ID="btnLeft" runat="server" Text="<" CssClass="btn btn-default" OnClick="btnLeft_Click"  /> <br />
                                        <asp:Button ID="btnAllLeft" runat="server" Text="<<" CssClass="btn btn-default" OnClick="btnAllLeft_Click"  /> <br />
                                        <asp:Button ID="btnUp" runat="server" Text="Up" CssClass="btn btn-default" OnClick="btnUp_Click"  /> 
                                        <asp:Button ID="btnDown" runat="server" Text="Down" CssClass="btn btn-default" OnClick="btnDown_Click"  /> 
                                    </div>

                                    <div class="subject-info-box-2">
                                        <label class="fields-label">Fields to be displyed on Invoice</label>
                                        <asp:ListBox ID="lstBox2" runat="server" SelectionMode="Multiple" CssClass="form-control"></asp:ListBox>
                                    </div>
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-success" OnClick="btnSave_Click" style="float:right"  /> 
                                    <div class="clearfix"></div>

<%--                                    <label id="lblMesage" runat="server" style="font-size:medium;font-weight:bold;color:red;"></label>--%>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>


        
    </section>
    <%-- Modal popup --%>
        <div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" id="modal-image">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title">Section Image</h4>
      </div>
      <div class="modal-body text-center">
          <img alt="Alternate Text" id="sectionImage" runat="server"/>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

    <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content" id="modal-image2">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="modal-title2">Create New Vendor</h4>
      </div>
      <div class="modal-body">
          <form action="/">
              <div class="form-group">
                  <label for="vendor">Vendor:</label>
                  <input type="text" class="form-control" id="txtVendorName" value="" placeholder="Please enter vendor name">
                  &nbsp;&nbsp;&nbsp;<label id="lblErrorMessage" class="label label-danger" style="display: none;">
                      Please enter vendor name
                  </label>
              </div>
              <div class="form-group pull-right">

                  <button type="button" id="btnSaveVendor" class="btn btn-success">Save</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              </div>
              <div class="clearfix"></div>
          </form>
      </div>
       
    </div>

  </div>
</div>
   
    <script type="text/javascript">

        function ValidateVendor(VendorName) {

            if(VendorName == '' || VendorName == null)
            {
                $('#txtVendorName').focus();
                $('#lblErrorMessage').show();
                return false;
            }
            $('#lblErrorMessage').hide();
            return true;
        }

        $('#btnSaveVendor').click(function () {

            var VendorName = $('#txtVendorName').val();
             
            if (ValidateVendor(VendorName)) {

                $.ajax({
                    url: 'ASHX/AjaxHdlr.ashx',
                    data: "VendorName=" + VendorName,
                    success: function (msg) {
                        if (msg.data == 1) {
                            $('#myModal2').modal('hide');
                            window.location.reload(true);
                        }
                        else
                            $.alert("There was an error while updating vendor details. Try again!");
                    },
                    error: function (e) {
                        console.log(e);
                    }
                })
            }

        });


    </script>
     
</asp:Content>
