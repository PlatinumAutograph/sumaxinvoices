﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sumax
{
    public class ApplicationSettings
    {
        public const string LogAppName = "Sumax";
        public const string CompanyInvoice = "Sumax Invoice";
        public const string Exception = "Exception";
        public const string CompanyHeader = "SUMAX ENGINEERING (P) LTD.";
        public const string CompanyName = "For Sumax Engineering Pvt Ltd";
        public const string Declaration = "Declaration";
        public const string Declarations = "We declare that this invoice shows the actual price of the goods described and that all particulars are true and correct. (Wether Tax Payable Under reverse Charge Basic - No.)";
        public const string AuthSign = "Authorised Signatory";
    }
    public enum SumaxLogType
    {
        CRIT,
        INFO
    }

    public enum ApplicationLogType
    {
        ERROR, INFO
    }
    public class InvoiceSettings
    {
        public const float TotalWidth = 550f;
        public const float BoderWidth = 0.7f;
        public const float FooterTotalWidth = 200f;
        public const float CellPadding = 7f;
        public const float BodyHdrPadding = 3f;
        public const float cellLeading = 1.3f;
        public const float lastSectionPadding = 6f; //  dispatch through
        public const float footerCellPadding = 3f;
        public const float AmountInWordsPadding = 5f;
        public const float GrossAmountPadding = 5f;
        public static readonly float[] BodyWidths = { 0.5f, 1.4f, 2.26f, 0.8f, 0.52f, 0.75f, 0.7f, 0.55f, 0.85f };
        public static readonly float[] FooterWidths = { 1f, 1, 1 };
        public static readonly float[] LastSectionWidths = { 1.1f, 0.4f, 1 };
    }
    public static class InvoiceTypes
    {
        public static readonly String[] InvoiceType = { "Original", "Duplicate", "Triplicate", "Extra Copy" };
    }
    public static class ProductDetailsSec
    {
        public static readonly string[] Code = { "MasterIdNameCode|Code", "ProductCode" };
        public static readonly string[] Quantity = { "Quantity" };
        public static readonly string[] Rate = { "Rate" };
        public static readonly string[] Gross = { "Gross" };
    }
    public static class InvoiceDefaults
    {
        public static string UOM(string strunit)
        {
            string UOM = string.Empty;
            UOM = strunit;

            int unit = 0;
            if (int.TryParse(strunit, out unit))
                switch (unit)
                {
                    case 3:
                        UOM = "NOS";
                        break;
                    case 4:
                        UOM = "BOX";
                        break;
                    case 5:
                        UOM = "KGS";
                        break;
                    case 6:
                        UOM = "SQM";
                        break;
                    case 7:
                        UOM = "ROL";
                        break;
                    case 9:
                        UOM = "CAN";
                        break;
                    case 10:
                        UOM = "MTR";
                        break;
                    case 11:
                        UOM = "SET";
                        break;
                    case 12:
                        UOM = "PRS";
                        break;
                    case 13:
                        UOM = "PCS";
                        break;
                    case 14:
                        UOM = "LTR";
                        break;
                    case 15:
                        UOM = "GMS";
                        break;
                    case 16:
                        UOM = "BAG";
                        break;
                    case 17:
                        UOM = "MLT";
                        break;
                    case 19:
                        UOM = "SQF";
                        break;
                    case 20:
                        UOM = "PAC";
                        break;
                    case 22:
                        UOM = "SHEET";
                        break;
                    default:
                        UOM = unit.ToString();
                        break;
                }
            return UOM;

        }

        public static string StateName(string statecode)
        {
            string statename = string.Empty;
            int intstatecode;
            if (int.TryParse(statecode, out intstatecode))
                switch (intstatecode)
                {
                    case 1:
                        statename = "Jammu & Kashmir";
                        break;
                    case 2:
                        statename = "Himachal Pradesh";
                        break;
                    case 3:
                        statename = "Punjab";
                        break;
                    case 4:
                        statename = "Chandigarh";
                        break;
                    case 5:
                        statename = "Uttarakhand";
                        break;
                    case 6:
                        statename = "Haryana";
                        break;
                    case 7:
                        statename = "Delhi";
                        break;
                    case 8:
                        statename = "Rajasthan";
                        break;
                    case 9:
                        statename = "Uttar Pradesh";
                        break;
                    case 10:
                        statename = "Bihar";
                        break;
                    case 11:
                        statename = "Sikkim";
                        break;
                    case 12:
                        statename = "Arunachal Pradesh";
                        break;
                    case 13:
                        statename = "Nagaland";
                        break;
                    case 14:
                        statename = "Manipur";
                        break;
                    case 15:
                        statename = "Mizoram";
                        break;
                    case 16:
                        statename = "Tripura";
                        break;
                    case 17:
                        statename = "Meghalaya";
                        break;
                    case 18:
                        statename = "Assam";
                        break;
                    case 19:
                        statename = "West Bengal";
                        break;
                    case 20:
                        statename = "Jharkhand";
                        break;
                    case 21:
                        statename = "Odisha";
                        break;
                    case 22:
                        statename = "Chhattisgarh";
                        break;
                    case 23:
                        statename = "Madhya Pradesh";
                        break;
                    case 24:
                        statename = "Gujarat";
                        break;
                    case 25:
                        statename = "Daman & Diu";
                        break;
                    case 26:
                        statename = "Dadra & Nagar Haveli";
                        break;
                    case 27:
                        statename = "Maharashtra";
                        break;
                    case 28:
                        statename = "Karnataka";
                        break;
                    case 29:
                        statename = "Goa";
                        break;
                    case 30:
                        statename = "Lakshadweep";
                        break;
                    case 31:
                        statename = "Kerala";
                        break;
                    case 32:
                        statename = "Tamilnadu";
                        break;
                    case 33:
                        statename = "Puducherry";
                        break;
                    case 34:
                        statename = "Andaman and Nicobar islands";
                        break;
                    case 35:
                        statename = "Telangana";
                        break;
                    case 36:
                        statename = "Andhra Pradesh";
                        break;
                    case 37:
                        statename = "Other Territory";
                        break;
                    case 39:
                        statename = "Other Territory";
                        break;
                    default:
                        statename = intstatecode.ToString();
                        break;
                }
            else
                statename = statecode;

            return statename;
        }
        public static string DispatchThrough(string dispatchThrough)
        {
            string DispatchThrough = string.Empty;
            switch (dispatchThrough)
            {
                case "1":
                    DispatchThrough = "Road";
                    break;
                default:
                    DispatchThrough = dispatchThrough;
                    break;
            }
            return DispatchThrough;
        }
    }
}