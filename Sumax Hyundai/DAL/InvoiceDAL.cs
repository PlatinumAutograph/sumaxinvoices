﻿using sumax.DAL;
using Sumax_Hyundai.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Sumax_Hyundai.DAL
{
    public class InvoiceDAL
    {
        #region Declarations

        sumaxEntities entities = new sumaxEntities();

        private static string connString = Convert.ToString(ConfigurationManager.ConnectionStrings["ConnectionSumax"]);
        //private static string connString = Convert.ToString();

        #endregion

        #region Sections

        public List<tbl_sections> GetSectionsList()
        {
            return entities.tbl_sections.OrderBy(a => a.TemplateOrder).ToList();
        }

        #endregion

        #region Vendors


        public List<tbl_Vendors> GetVendorsList()
        {
            return entities.tbl_Vendors.ToList();
        }

        public tbl_Vendors GetVendor(int VendorId)
        {
            return entities.tbl_Vendors.Where(a => a.Id == VendorId).FirstOrDefault();
        }

        public int InsertVendor(string VendorName)
        {
            int res = 0;
            try
            {
                tbl_Vendors vendorInfo = new tbl_Vendors();
                vendorInfo.VendorName = VendorName;
                vendorInfo.CreatedOn = DateTime.Now;
                entities.tbl_Vendors.Add(vendorInfo);
                res = entities.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;
        }

        public int GetSectionId(string section)
        {
            return entities.tbl_sections.Where(a => a.Section == section).FirstOrDefault().Id;
        }


        #endregion

        #region Templates

        public List<tbl_templates> GetAllTemplates(string TemplatesIds)
        {
            List<tbl_templates> _lstTemplates = new List<tbl_templates>();
            _lstTemplates = entities.tbl_templates.ToList();
            if (!string.IsNullOrEmpty(TemplatesIds))
            {
                try
                {
                    long[] arrTemplates = TemplatesIds.Split('|').Select(r => Convert.ToInt64(r)).ToArray();
                    if (arrTemplates != null)
                        _lstTemplates = _lstTemplates.Where(r => arrTemplates.Contains(r.Id)).ToList();
                }
                catch (Exception ex)
                {
                    Helper.WriteLog(sumax.ApplicationLogType.ERROR.ToString(), "GetAllTemplates", ex.Message + " | " + ex.StackTrace);
                    _lstTemplates = new List<tbl_templates>();
                }
            }

            return _lstTemplates;
        }
        public int GetVendorIdByTemplate(int TemplateId)
        {
            int id = 0;

            try
            {
                int? id1 = entities.tbl_templates.Where(r => r.Id == TemplateId).Select(r => r.VendorId).FirstOrDefault();
                id = id1 ?? 0;
            }
            catch (Exception ex)
            {
                id = 0;
                Helper.WriteLog("Error", "GetVendorIdByTemplate", ex.Message + ex.StackTrace);
            }

            return id;
        }

        public int CheckIfTemplateDetailsExistsOrNot(int VendorId)
        {
            var res = entities.tbl_templates.Where(a => a.VendorId == VendorId).FirstOrDefault();
            if (res != null && res.Id != 0)
                return res.Id;
            return 0;
        }
        public List<TemplateDetailResult> GetTemplateDetailList(int VendorId)
        {
            List<TemplateDetailResult> res = new List<TemplateDetailResult>();

            SqlConnection con = new SqlConnection(connString);
            try
            {
                SqlCommand cmd = new SqlCommand("SP_GetTemplateDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("VendorId", VendorId);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    res.Add(new TemplateDetailResult()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        SectionId = Convert.ToInt32(dr["SectionId"]),
                        TemplateOrder = Convert.ToInt32(dr["TemplateOrder"]),
                        DisplayField = Convert.ToString(dr["DisplayField"]),
                        XmlField = Convert.ToString(dr["XmlField"]),
                        Section = Convert.ToString(dr["Section"]),
                        CreatedOn = Convert.ToString(dr["CreatedOn"]),
                        ModifedOn = Convert.ToString(dr["ModifedOn"]),
                        SortOrder = Convert.ToInt32(dr["SortOrder"])
                    });
                }
                return res;
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Error", "GetVendorIdByTemplate", ex.Message + ex.StackTrace);
                return res;
            }
            finally
            {
                con.Close();
            }


        }

        public TemplateDetailResult GetTemplateDetails(int TemplatedDetailId)
        {
            var tempDetails = entities.tbl_template_details.Where(a => a.Id == TemplatedDetailId).SingleOrDefault();
            TemplateDetailResult res = new TemplateDetailResult();
            if (tempDetails != null)
            {
                string Section = entities.tbl_sections.Where(a => a.Id == tempDetails.SectionId).FirstOrDefault().Section;
                res = new TemplateDetailResult() { Section = Section, DisplayField = tempDetails.DisplayField, Id = Convert.ToInt32(tempDetails.Id), SectionId = Convert.ToInt32(tempDetails.SectionId), XmlField = tempDetails.XmlField };
            }
            return res;
        }

        public int InsertTemplate(tbl_templates templates, List<tbl_template_details> list)
        {
            entities.tbl_templates.Add(templates);

            int res = entities.SaveChanges();

            if (res > 0)
            {
                list.ToList().ForEach(u =>
                {
                    u.TempId = templates.Id;
                });

                foreach (var item in list)
                {
                    InsertTemplateDetails(item);
                }
            }
            return res;
        }

        public int InsertTemplateDetails(tbl_template_details details)
        {
            entities.tbl_template_details.Add(details);
            return entities.SaveChanges();
        }

        public int UpdateTemplate(tbl_templates templates, List<tbl_template_details> list)
        {
            int res = 0;
            using (var db = new sumaxEntities())
            {
                tbl_templates template = db.tbl_templates.Where(a => a.Id == templates.Id).SingleOrDefault();
                template.ModifiedOn = templates.ModifiedOn;
                template.ModifiedBy = templates.ModifiedBy;
                res = db.SaveChanges();
            }

            if (res > 0)
            {
                list.ToList().ForEach(u =>
                {
                    u.TempId = templates.Id;
                });
                List<tbl_template_details> _lsttemplate = null;
                if (list != null && list.Count > 0)
                {
                    int? sectionId = list.FirstOrDefault().SectionId;
                    if (sectionId != null)
                    {
                        _lsttemplate = entities.tbl_template_details.Where(r => r.TempId == templates.Id && r.SectionId == sectionId).ToList();

                        //entities.tbl_template_details.RemoveRange(entities.tbl_template_details.Where(r => r.TempId == templates.Id && r.SectionId == list.FirstOrDefault().SectionId));
                        entities.Database.ExecuteSqlCommand("DELETE FROM [tbl_template_details] WHERE TempId = {0} AND SectionId = {1}", templates.Id, sectionId);
                    }
                }

                foreach (var item in list)
                {
                    tbl_template_details _temDet = entities.tbl_template_details.Where(a => a.XmlField == item.XmlField && a.TempId == item.TempId && a.SectionId == item.SectionId).FirstOrDefault();
                    if (_temDet != null && _temDet.Id != 0)
                        UpdateTemplateDetails(_temDet);
                    else
                    {
                        if (_lsttemplate != null && _lsttemplate.Count > 0)
                        {
                            string DispField = _lsttemplate.Where(r => r.XmlField.Equals(item.XmlField) && r.SectionId == item.SectionId && r.TempId == item.TempId).Select(r => r.DisplayField).FirstOrDefault();
                            if (!string.IsNullOrEmpty(DispField))
                                item.DisplayField = DispField;
                        }
                        InsertTemplateDetails(item);
                    }
                }
            }
            return res;
        }

        public int UpdateTemplateDetails(int TemplateDetailId, string DisplayField)
        {
            int res = 0;
            using (var db = new sumaxEntities())
            {
                tbl_template_details tempDetails = db.tbl_template_details.Where(a => a.Id == TemplateDetailId).FirstOrDefault();
                tempDetails.DisplayField = DisplayField;
                tempDetails.ModifedOn = DateTime.Now;
                res = db.SaveChanges();
            }
            return res;
        }

        public int UpdateTemplateDetails(tbl_template_details _tempDet)
        {
            int res = 0;
            using (var db = new sumaxEntities())
            {
                tbl_template_details tempDetails = db.tbl_template_details.Where(a => a.Id == _tempDet.Id).FirstOrDefault();
                tempDetails.DisplayField = _tempDet.DisplayField;
                tempDetails.XmlField = _tempDet.XmlField;
                tempDetails.ModifedBy = _tempDet.ModifedBy;
                tempDetails.SectionId = _tempDet.SectionId;
                tempDetails.TempId = _tempDet.TempId;
                tempDetails.ModifedOn = DateTime.Now;
                res = db.SaveChanges();
            }
            return res;
        }

        public int GetMaxSortOrder(int TempId)
        {
            return entities.tbl_template_details.Where(a => a.TempId == TempId).Select(a => a.SortOrder).Max() ?? 0;
        }

        #endregion 
    }
}