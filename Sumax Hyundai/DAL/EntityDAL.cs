﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using sumax;
using sumax.DAL;
using sumax.Models;

namespace Sumax_Hyundai.DAL
{
    public class EntityDAL
    {
        #region Declarations

        sumaxEntities entities = new sumaxEntities();

        #endregion


        public bool ValidateLogin(string email, string password, out tbl_Users _user)
        {
            bool flag = false;
            _user = new tbl_Users();
            try
            {
                string passwordHash = Helper.GetPasswordHash(password);
                List<tbl_Users> _Users = entities.tbl_Users.Where(r => r.Email.Equals(email) && r.Password.Equals(passwordHash)).ToList();
                if (_Users != null && _Users.Count > 0)
                {
                    flag = true;
                    _user = _Users.FirstOrDefault();
                    try
                    {
                        _user.LastLogin = DateTime.UtcNow;
                        entities.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "ValidateLogin", ex.Message + "  |  " + ex.StackTrace);
                    }

                }
                else
                    flag = false;
            }
            catch (Exception ex)
            {
                flag = false;
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "ValidateLogin", ex.Message + "  |  " + ex.StackTrace);
            }

            return flag;
        }

        public bool AddUser(string Firstname, string LastName, string Email, string TemplateId,string UserPassword)
        {
            bool flag = false;
            TemplateId = TemplateId.Replace(',', '|');

            tbl_Users _User = new tbl_Users();
            _User.FirstName = Firstname;
            _User.LastName = LastName;
            _User.Email = Email.Trim();
            _User.TemplateId = TemplateId;
            _User.UserType = "U";
            //string password = Membership.GeneratePassword(8, 1);

            string password = UserPassword;
            if (string.IsNullOrEmpty(password))
                password = "sumax@123";

            _User.Password = Helper.GetPasswordHash(password);
            _User.Status = "Y";

            try
            {
                entities.tbl_Users.Add(_User);
                flag = entities.SaveChanges() > 0;
                if(flag)
                    SendEmail(Email, password);
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "AddUser", ex.Message + " | " + ex.StackTrace);

            }

            return flag;
        }
        public bool DeleteUser(long UserId)
        {
            bool flag = false;

            try
            {
                tbl_Users _Users = entities.tbl_Users.Where(r => r.UserId == UserId).FirstOrDefault();
                if (_Users != null)
                {
                    entities.tbl_Users.Attach(_Users);
                    entities.tbl_Users.Remove(_Users);
                    flag = entities.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "DeleteUser", ex.Message + " | " + ex.StackTrace);
            }

            return flag;
        }

        private bool SendEmail(string Email, string Password)
        {
            bool flag = false;

            EmailModelDTO email = new EmailModelDTO();
            string url = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

            email.EmailTo = Email;
            email.Body = "<!doctype html><html> <head> <meta name='viewport' content='width=device-width'> " +
                "<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <title>Sumax new User</title> " +
                "<style> @media only screen and (max-width: 620px){table[class=body] h1{font-size: 28px !important; margin-bottom: 10px !important;}table[class=body] " +
                "p, table[class=body] ul, table[class=body] ol, table[class=body] td, table[class=body] span, table[class=body] a{font-size: 16px !important;}" +
                "table[class=body] .wrapper, table[class=body] .article{padding: 10px !important;}table[class=body] .content{padding: 0 !important;}table[class=body] " +
                ".container{padding: 0 !important; width: 100% !important;}table[class=body] .main{border-left-width: 0 !important; border-radius: 0 !important; " +
                "border-right-width: 0 !important;}table[class=body] .btn table{width: 100% !important;}table[class=body] .btn a{width: 100% !important;}" +
                "table[class=body] .img-responsive{height: auto !important; max-width: 100% !important; width: auto !important;}}@media all{.ExternalClass{width: 100%;}" +
                ".ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height: 100%;}.apple-link " +
                "a{color: inherit !important; font-family: inherit !important; font-size: inherit !important; font-weight: inherit !important; line-height: inherit " +
                "!important; text-decoration: none !important;}.btn-primary table td:hover{background-color: #34495e !important;}.btn-primary a:hover{background-color:" +
                " #34495e !important; border-color: #34495e !important;}}</style> </head> <body class='' style='background-color: #f6f6f6; font-family: sans-serif; " +
                "-webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;'>" +
                " <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; " +
                "background-color: #f6f6f6;'> <tr> <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td><td class='container' style='font-family: " +
                "sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;'> <div class='content' style='box-sizing:" +
                " border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;'> <span class='preheader' style='color: transparent; display: none; height: 0; max-height:" +
                " 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;'>Sumax Engineering Credentials</span> <table class='main' " +
                "style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;'> " +
                "<tr> <td class='wrapper' style='font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;'> " +
                "<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'> <tr>" +
                " <td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'> <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0;" +
                " Margin-bottom: 15px;'>Hi User,</p><p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;'>" +
                "Admin had created a new login. Use below Credentials to login Sumax Invoice System</p> <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;'>" +

                "Username/Email:" + Email + "<br/>" +
                "Password: " + Password +


                " </p><table border='0' cellpadding='0' cellspacing='0' class='btn btn-primary'" +
                " style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;'> " +
                "<tbody> <tr> <td align='left' style='font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;'> " +
                "<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;'>" +
                " <tbody> <tr> <td style='font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;'> " +
                "<a href= '" + url + "' target='_blank' style='display: inline-block; color: #ffffff; background-color:" +
                " #3498db; border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold;" +
                " margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #3498db;'>Go to Sumax</a> </td></tr></tbody> </table> </td></tr></tbody> </table>" +
                " <p style='font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;'>Thanks<br/>Sumax Engineering</p></td></tr></table> " +
                "</td></tr></table> <div class='footer' style='clear: both; Margin-top: 10px; text-align: center; width: 100%;'> <table border='0' cellpadding='0' cellspacing='0' " +
                "style='border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;'> <tr> <td class='content-block' style='font-family:" +
                " sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;'> " +
                "</td></tr><tr> <td class='content-block powered-by' style='font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px;" +
                " font-size: 12px; color: #999999; text-align: center;'> Copyrights&copy; <a href='" + url + "' style='color: #999999; font-size: 12px; text-align: " +
                "center; text-decoration: none;'>Sumax - 2018</a>. </td></tr></table> </div></div></td><td style='font-family: sans-serif; font-size: 14px; vertical-align: top;'>&nbsp;</td></tr></table>" +
                " </body></html>";
            email.Subject = "Sumax Engineering Credentials";


            flag = Helper.SendEmail(email) > 0;

            return flag;
        }
        public bool CheckUser(string Email)
        {
            bool flag = false;

            try
            {
                var users = entities.tbl_Users.Where(r => r.Email.Equals(Email.Trim())).ToList();
                if (users != null && users.Count > 0)
                    flag = false;
                else
                    flag = true;
            }
            catch (Exception ex)
            {

            }
            return flag;
        }

        public List<tbl_Users> BindUserList()
        {
            List<tbl_Users> _lstUser = null;
            try
            {
                _lstUser = entities.tbl_Users.Where(r => r.UserId != 1).ToList();
            }
            catch (Exception ex)
            {
                _lstUser = null;
                Helper.WriteLog("Error", "BindUserList", ex.Message + ex.StackTrace);
            }
            return _lstUser;
        }

        public bool InsertFileData(List<tbl_FilesData> _lstFilesData)
        {
            bool flag = false;

            try
            {
                foreach (tbl_FilesData fileData in _lstFilesData)
                    entities.tbl_FilesData.Add(fileData);
                flag = entities.SaveChanges() > 0;
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "InsertFileData", ex.StackTrace);
            }

            return flag;
        }

        public List<tbl_FilesData> GetListFiles(int TemplateId, int UserId,bool isAdmin)
        {
            List<tbl_FilesData> _lstFiles = new List<tbl_FilesData>();

            try
            {
                if (UserId > 0)
                {
                    if (isAdmin)
                        _lstFiles = entities.tbl_FilesData
                                    .Where(r => r.TemplateId == TemplateId)
                                    .OrderByDescending(r => r.FileId)
                                    .Take(500)
                                    .ToList();
                    else
                        _lstFiles = entities.tbl_FilesData
                                    .Where(r => r.TemplateId == TemplateId && r.UserId != null && r.UserId == UserId)
                                    .OrderByDescending(r => r.FileId)
                                    .Take(500)
                                    .ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return _lstFiles;
        }
        public tbl_FilesData GetSingleFileData(long FileId)
        {
            tbl_FilesData tbl_Files = new tbl_FilesData();
            try
            {
                tbl_Files = entities.tbl_FilesData.Where(r => r.FileId == FileId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tbl_Files;
        }
        public bool UpdateStatusFile(long FileId, string Status)
        {
            bool flag = false;
            try
            {
                tbl_FilesData _FilesData = entities.tbl_FilesData.Where(r => r.FileId == FileId).FirstOrDefault();
                if (_FilesData != null)
                {
                    _FilesData.Status = Status;
                    _FilesData.ProcessDate = DateTime.UtcNow;
                    flag = entities.SaveChanges() > 0;
                }
            }
            catch (Exception ex)
            {
                flag = false;
                throw ex;
            }
            return flag;
        }


    }
}