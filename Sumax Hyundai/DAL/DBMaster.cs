﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;

namespace sumax.DAL
{
    public abstract class DBMaster
    {
        #region Declarations

        public SqlConnection sqlConn;
        public string sQuery;
        public SqlTransaction Transaction;

        #endregion

        public abstract int Initialize(SqlConnection sqlConnection);
        public DBMaster()
        {
            sqlConn = new SqlConnection();
        }

        public string sGetConnectionString()
        {
            string sSqlConnection = "";
            //try
            //{
            //    ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["ConnectionSumax"];
            //    sSqlConnection = settings.ConnectionString;
            //}

            //catch (Exception ex)
            //{

            //}

            return sSqlConnection;

        }
        public bool OpenConnection()
        {
            bool bReturn = false;
            try
            {
                if (sqlConn.State == ConnectionState.Closed || sqlConn.State == ConnectionState.Broken)
                {
                    sqlConn.ConnectionString = sGetConnectionString();
                    sqlConn.Open();

                }
                bReturn = true;
            }

            catch (Exception ex)
            {

                bReturn = false;
                throw ex;
            }
            return (bReturn);
        }

        public bool CloseConnection()
        {
            sqlConn.Close();
            return (true);
        }

        public void BeginTransaction()
        {
            Transaction = sqlConn.BeginTransaction();
        }

        public void RollBack()
        {
            Transaction.Rollback();
        }

        public void Commit()
        {
            Transaction.Commit();
        }

    }
}