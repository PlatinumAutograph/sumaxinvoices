﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using QRCoder;
using sumax.DAL;
using sumax.Models;
using Sumax_Hyundai;
using Sumax_Hyundai.DAL;
using Sumax_Hyundai.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Image = iTextSharp.text.Image;

namespace sumax.DAL
{
    public class Utility
    {
        #region Declarations
        EntityDAL entityDAL = new EntityDAL();
        #endregion

        #region ReadFIle

        public string ReadDataFromBytes(byte[] data)
        {
            string text = System.Text.Encoding.UTF8.GetString(data);
            string InvoiceNumber = string.Empty;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(text);
                XmlNodeList nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/Header");
                foreach (XmlNode node in nodelist) // for each <testcase> node
                {
                    InvoiceNumber = node.SelectSingleNode("DocNo").InnerText;
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), "ReadDataFromBytes", ex.StackTrace.ToString());
            }
            return InvoiceNumber;
        }
        public bool GenerateInvoice(long FileId)
        {
            bool flag = false;

            tbl_FilesData tbl_Files = entityDAL.GetSingleFileData(FileId);
            if (tbl_Files != null)
            {
                InvoiceModel invoiceModel = new InvoiceModel();
                string Filename = string.Format("{0}_{1}", tbl_Files.FileName, tbl_Files.InvoiceNo);
                flag = ReadFile(tbl_Files.Data, Filename, FileId, out invoiceModel);
            }


            return flag;
        }

        public bool ReadFile(byte[] Data, string Filename, long FileId, out InvoiceModel invoiceModel)
        {
            bool flag = false;
            entityDAL.UpdateStatusFile(FileId, "In - Progress");

            string applicationPath = HttpContext.Current.Server.MapPath("~/");
            Filename = Path.Combine(applicationPath, "pdf", Filename);


            invoiceModel = new InvoiceModel();
            invoiceModel.FileName = Filename;
            invoiceModel.ReceivedDate = DateTime.UtcNow;
            invoiceModel.Status = "Received";
            invoiceModel.division = new Division();
            invoiceModel.invoiceHeader = new InvoiceHeader();
            invoiceModel.address = new Address();
            invoiceModel.bodyData = new List<BodyData>();
            invoiceModel.mCoreProduct = new MCoreProduct();
            invoiceModel.footer = new Footer();
            invoiceModel.lastSection = new LastSection();

            try
            {
                XmlDocument doc = new XmlDocument();
                string text = System.Text.Encoding.UTF8.GetString(Data);
                doc.LoadXml(text);

                XmlNodeList nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/Header"); // Header node

                foreach (XmlNode node in nodelist) // for each <testcase> node
                {
                    invoiceModel.invoiceHeader.VoucherType = node.SelectSingleNode("VoucherType").InnerText;
                    invoiceModel.invoiceHeader.InvoiceNo = node.SelectSingleNode("DocNo").InnerText;  // Invoice number
                    invoiceModel.invoiceHeader.CreateDate = node.SelectSingleNode("Date").InnerText;
                    invoiceModel.invoiceHeader.CreateTime = node.SelectSingleNode("Time").InnerText;
                    invoiceModel.invoiceHeader.ModifiedDate = node.SelectSingleNode("ModifiedDate").InnerText;
                    invoiceModel.invoiceHeader.ModifiedTime = node.SelectSingleNode("ModifiedTime").InnerText;
                }


                // Header extra
                nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/HeaderExtra/IdNamePair");
                //mahesh verify
                foreach (XmlNode node in nodelist)
                {
                    var nodeName = node.SelectSingleNode("Name").InnerText;
                    string tag = node.SelectSingleNode("Tag").InnerText;
                    if (!string.IsNullOrEmpty(nodeName))
                        switch (nodeName.Trim().ToLower())
                        {
                            case "customer_po_no":
                            case "67110589":
                                invoiceModel.invoiceHeader.PONo = tag;
                                break;

                            case "customer_po_date":
                            case "67110590":
                                invoiceModel.invoiceHeader.PODate = tag;
                                break;

                            case "buyer_address":
                            case "67111409": // Buyer Address
                                invoiceModel.address.buyer_address = tag;
                                break;

                            case "buyer_gstin_no":
                            case "67111410":
                                invoiceModel.address.GSTIN = tag;
                                break;

                            case "buyer_state_code":
                            case "67111411":
                                invoiceModel.address.Buyer_State_Code = tag;
                                break;

                            case "buyer_statename":
                            case "67111444":
                                invoiceModel.address.Buyer_StateName = InvoiceDefaults.StateName(tag.Trim());
                                break;

                            case "contact_person":
                            case "67110595":
                                invoiceModel.address.ContactPerson = tag;
                                break;

                            case "contact_mobile_no":
                            case "67111391":
                                invoiceModel.address.MobileNo = tag;
                                break;

                            case "placeofsupply":
                            case "67110577":
                                invoiceModel.address.RPlaceofSupply = InvoiceDefaults.StateName(tag.Trim());
                                break;

                            case "delivery_location_address1":
                            case "67111414":
                                invoiceModel.address.Delivery_Location_Address1 = tag;
                                break;

                            case "delivery_location_address2":
                            case "67111415":
                                invoiceModel.address.Delivery_Location_Address2 = tag;
                                break;

                            case "delivery_location_gstin_no":
                            case "67111419":
                                invoiceModel.address.delivery_location_gstin_no = tag;
                                break;

                            case "delivery_location_state_code":
                            case "67111418":
                                invoiceModel.address.delivery_location_state_code = tag;
                                break;

                            case "delivery_location_state_name":
                            case "67111424":
                                invoiceModel.address.Delivery_Location_State_Name = InvoiceDefaults.StateName(tag);
                                break;

                            case "dispatch_through":
                            case "67110597":
                                invoiceModel.lastSection.DispatchTrough = InvoiceDefaults.DispatchThrough(tag);
                                break;

                            case "transporter":
                            case "67110587":
                                invoiceModel.lastSection.TranspoterName = tag;
                                break;

                            case "vehicleno":
                            case "67111040":
                                invoiceModel.lastSection.VehicleNo = tag;
                                break;

                            case "company_account_no":
                            case "67111402":
                                invoiceModel.lastSection.AccountNo = tag;
                                break;

                            case "comapny_bank_details":
                            case "67111392":
                                invoiceModel.lastSection.NameOfBank = tag;
                                break;

                            //case "bank_ifsc_code":
                            //case "67111298":
                            //    invoiceModel.lastSection.IFSCCode = tag;
                            //    break;
                            case "company_ifsc_code":
                            case "67111403":
                                invoiceModel.lastSection.IFSCCode = tag;
                                break;

                            case "branch":
                            case "67111407":
                                invoiceModel.lastSection.Branch = tag;
                                break;

                            case "credit_days":
                            case "67110579":
                                invoiceModel.footer.CreditDays = tag;
                                break;

                            case "deliver_note":
                            case "67111413":
                                invoiceModel.footer.Deliver_Note = tag;
                                break;

                            case "po_item_serial_no":
                            case "67111432":
                                invoiceModel.invoiceHeader.Po_Item_Serial_No = tag;
                                break;

                            case "gate_location":
                            case "67111435":
                                invoiceModel.invoiceHeader.Gate_Location = tag;
                                break;

                            case "unit_uom_":
                            case "67111436":
                                invoiceModel.invoiceHeader.Unit_UOM_ = tag;
                                break;

                            case "cuurency_name":
                            case "67111437":
                                invoiceModel.invoiceHeader.Cuurency_Name = tag;
                                break;

                            case "division_address":
                            case "67111426":
                                invoiceModel.division.Division_Address = tag;
                                break;

                            case "division_address_1":
                            case "67111427":
                                invoiceModel.division.Division_Address_1 = tag;
                                break;

                            case "division_state_name":
                            case "67111428":
                                invoiceModel.division.Division_State_Name = InvoiceDefaults.StateName(tag);
                                break;

                            case "division_state_code":
                            case "67111429":
                                invoiceModel.division.Division_State_Code = tag;
                                break;

                            case "divivsion_email":
                            case "67111430":
                                invoiceModel.division.Divivsion_Email = tag;
                                break;
                            case "division_gstin_no":
                            case "67111441":
                                invoiceModel.division.Division_Gstin_No = tag;
                                break;

                            case "lr_number":
                            case "67110584":
                                invoiceModel.invoiceHeader.LR_Number = tag;
                                break;

                            default:
                                break;
                        }
                }
                // Header Extra End

                //Body Data
                nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/BodyData/TransBody");

                foreach (XmlNode node in nodelist)
                {
                    BodyData bodyData = new BodyData();
                    bodyData.Quantity = float.Parse(node.SelectSingleNode("Sales/Quantity").InnerText, CultureInfo.InvariantCulture.NumberFormat);
                    bodyData.Rate = node.SelectSingleNode("Sales/Rate").InnerText;
                    bodyData.Amount = float.Parse(node.SelectSingleNode("Sales/Gross").InnerText, CultureInfo.InvariantCulture.NumberFormat);
                    bodyData.Unit = node.SelectSingleNode("Sales/Unit").InnerText;

                    XmlNodeList nodelist1 = node.SelectNodes("Sales/ScreenData/ScreenData");
                    foreach (XmlNode node1 in nodelist1)
                    {
                        string FieldName = node1.SelectSingleNode("FieldName").InnerText;
                        string Value = node1.SelectSingleNode("Value").InnerText;
                        string Input = node1.SelectSingleNode("Input").InnerText;
                        switch (FieldName.Trim().ToLower())
                        {
                            case "gst %":
                                bodyData.GSTPerct = node1.SelectSingleNode("Value").InnerText;
                                break;
                            case "cgst":
                                bodyData.CGST = Value;
                                bodyData.CGSTInput = Input;
                                break;
                            case "sgst":
                                bodyData.SGST = Value;
                                bodyData.SGSTInput = Input;
                                break;
                            case "igst":
                                bodyData.IGST = Value;
                                bodyData.IGSTInput = Input;
                                break;
                            case "cess":
                                bodyData.Cess = Value;
                                bodyData.CessInput = Input;
                                break;
                            case "taxable value":
                                bodyData.TaxableValue = Value;
                                bodyData.TaxableValueInput = Input;
                                break;
                            case "tax amount":
                                bodyData.TaxAmount = Value;
                                bodyData.TaxAmountInput = Input;
                                break;
                            case "net rate":
                                bodyData.NetRate = Value;
                                bodyData.NetRateInput = Input;
                                break;
                            case "charges":
                                bodyData.Frieght = Value;
                                break;
                            default:
                                break;
                        }
                    }

                    nodelist1 = node.SelectNodes("BodyExtra/IdNamePair");
                    foreach (XmlNode node1 in nodelist1)
                    {
                        string name = node1.SelectSingleNode("Name").InnerText;
                        string tag = node1.SelectSingleNode("Tag").InnerText;
                        switch (name.Trim().ToLower())
                        {
                            case "description":
                                bodyData.Description = tag;
                                break;
                            case "hsn_code":
                                bodyData.HSNCode = node1.SelectSingleNode("Tag").InnerText;
                                break;
                            case "productcode":
                                invoiceModel.mCoreProduct.Code = tag;
                                invoiceModel.invoiceHeader.ProductCode = tag;
                                bodyData.ProductCode = tag;
                                break;
                            case "partyproductname":
                                invoiceModel.mCoreProduct.Name = tag;
                                bodyData.PartyProductName = tag;
                                break;
                            default:
                                break;

                        }
                    }
                    invoiceModel.bodyData.Add(bodyData);
                }

                // Body Data end

                // origianl amounts
                nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/BodyData/TransBody/OriginalAmounts");
                foreach (XmlNode node in nodelist)
                {
                    XmlNodeList nodelist1 = node.SelectNodes("decimal");
                    float oneNodeTotalamount = 0;
                    foreach (XmlNode node1 in nodelist1)
                    {
                        oneNodeTotalamount = 0;
                        float temp = float.Parse(node1.InnerText.Replace("-", ""));
                        if (temp == 0) continue;
                        oneNodeTotalamount = temp >= oneNodeTotalamount ? temp : oneNodeTotalamount;
                    }
                    invoiceModel.footer.TotalAmount += oneNodeTotalamount;
                }

                //original amounts end

                // footer
                nodelist = doc.SelectNodes("ArrayOfTransaction/Transaction/Footer/FooterData");
                foreach (XmlNode node in nodelist)
                {
                    string strFieldname = node.SelectSingleNode("FieldName").InnerText;
                    if (!string.IsNullOrEmpty(strFieldname))
                        switch (strFieldname.Trim().ToLower())
                        {
                            case "roundoff":
                            case "1346":
                                invoiceModel.footer.RoundOff = float.Parse(node.SelectSingleNode("Value").InnerText, CultureInfo.InvariantCulture.NumberFormat);
                                break;
                            case "tcs":
                                invoiceModel.footer.TCS = float.Parse(node.SelectSingleNode("Value").InnerText, CultureInfo.InvariantCulture.NumberFormat);
                                break;

                            default:
                                break;
                        }
                }

                // footer end

                // Masters
                nodelist = doc.SelectNodes("ArrayOfTransaction/Masters");
                foreach (XmlNode node in nodelist)
                {
                    XmlDocument masters = new XmlDocument();
                    masters.LoadXml("<MastersExport>\r\n <MastersData>\r\n" + System.Net.WebUtility.HtmlDecode(HttpUtility.HtmlDecode(node.InnerXml)));
                    var nodelist1 = masters.SelectNodes("MastersExport/MastersData");

                    foreach (XmlNode node1 in nodelist1)
                    {
                        string strMasterName = node1.SelectSingleNode("strMasterName").InnerText;
                        switch (strMasterName.Trim().ToLower())
                        {
                            case "mcore_department":
                                invoiceModel.address.Name = node1.SelectSingleNode("arrMasterIdNameCode/MasterIdNameCode/Name").InnerText;
                                break;
                            case "mcore_product":
                                //invoiceModel.mCoreProduct.Code = node1.SelectSingleNode("arrMasterIdNameCode/MasterIdNameCode/Code").InnerText;
                                //invoiceModel.mCoreProduct.Name = node1.SelectSingleNode("arrMasterIdNameCode/MasterIdNameCode/Name").InnerText;
                                break;
                            default:
                                break;
                        }
                    }
                }
                //Masters End

            }
            catch (Exception ex)
            {
                flag = false;
                invoiceModel = null;
                //throw ex;
            }
            try
            {
                if (invoiceModel != null)
                    flag = GenerateInvoice(Filename, FileId, Path.Combine(applicationPath, "assets\\images\\"), ref invoiceModel);
                if (flag)
                {
                    //update status completed
                    entityDAL.UpdateStatusFile(FileId, "Completed");
                }
            }
            catch (Exception ex)
            {
                flag = false;
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), MethodBase.GetCurrentMethod().Name, ex.StackTrace);
                entityDAL.UpdateStatusFile(FileId, "Error");
                //throw ex;
            }

            return flag;
        }

        #endregion

        #region GenerateInvoice

        public bool GenerateInvoice(string Filename, long FileId, string images, ref InvoiceModel invoiceModel)
        {
            bool flag = false;
            try
            {
                string Filepath = Filename;

                float GrossAmount = 0, Qty = 0, Rate = 0;
                string HSN_Code = "";
                // string Unit_UOM = "";

                string QRCodeText = string.Empty;


                Dictionary<string, int> dict = new Dictionary<string, int>();
                Stream fileStream;

                List<BodyData> tempBodyData = new List<BodyData>();
                foreach (BodyData item in invoiceModel.bodyData)
                {
                    GrossAmount += item.Amount;
                    invoiceModel.footer.CSTValue += float.Parse(item.CGST, CultureInfo.InvariantCulture.NumberFormat);
                    invoiceModel.footer.SGSTValue += float.Parse(item.SGST, CultureInfo.InvariantCulture.NumberFormat);
                    invoiceModel.footer.IGSTVAlue += float.Parse(item.IGST, CultureInfo.InvariantCulture.NumberFormat);
                    invoiceModel.footer.Freight += float.Parse(item.Frieght, CultureInfo.InvariantCulture.NumberFormat);
                    Qty += item.Quantity;
                    HSN_Code = item.HSNCode;
                    Rate = float.Parse(item.Rate);
                    BodyData tbody = tempBodyData.Where(r => r.ProductCode == item.ProductCode).FirstOrDefault();
                    if (tbody == null)
                        tempBodyData.Add(item);
                    else
                    {
                        tbody.Quantity = item.Quantity + tbody.Quantity;
                        tbody.Amount = item.Amount + tbody.Amount;
                        tempBodyData.RemoveAll(r => r.ProductCode == item.ProductCode);
                        tempBodyData.Add(tbody);
                    }
                }

                float TotalAmount = invoiceModel.footer.TotalAmount - invoiceModel.footer.RoundOff;

                // QR Code data
                string indate = Helper.ConvertNumberToDate(invoiceModel.invoiceHeader.CreateDate, true);
                QRCodeText = invoiceModel.invoiceHeader.PONo + invoiceModel.invoiceHeader.Po_Item_Serial_No + invoiceModel.invoiceHeader.ProductCode + "\t" + invoiceModel.invoiceHeader.InvoiceNo + "\t" + indate + "\t";
                QRCodeText += Qty + "\t" + invoiceModel.invoiceHeader.Unit_UOM_ + "\t" + invoiceModel.lastSection.VehicleNo + "\t" + Rate + "\t" + string.Format("{0:f2}", TotalAmount) + "\t" + invoiceModel.invoiceHeader.Cuurency_Name + "\t" + invoiceModel.invoiceHeader.Gate_Location + "\tT\t" +
                            string.Format("{0:f2}", GrossAmount) + "\t0\t0\t0\t0\t0\t0\t0\tB\t" + invoiceModel.division.Division_Gstin_No + "\t" + invoiceModel.division.Division_State_Code + "\t"
                              + HSN_Code + "\tPARTNER\t" + string.Format("{0:f2}\t{1}\t{2:f2}\t{3:f2}\t{4:f2}", invoiceModel.footer.Freight, GrossAmount + invoiceModel.footer.Freight, invoiceModel.footer.IGSTVAlue, invoiceModel.footer.SGSTValue, invoiceModel.footer.CSTValue) + "\t0\tR";



                //TODO: increase font size
                iTextSharp.text.Font shrinkFont = null; //  dynamic font based on string length

                Font font7 = FontFactory.GetFont(FontFactory.HELVETICA, 7);
                Font font8 = FontFactory.GetFont(FontFactory.HELVETICA, 8);
                Font font09 = FontFactory.GetFont(FontFactory.HELVETICA, 9);
                Font font09Bold = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
                Font font09BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font11BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 11, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font12 = FontFactory.GetFont(FontFactory.HELVETICA, 12);
                Font font12Bold = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
                Font font12BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
                Font font18Bold = FontFactory.GetFont(FontFactory.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);
                Font font18 = FontFactory.GetFont(FontFactory.HELVETICA, 18);
                Font font14 = FontFactory.GetFont(FontFactory.HELVETICA, 14);
                Font fontAnchor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, BaseColor.BLUE);
                Chunk bullet = new Chunk("\u2022", font18);

                invoiceModel.invoiceHeader.CreateDate = Helper.ConvertNumberToDate(invoiceModel.invoiceHeader.CreateDate);
                invoiceModel.invoiceHeader.PODate = Helper.ConvertNumberToDate(invoiceModel.invoiceHeader.PODate);
                string AmountInWords = NumberToWords.ConvertNumberToWords(TotalAmount);


                #region InvoiceTypes
                foreach (string invoiceType in InvoiceTypes.InvoiceType)
                {
                    Filename = string.Format("{0}_{1}.pdf", Filepath, invoiceType);

                    fileStream = new System.IO.FileStream(Filename, System.IO.FileMode.Create);

                    string assemblyName = System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.ToString());
                    string assemblyVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    string imagepath = images;
                    //Rectangle rectangle = new Rectangle(PageSize.A4);
                    //rectangle.BorderColor = BaseColor.BLACK;
                    //rectangle.Border = Rectangle.BOX;

                    Document document = new Document(PageSize.A4, 0f, 0f, 9f, 0f);

                    PdfWriter writer = iTextSharp.text.pdf.PdfWriter.GetInstance(document, fileStream);
                    writer.PageEvent = new MyPdfPageEventHelper(dict);

                    document.AddTitle("Sumax Invoice");
                    document.AddSubject("Sumax Invoice");
                    document.AddKeywords("SUMAX ENGINEERING (P) LTD.");
                    document.AddCreator(".NET Assembly:" + assemblyName);
                    document.AddAuthor("Sumax");
                    document.AddProducer();

                    // step 4: open the doc
                    document.Open();

                    PdfPTable header = new PdfPTable(1);
                    header.LockedWidth = true;
                    header.TotalWidth = InvoiceSettings.TotalWidth;

                    PdfPCell pCell = new PdfPCell();
                    switch (invoiceType)
                    {
                        case "Original":
                            pCell = new PdfPCell(new Phrase("IRN Number: fe7b5d5707826b1fdbdc87db5cfa4f635849f3e1bf8e1bc6d86b1491ae48968d", font09Bold));
                            break;
                        case "Duplicate":
                            pCell = new PdfPCell(new Phrase(invoiceType + " for Transporter", font09Bold));
                            break;
                        case "Triplicate":
                            pCell = new PdfPCell(new Phrase(invoiceType + " for Supplier", font09Bold));
                            break;
                        default:
                            pCell = new PdfPCell(new Phrase(invoiceType, font09Bold));
                            break;
                    }
                    pCell.HorizontalAlignment = 2;
                    pCell.Border = 0;
                    pCell.PaddingBottom = InvoiceSettings.CellPadding;
                    header.AddCell(pCell);
                    document.Add(header);

                    iTextSharp.text.Image imgLogo = iTextSharp.text.Image.GetInstance(Path.Combine(imagepath, "sumax-logo-invoice.png"));
                    imgLogo.ScaleAbsoluteWidth(140f);
                    imgLogo.ScaleAbsoluteHeight(60f);
                    imgLogo.PaddingTop = 10f;

                    //float TotalWidth = 523f;

                    PdfPTable cDetails = new PdfPTable(2);
                    PdfPTable Address = new PdfPTable(2);
                    PdfPTable InvoiceNo = new PdfPTable(4);
                    PdfPTable BodyData = new PdfPTable(9);
                    PdfPTable Footer = new PdfPTable(3);
                    PdfPTable LastSection = new PdfPTable(2);


                    cDetails.LockedWidth = true;
                    cDetails.PaddingTop = 5f;
                    cDetails.TotalWidth = InvoiceSettings.TotalWidth;

                    Address.LockedWidth = true;
                    Address.TotalWidth = InvoiceSettings.TotalWidth;

                    BodyData.LockedWidth = true;
                    BodyData.TotalWidth = InvoiceSettings.TotalWidth;

                    Footer.LockedWidth = true;
                    Footer.TotalWidth = InvoiceSettings.TotalWidth;

                    LastSection.LockedWidth = true;
                    LastSection.TotalWidth = InvoiceSettings.TotalWidth;

                    PdfPCell cell = new PdfPCell();
                    cell.PaddingTop = 5f;
                    cell = new PdfPCell(imgLogo, false);
                    cell.PaddingTop = 10f;
                    cell.VerticalAlignment = 1;
                    cell.HorizontalAlignment = 1;
                    cDetails.AddCell(cell);

                    Phrase phrase = new Phrase();

                    // right side
                    PdfPTable RCompDetails = new PdfPTable(1);


                    cell = new PdfPCell(new Phrase("SUMAX ENGINEERING (P) LTD.", font12Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 0;
                    RCompDetails.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.division.Division_Address, font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 0;
                    cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                    RCompDetails.AddCell(cell);
                    if (!string.IsNullOrEmpty(invoiceModel.division.Division_Address_1))
                    {
                        cell = new PdfPCell(new Phrase(invoiceModel.division.Division_Address_1, font09));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 0;
                        cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                        RCompDetails.AddCell(cell);
                    }

                    // CIN Number
                    //cell = new PdfPCell(new Phrase(" ", font09));
                    //cell.Border = 0;
                    //cell.HorizontalAlignment = 0;
                    //cell.PaddingBottom = InvoiceSettings.CellPadding;
                    //RCompDetails.AddCell(cell);

                    #region StateName 
                    PdfPTable StateName = new PdfPTable(3);
                    float[] widths = { 1.2f, 0.3f, 2.5f };
                    StateName.DefaultCell.Padding = 5f;
                    StateName.DefaultCell.Border = Rectangle.NO_BORDER;
                    StateName.SetWidths(widths);

                    cell = new PdfPCell(new Phrase("State Name", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.division.Division_State_Name, font09));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase("GSTIN No", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.division.Division_Gstin_No, font09));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Email", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.division.Divivsion_Email, font09));
                    cell.Border = 0;
                    StateName.AddCell(cell);

                    cell = new PdfPCell(StateName);
                    cell.Border = 0;

                    RCompDetails.AddCell(cell);

                    #endregion
                    // header
                    cDetails.AddCell(RCompDetails);
                    cDetails.PaddingTop = 5f;
                    document.Add(cDetails);
                    // header end

                    #region InvoiceNo
                    InvoiceNo.TotalWidth = InvoiceSettings.TotalWidth;
                    InvoiceNo.DefaultCell.PaddingTop = 10f;
                    InvoiceNo.DefaultCell.PaddingBottom = 10f;
                    InvoiceNo.LockedWidth = true;
                    InvoiceNo.DefaultCell.Border = Rectangle.BOX;

                    phrase = new Phrase(string.Format("Invoice No  :     "), font09Bold);
                    phrase.Add(new Chunk(string.Format("{0}", invoiceModel.invoiceHeader.InvoiceNo), font09));
                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    InvoiceNo.AddCell(cell);

                    phrase = new Phrase("Date  :      ", font09Bold);
                    phrase.Add(new Chunk(string.Format("{0}", invoiceModel.invoiceHeader.CreateDate), font09));
                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    InvoiceNo.AddCell(cell);

                    phrase = new Phrase("PO No  :", font09Bold);
                    phrase.Add(new Chunk(string.Format(" {0}", invoiceModel.invoiceHeader.PONo), font09));

                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    InvoiceNo.AddCell(cell);

                    phrase = new Phrase("PO Date  :     ", font09Bold);
                    phrase.Add(new Chunk(string.Format("{0}", invoiceModel.invoiceHeader.PODate), font09));

                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    InvoiceNo.AddCell(cell);

                    document.Add(InvoiceNo);
                    #endregion

                    #region Address
                    PdfPTable LAddress = new PdfPTable(3);
                    LAddress.DefaultCell.HorizontalAlignment = 0;
                    LAddress.SetWidths(widths);

                    //cell = new PdfPCell(new Phrase("Name", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    cell = new PdfPCell(new Paragraph("Buyer", font11BoldUnderline));
                    cell.Border = 0;
                    cell.Colspan = 3;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.address.Name, font09Bold));
                    cell.Border = 0;
                    cell.Colspan = 3;
                    LAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Address", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    // Customer Address
                    //PdfPTable CustomerAddr = new PdfPTable(1);

                    //cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.buyer_address), font09));
                    //cell.Border = 0;
                    //cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                    //CustomerAddr.AddCell(cell);

                    shrinkFont = font09;
                    //if (!string.IsNullOrEmpty(invoiceModel.address.buyer_address))
                    //{
                    //    if (invoiceModel.address.buyer_address.Length < 20) shrinkFont = font09;
                    //    else if (invoiceModel.address.buyer_address.Length < 35) shrinkFont = font8;
                    //    else shrinkFont = font7;
                    //}

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.buyer_address), shrinkFont));
                    cell.Border = 0;
                    cell.Colspan = 3;
                    cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                    LAddress.AddCell(cell);

                    //Customer Address End

                    cell = new PdfPCell(new Phrase("GSTIN", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    // GSTIN and state code

                    //PdfPTable gstin = new PdfPTable(3);
                    phrase = new Phrase(string.Format("{0}", invoiceModel.address.GSTIN), font09);
                    phrase.Add(new Chunk("  State Code: ", font09Bold));
                    phrase.Add(new Chunk(invoiceModel.address.Buyer_State_Code, font09));

                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    // GSTIN and state code end

                    cell = new PdfPCell(new Phrase("State Name", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", string.IsNullOrEmpty(invoiceModel.address.Buyer_StateName) ? "" : invoiceModel.address.Buyer_StateName.ToUpper()), font09));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Contact Person", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.ContactPerson), font09));
                    cell.Border = 0;
                    LAddress.AddCell(cell);

                    // Mobile number
                    //cell = new PdfPCell(new Phrase("Mobile No", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.MobileNo), font09));
                    //cell.Border = 0;
                    //LAddress.AddCell(cell);

                    Address.AddCell(LAddress);

                    PdfPTable RAddress = new PdfPTable(3);
                    RAddress.SetWidths(widths);
                    RAddress.DefaultCell.HorizontalAlignment = 0;
                    RAddress.DefaultCell.Border = Rectangle.BOX;

                    // delivery location align center 
                    //cell = new PdfPCell(new Phrase(""));
                    //cell.Border = 0;
                    //RAddress.AddCell(cell);

                    cell = new PdfPCell(new Paragraph("Delivery Location", font11BoldUnderline));
                    cell.Colspan = 3;
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Name", font09Bold));
                    //cell.Border = 0;
                    //RAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.Name), font09Bold));
                    cell.Border = 0;
                    cell.Colspan = 3;
                    RAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Shipping Address", font09Bold));
                    //cell.Border = 0;
                    //RAddress.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //RAddress.AddCell(cell);

                    shrinkFont = font09;
                    //if (!string.IsNullOrEmpty(invoiceModel.address.Delivery_Location_Address1))
                    //{
                    //    if (invoiceModel.address.Delivery_Location_Address1.Length < 60) shrinkFont = font09;
                    //    else if (invoiceModel.address.Delivery_Location_Address1.Length < 120) shrinkFont = font8;
                    //    else shrinkFont = font7;
                    //}
                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.Delivery_Location_Address1), shrinkFont));
                    cell.Border = 0;
                    cell.Colspan = 3;
                    cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                    RAddress.AddCell(cell);

                    if (!string.IsNullOrEmpty(invoiceModel.address.Delivery_Location_Address2))
                    {
                        cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.Delivery_Location_Address2), shrinkFont));
                        cell.Border = 0;
                        cell.Colspan = 3;
                        cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                        RAddress.AddCell(cell);
                    }


                    cell = new PdfPCell(new Phrase("GSTIN", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    phrase = new Phrase(string.Format("{0}", invoiceModel.address.delivery_location_gstin_no), font09);
                    phrase.Add(new Chunk("  State Code: ", font09Bold));
                    phrase.Add(new Chunk(invoiceModel.address.delivery_location_state_code, font09));

                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Place of Supply", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", string.IsNullOrEmpty(invoiceModel.address.RPlaceofSupply) ? " " : invoiceModel.address.RPlaceofSupply.ToUpper()), font09));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Mobile No", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.address.MobileNo), font09));
                    cell.Border = 0;
                    RAddress.AddCell(cell);

                    Address.AddCell(RAddress);

                    document.Add(Address);
                    #endregion

                    #region BodyData
                    BodyData.DefaultCell.HorizontalAlignment = 1;
                    BodyData.SetWidths(InvoiceSettings.BodyWidths);
                    cell = new PdfPCell(new Phrase("S.NO", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("PartyProductCode", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("PartyProductName", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("HSN Code", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("UOM", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Quantity", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Rate", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("GST %", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Amount", font09));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                    cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    BodyData.AddCell(cell);

                    int productCount = 20 - tempBodyData.Count;
                    int i = 0;
                    //Looping the data here
                    foreach (BodyData item in tempBodyData)
                    {
                        //GrossAmount += float.Parse(item.Amount, CultureInfo.InvariantCulture.NumberFormat);
                        //invoiceModel.footer.CSTValue += float.Parse(item.CGST, CultureInfo.InvariantCulture.NumberFormat);
                        //invoiceModel.footer.SGSTValue += float.Parse(item.SGST, CultureInfo.InvariantCulture.NumberFormat);
                        //invoiceModel.footer.IGSTVAlue += float.Parse(item.IGST, CultureInfo.InvariantCulture.NumberFormat);
                        //invoiceModel.footer.Freight += float.Parse(item.Frieght, CultureInfo.InvariantCulture.NumberFormat);

                        cell = new PdfPCell(new Phrase(string.Format("{0}", ++i), font09));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.PaddingBottom = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        shrinkFont = font09;
                        if (!string.IsNullOrEmpty(invoiceModel.mCoreProduct.Code))
                        {
                            if (invoiceModel.mCoreProduct.Code.Length < 16) shrinkFont = font09;
                            else if (invoiceModel.mCoreProduct.Code.Length < 24) shrinkFont = font8;
                            else shrinkFont = font7;
                        }

                        cell = new PdfPCell(new Phrase(invoiceModel.mCoreProduct.Code, shrinkFont));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        shrinkFont = font09;
                        if (!string.IsNullOrEmpty(invoiceModel.mCoreProduct.Name))
                        {
                            if (invoiceModel.mCoreProduct.Name.Length < 20) shrinkFont = font09;
                            else if (invoiceModel.mCoreProduct.Name.Length < 35) shrinkFont = font8;
                            else shrinkFont = font7;
                        }
                        cell = new PdfPCell(new Phrase(invoiceModel.mCoreProduct.Name, shrinkFont));
                        cell.Border = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(item.HSNCode, font09));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.invoiceHeader.Unit_UOM_), font09));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(string.Format("{0:f2}", item.Quantity), font09));
                        cell.HorizontalAlignment = 2;
                        cell.Border = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(string.Format("{0:f2}", float.Parse(item.Rate, CultureInfo.InvariantCulture.NumberFormat)), font09));
                        cell.Border = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.HorizontalAlignment = 2;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(string.Format("{0:f2}", float.Parse(item.GSTPerct, CultureInfo.InvariantCulture.NumberFormat)), font09));
                        cell.Border = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.HorizontalAlignment = 2;
                        BodyData.AddCell(cell);

                        cell = new PdfPCell(new Phrase(string.Format("{0:f2}", item.Amount), font09));
                        cell.Border = 0;
                        cell.PaddingTop = InvoiceSettings.BodyHdrPadding;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.HorizontalAlignment = 2;
                        BodyData.AddCell(cell);
                    }
                    i = 0;
                    do
                    {
                        cell = new PdfPCell(new Phrase(" ", font09));
                        cell.Border = 0;
                        cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                        BodyData.AddCell(cell);

                        for (int j = 0; j < 8; j++)
                        {
                            cell = new PdfPCell(new Phrase("", font09));
                            cell.Border = 0;
                            cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                            BodyData.AddCell(cell);
                        }

                        i++;
                    } while (i < productCount);

                    document.Add(BodyData);
                    #endregion



                    #region footer1
                    Footer.SetWidths(InvoiceSettings.FooterWidths);

                    // Footer 
                    PdfPTable Footer1 = new PdfPTable(3);
                    Footer1.SetWidths(InvoiceSettings.LastSectionWidths);
                    Footer1.HorizontalAlignment = 2;
                    Footer1.TotalWidth = InvoiceSettings.FooterTotalWidth;
                    Footer1.LockedWidth = true;

                    cell = new PdfPCell(new Phrase("Gross Amount", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", GrossAmount), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(Footer1);
                    cell.HorizontalAlignment = 2;
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.AmountInWordsPadding;
                    cell.PaddingBottom = InvoiceSettings.AmountInWordsPadding;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                    cell.Colspan = 3;
                    Footer.AddCell(cell);


                    //Footer1 = new PdfPTable(1);
                    ////Footer1.SetWidths(new float[] { 0.9f, 1.1f });
                    //Footer1.HorizontalAlignment = 0;

                    //cell = new PdfPCell(new Phrase("Credit Days", font09));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(invoiceModel.footer.CreditDays, font09));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Delivary Note", font09));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(invoiceModel.footer.Deliver_Note, font09));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("Gate Location :", font09Bold)); 
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(invoiceModel.invoiceHeader.Gate_Location, font09));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    phrase = new Phrase("Gate Location : ", font09Bold);
                    phrase.Add(new Chunk(invoiceModel.invoiceHeader.Gate_Location, font09));
                    cell = new PdfPCell(phrase);
                    cell.HorizontalAlignment = 0;
                    cell.Border = 0;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    Footer.AddCell(cell);


                    // System.Drawing.Bitmap QRCodeBitmap = Helper.GenerateQRCoder(QRCodeText);
                    iTextSharp.text.Image QRCode = iTextSharp.text.Image.GetInstance(Helper.GenerateQRCoder(QRCodeText), System.Drawing.Imaging.ImageFormat.Png);
                    QRCode.ScaleAbsoluteWidth(85f);
                    QRCode.ScaleAbsoluteHeight(85f);

                    cell = new PdfPCell(QRCode);
                    cell.HorizontalAlignment = 2;
                    cell.VerticalAlignment = 1;
                    cell.PaddingTop = 2f;
                    cell.PaddingRight = 20f;
                    cell.Border = 0;
                    Footer.AddCell(cell);

                    Footer1 = new PdfPTable(3);
                    Footer1.SetWidths(InvoiceSettings.LastSectionWidths);
                    Footer1.HorizontalAlignment = 2;
                    Footer1.TotalWidth = InvoiceSettings.FooterTotalWidth;
                    Footer1.LockedWidth = true;

                    cell = new PdfPCell(new Phrase("Frieght", font09Bold));
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.footerCellPadding;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.Freight), font09Bold));
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.footerCellPadding;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("CGST Value", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.CSTValue), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("SGST Value", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.SGSTValue), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("IGST Value", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.IGSTVAlue), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("TCS Value", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.TCS), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase("RoundOff", font09Bold));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(":", font09Bold));
                    //cell.Border = 0;
                    //Footer1.AddCell(cell);

                    //cell = new PdfPCell(new Phrase(string.Format("{0:f2}", invoiceModel.footer.RoundOff), font09Bold));
                    //cell.Border = 0;
                    //cell.HorizontalAlignment = 2;
                    //Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Total Amount", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.footerCellPadding;
                    Footer1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    Footer1.AddCell(cell);

                    //float TotalAmount = GrossAmount + invoiceModel.footer.Freight + invoiceModel.footer.CSTValue + invoiceModel.footer.SGSTValue + invoiceModel.footer.IGSTVAlue + invoiceModel.footer.RoundOff;

                    cell = new PdfPCell(new Phrase(string.Format("{0:f2}", TotalAmount), font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 2;
                    Footer1.AddCell(cell);


                    cell = new PdfPCell(Footer1);
                    cell.HorizontalAlignment = 2;
                    cell.Border = 0;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    Footer.AddCell(cell);


                    phrase = new Phrase("Amount In Words :   ", font09Bold);
                    phrase.Add(new Chunk(string.Format("Rupees {0} Only", AmountInWords), font09));

                    cell = new PdfPCell(phrase);
                    cell.HorizontalAlignment = 0;
                    cell.Colspan = 3;
                    cell.PaddingTop = InvoiceSettings.AmountInWordsPadding;
                    cell.PaddingBottom = InvoiceSettings.AmountInWordsPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    Footer.AddCell(cell);

                    document.Add(Footer);

                    #endregion

                    #region Last Section

                    PdfPTable LastSection1 = new PdfPTable(3);
                    LastSection1.SetWidths(InvoiceSettings.LastSectionWidths);
                    LastSection1.HorizontalAlignment = 0;


                    cell = new PdfPCell(new Phrase("Dispatch Through", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.lastSection.DispatchTrough), font09));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);


                    cell = new PdfPCell(new Phrase("Transporter Name", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.lastSection.TranspoterName, font09));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Vehicle No", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.lastSection.VehicleNo, font09));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("LR Number", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.invoiceHeader.LR_Number, font09));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    LastSection1.AddCell(cell);


                    cell = new PdfPCell(LastSection1);
                    cell.Border = 0;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    LastSection.AddCell(cell);

                    LastSection1 = new PdfPTable(3);
                    LastSection1.SetWidths(InvoiceSettings.LastSectionWidths);
                    LastSection1.HorizontalAlignment = 0;

                    cell = new PdfPCell(new Phrase("Company's Bank Details", font09Bold));
                    cell.Border = 0;
                    cell.PaddingBottom = InvoiceSettings.lastSectionPadding;
                    cell.Colspan = 3;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Name of the bank", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.lastSection.NameOfBank), font09));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Account No", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.lastSection.AccountNo), font09));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("IFSC Code", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(invoiceModel.lastSection.IFSCCode, font09));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase("Branch", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(":", font09Bold));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(new Phrase(string.Format("{0}", invoiceModel.lastSection.Branch), font09));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);


                    cell = new PdfPCell(LastSection1);
                    cell.Border = 0;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    LastSection.AddCell(cell);

                    LastSection1 = new PdfPTable(1);
                    LastSection1.HorizontalAlignment = 0;

                    cell = new PdfPCell(new Phrase("Declaration", font09BoldUnderline));
                    cell.Border = 0;
                    LastSection1.AddCell(cell);
                    new Phrase();

                    cell = new PdfPCell(new Phrase(ApplicationSettings.Declarations, font09));
                    cell.Border = 0;
                    cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(LastSection1);
                    cell.Border = 0;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                    cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    LastSection.AddCell(cell);

                    LastSection1 = new PdfPTable(1);
                    LastSection1.HorizontalAlignment = 0;

                    cell = new PdfPCell(new Phrase(ApplicationSettings.CompanyName, font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    LastSection1.AddCell(cell);
                    i = 0;
                    while (i < 3)
                    {
                        i++;
                        cell = new PdfPCell(new Phrase(" ", font09Bold));
                        cell.Border = 0;
                        cell.HorizontalAlignment = 1;
                        LastSection1.AddCell(cell);
                    }

                    cell = new PdfPCell(new Phrase(ApplicationSettings.AuthSign, font09Bold));
                    cell.Border = 0;
                    cell.HorizontalAlignment = 1;
                    LastSection1.AddCell(cell);

                    cell = new PdfPCell(LastSection1);
                    cell.Border = 0;
                    cell.BorderWidthBottom = InvoiceSettings.BoderWidth;
                    cell.BorderWidthTop = InvoiceSettings.BoderWidth;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    LastSection.AddCell(cell);

                    document.Add(LastSection);
                    #endregion



                    document.Close();
                }

                #endregion

                flag = true;
            }
            catch (Exception ex)
            {
                flag = false;
                Helper.WriteLog(ApplicationLogType.ERROR.ToString(), MethodBase.GetCurrentMethod().Name, ex.Message + " | " + ex.StackTrace.ToString());
                throw ex;
            }

            return flag;
        }

        #endregion

    }




    public class Helper
    {
        //private static string connString = Convert.ToString(new sumaxEntities().Database.Connection.ConnectionString);
        private static string connString = Convert.ToString(Convert.ToString(ConfigurationManager.ConnectionStrings["ConnectionSumax"]));
        public static int? WriteLog(string LogType, string Method, string Action)
        {
            SqlConnection con = new SqlConnection(connString);
            int res = 0;
            try
            {
                SqlCommand cmd = new SqlCommand("SPY_WriteLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("LogType", LogType);
                cmd.Parameters.AddWithValue("Module", Method);
                cmd.Parameters.AddWithValue("Action", Action);
                con.Open();
                res = cmd.ExecuteNonQuery();
                if (res != 0)
                {
                    //Write Error Log
                }

            }

            catch (Exception ex)
            {
                //WriteLog(ApplicationLogType.ERROR.ToString(), "Helper.WriteLog", ex.StackTrace.ToString());
                res = 0;
            }
            finally
            {
                con.Close();
            }
            return res;
        }

        #region Email
        public static int SendEmail(EmailModelDTO model)
        {
            try
            {
                using (System.Net.Mail.MailMessage mm = new System.Net.Mail.MailMessage("socialtest1990@gmail.com", model.EmailTo))
                {
                    mm.Subject = model.Subject;
                    mm.Body = model.Body;
                    //if (!string.IsNullOrEmpty(model.AttachmentPath))
                    //{
                    //    Attachment at = new Attachment(model.AttachmentPath);
                    //    mm.Attachments.Add(at);
                    //}
                    mm.IsBodyHtml = true;
                    mm.From = new MailAddress("info@exadataconsulting.com", "Sumax Engineering");
                    using (SmtpClient smtp = new SmtpClient())
                    {
                        smtp.Host = "smtp.gmail.com";
                        smtp.EnableSsl = true;
                        NetworkCredential NetworkCred = new NetworkCredential("sumaxinvoice@gmail.com", "Sumax@1994");
                        smtp.UseDefaultCredentials = true;
                        smtp.Credentials = NetworkCred;
                        smtp.Port = 587;
                        smtp.Send(mm);
                        return 1;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion

        public static string GetPasswordHash(string password = "admin123123455")
        {
            using (var sha1 = new SHA1CryptoServiceProvider())
            {
                var hash = Encoding.UTF8.GetBytes(password);
                var generatedHash = sha1.ComputeHash(hash);
                //return generatedHash;
                var generatedHashString = Convert.ToBase64String(generatedHash);
                //return "4NMznJlRfUmdAvv3LXtHl+VPp04="; //
                return generatedHashString;
            }
        }

        public static string ConvertNumberToDate(string strNumber, bool IsQr = false)
        {
            StringBuilder sbDate = new StringBuilder();

            if (!string.IsNullOrEmpty(strNumber))
            {
                long Number;
                if (long.TryParse(strNumber, out Number) && Number > 0)
                {
                    if (IsQr)
                    {
                        sbDate.Append((int)Number / 65536);
                        sbDate.Append(((int)(Number / 256) % 256).ToString("00"));
                        sbDate.Append(((int)Number % 256).ToString("00"));
                    }
                    else
                    {
                        sbDate.Append(((int)Number % 256).ToString("00") + "-"); // Date
                        sbDate.Append(((int)(Number / 256) % 256).ToString("00") + "-"); // Month
                        sbDate.Append((int)Number / 65536); // Year
                    }
                }
            }
            return sbDate.ToString();
        }
        public static string ConvertNumberToDate(string strNumber, string strDateFormat)
        {
            StringBuilder sbDate = new StringBuilder();

            if (!string.IsNullOrEmpty(strNumber))
            {
                long Number;
                if (long.TryParse(strNumber, out Number) && Number > 0)
                {
                    int Year = ((int)Number / 65536) % 100;
                    string Month = ((int)(Number / 256) % 256).ToString("00");
                    string Date = ((int)Number % 256).ToString("00");
                    switch (strDateFormat)
                    {
                        case "ddmmyy":
                            sbDate.Append(Date);
                            sbDate.Append(Month);
                            sbDate.Append(Year);
                            break;
                        case "mmddyy":
                            sbDate.Append(Month);
                            sbDate.Append(Date);
                            sbDate.Append(Year);
                            break;
                        default:
                            sbDate.Append(Year);
                            sbDate.Append(Month);
                            sbDate.Append(Date);
                            break;
                    }
                }
            }
            return sbDate.ToString();
        }
        public static System.Drawing.Bitmap GenerateQRCoder(string Text)
        {
            if (!string.IsNullOrEmpty(Text))
                Text = Text.ToUpper();
            else
                Text = "SUMAX";
            QRCode qrCode = new QRCode(new QRCodeGenerator().CreateQrCode(Text, QRCodeGenerator.ECCLevel.Q));
            return qrCode.GetGraphic(20);
        }
    }

    public class NumberToWords
    {
        public static string ConvertNumberToWords(double number)
        {
            string ss = NumberToText(Convert.ToInt64(Math.Floor(number)));

            var regex = new System.Text.RegularExpressions.Regex("(?<=[\\.])[0-9]+");
            if (regex.IsMatch(number.ToString("c2")))
            {
                long decimal_places = Convert.ToInt64(regex.Match(number.ToString("c2")).Value);
                if (decimal_places > 0)
                    ss += " " + NumberToText(decimal_places) + " paise";
            }


            return ss;

        }
        public static string NumberToText(long number)
        {
            if (number == 0) return "Zero";

            if (number == -2147483648) return "Minus Two Hundred and Fourteen Crore Seventy Four Lakh Eighty Three Thousand Six Hundred and Forty Eight";

            long[] num = new long[4];
            int first = 0;
            long u, h, t;
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            if (number < 0)
            {
                sb.Append("Minus ");
                number = -number;
            }

            string[] words0 = { "", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine " };

            string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };

            string[] words2 = { "Twenty ", "Thirty ", "Fourty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };

            string[] words3 = { "Thousand ", "Lakh ", "Crore " };

            num[0] = number % 1000; // units
            num[1] = number / 1000;
            num[2] = number / 100000;
            num[1] = num[1] - 100 * num[2]; // thousands
            num[3] = number / 10000000; // crores
            num[2] = num[2] - 100 * num[3]; // lakhs

            for (int i = 3; i > 0; i--)
            {
                if (num[i] != 0)
                {
                    first = i;
                    break;
                }
            }

            for (int i = first; i >= 0; i--)
            {
                if (num[i] == 0) continue;
                u = num[i] % 10; // ones
                t = num[i] / 10;
                h = num[i] / 100; // hundreds
                t = t - 10 * h; // tens

                if (h > 0) sb.Append(words0[h] + "Hundred ");
                if (u > 0 || t > 0)
                {
                    if ((h > 0 || i == 0) && number > 100) sb.Append("and ");

                    if (t == 0)
                        sb.Append(words0[u]);
                    else if (t == 1)
                        sb.Append(words1[u]);
                    else
                        sb.Append(words2[t - 2] + words0[u]);
                }
                if (i != 0) sb.Append(words3[i - 1]);
            }
            return sb.ToString().TrimEnd();
        }

    }

    public class MyPdfPageEventHelper : PdfPageEventHelper
    {
        Dictionary<string, int> dict = null;
        public DyanmicInvoiceModel invoiceModel { get; set; }
        public string invoiceType { get; set; }
        public string imagepath { get; set; }

        public MyPdfPageEventHelper(Dictionary<string, int> dict)
        {

            this.dict = dict;
        }
        public MyPdfPageEventHelper(DyanmicInvoiceModel inv, string invoiceType, string imagepath)
        {
            invoiceModel = inv;
            this.invoiceType = invoiceType;
            this.imagepath = imagepath;
        }


        public override void OnStartPage(PdfWriter writer, Document document)
        {
            #region fonts

            float[] widths = { 1.2f, 0.3f, 2.5f };

            PdfPTable RCompDetails = new PdfPTable(1);
            PdfPCell cell = new PdfPCell();
            Phrase phrase = new Phrase();

            Font font7 = FontFactory.GetFont(FontFactory.HELVETICA, 7);
            Font font8 = FontFactory.GetFont(FontFactory.HELVETICA, 8);
            Font font09 = FontFactory.GetFont(FontFactory.HELVETICA, 9);
            Font font09Bold = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
            Font font09BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 9, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
            Font font11BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 11, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
            Font font12 = FontFactory.GetFont(FontFactory.HELVETICA, 12);
            Font font12Bold = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD, BaseColor.BLACK);
            Font font12BoldUnderline = FontFactory.GetFont(FontFactory.HELVETICA, 12, Font.BOLD | Font.UNDERLINE, BaseColor.BLACK);
            Font font18Bold = FontFactory.GetFont(FontFactory.HELVETICA, 18, Font.BOLD, BaseColor.BLACK);
            Font font18 = FontFactory.GetFont(FontFactory.HELVETICA, 18);
            Font font14 = FontFactory.GetFont(FontFactory.HELVETICA, 14);
            Font fontAnchor = FontFactory.GetFont(FontFactory.HELVETICA, 10, Font.UNDERLINE, BaseColor.BLUE);
            Chunk bullet = new Chunk("\u2022", font18);

            #endregion

            #region Header
            PdfPTable header = new PdfPTable(3);
            header.LockedWidth = true;
            header.TotalWidth = InvoiceSettings.TotalWidth;
            header.SetWidths(new float[] { 80f, 320f, 150f });

            PdfPCell IRNDispCell = new PdfPCell(new iTextSharp.text.Phrase("")) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, Border = 0, PaddingLeft = 0, PaddingRight = 0, PaddingBottom = 7f };
            header.AddCell(IRNDispCell);
            PdfPCell IRNValueCell = new PdfPCell(new iTextSharp.text.Phrase("")) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT, Border = 0, PaddingLeft = 0, PaddingRight = 0, PaddingBottom = 7f };
            header.AddCell(IRNValueCell);
            PdfPCell pCell = new PdfPCell();
            switch (invoiceType)
            {
                case "Original":
                    pCell = new PdfPCell(new Phrase(invoiceType + " for Recipient", font09Bold));
                    break;
                case "Duplicate":
                    pCell = new PdfPCell(new Phrase(invoiceType + " for Transporter", font09Bold));
                    break;
                case "Triplicate":
                    pCell = new PdfPCell(new Phrase(invoiceType + " for Supplier", font09Bold));
                    break;
                default:
                    pCell = new PdfPCell(new Phrase(invoiceType, font09Bold));
                    break;
            }
            pCell.HorizontalAlignment = 2;
            pCell.Border = 0;
            pCell.PaddingBottom = InvoiceSettings.CellPadding;
            header.AddCell(pCell);

            document.Add(header);

            #endregion

            #region Log Section 

            var logSec = (List<TemplateDetailResult>)invoiceModel.LogoSection;

            iTextSharp.text.Image imgLogo;
            string LogoPath = Path.Combine(imagepath, "sumax-logo-invoice.png");

            if (logSec != null && logSec.Count > 0)
                LogoPath = Path.Combine(imagepath, logSec[0].FieldValue);

            if (!File.Exists(LogoPath))
                LogoPath = Path.Combine(imagepath, "sumax-logo-invoice.png");

            imgLogo = iTextSharp.text.Image.GetInstance(LogoPath);
            imgLogo.ScaleAbsoluteWidth(100f);
            imgLogo.ScaleAbsoluteHeight(60f);
            imgLogo.PaddingTop = 10f;


            PdfPTable cDetails = new PdfPTable(3);
            cDetails.LockedWidth = true;
            cDetails.PaddingTop = 5f;
            cDetails.SetWidths(new float[] { 150f, 100f, 150f });
            cDetails.TotalWidth = InvoiceSettings.TotalWidth;

            cell.PaddingTop = 5f;
            cell = new PdfPCell(imgLogo, false);
            cell.PaddingTop = 10f;
            cell.PaddingBottom = 5f;
            cell.VerticalAlignment = 1;
            cell.HorizontalAlignment = 1;
            cell.DisableBorderSide(PdfPCell.RIGHT_BORDER);
            cDetails.AddCell(cell);

            #endregion
            #region QRCodeLogo
            ////PdfPCell qrLogoCell = new PdfPCell();// (new Phrase("mahesh Kumar", font12)) { HorizontalAlignment = iTextSharp.text.Element.ALIGN_CENTER, Border = 0, Padding=0f };
            ////qrLogoCell.PaddingTop = 5f;
            ////qrLogoCell = new PdfPCell(imgLogo, false);
            ////qrLogoCell.PaddingTop = 10f;
            ////qrLogoCell.PaddingBottom = 5f;
            ////qrLogoCell.VerticalAlignment = 1;
            ////qrLogoCell.HorizontalAlignment = 1;
            ////cDetails.AddCell(qrLogoCell);

            string QRCodeText = logSec[1].FieldValue;
            Image QRCode = Image.GetInstance(Helper.GenerateQRCoder(QRCodeText), System.Drawing.Imaging.ImageFormat.Png);
            QRCode.ScaleAbsoluteWidth(85f);
            QRCode.ScaleAbsoluteHeight(85f);

            PdfPCell qrLogoCell = new PdfPCell(QRCode);
            qrLogoCell.HorizontalAlignment = 2;
            qrLogoCell.VerticalAlignment = 1;
            qrLogoCell.PaddingTop = 2f;
            qrLogoCell.PaddingRight = 20f;
            qrLogoCell.PaddingBottom = 2f;
            qrLogoCell.Border = PdfPCell.BOTTOM_BORDER | PdfPCell.TOP_BORDER| PdfPCell.RIGHT_BORDER;
            cell.DisableBorderSide(PdfPCell.LEFT_BORDER);
            cDetails.AddCell(qrLogoCell);
            #endregion



            #region Division Address

            widths = new float[] { 1.2f, 0.3f, 2.5f };

            RCompDetails = new PdfPTable(1);

            var divAddSec = (List<TemplateDetailResult>)invoiceModel.DivAddress;

            string DivName = ApplicationSettings.CompanyHeader;
            if (divAddSec != null)
            {
                DivName = divAddSec.Where(r => r.XmlField.Equals("DivisionName")).Select(r => r.FieldValue).FirstOrDefault();

                cell = new PdfPCell(new Phrase(DivName ?? ApplicationSettings.CompanyHeader, font12Bold));
                cell.Border = 0;
                cell.HorizontalAlignment = 0;
                RCompDetails.AddCell(cell);


                string[] addressList = new string[] { "67111426|Division_Address", "67111427|Division_Address_1" };
                var AddressDet = divAddSec.Where(a => addressList.Contains(a.XmlField)).FirstOrDefault();

                cell = new PdfPCell(new Phrase(AddressDet.FieldValue ?? "", font09));
                cell.Border = 0;
                cell.HorizontalAlignment = 0;
                cell.SetLeading(InvoiceSettings.cellLeading, InvoiceSettings.cellLeading);
                RCompDetails.AddCell(cell);

                PdfPTable DivAddressTable = new PdfPTable(3);
                foreach (var item in divAddSec)
                {
                    if (!addressList.Contains(item.XmlField) && !item.XmlField.Equals("DivisionName"))
                    {
                        DivAddressTable.DefaultCell.Padding = 5f;
                        DivAddressTable.DefaultCell.Border = Rectangle.NO_BORDER;
                        DivAddressTable.SetWidths(widths);

                        cell = new PdfPCell(new Phrase(item.DisplayField, font09Bold));
                        cell.Border = 0;
                        DivAddressTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(":", font09Bold));
                        cell.Border = 0;
                        DivAddressTable.AddCell(cell);

                        cell = new PdfPCell(new Phrase(item.FieldValue, font09));
                        cell.Border = 0;
                        DivAddressTable.AddCell(cell);
                    }
                }
                cell = new PdfPCell(DivAddressTable);
                cell.Border = 0;
                RCompDetails.AddCell(cell);
            }

            cDetails.AddCell(RCompDetails);
            cDetails.PaddingTop = 5f;
            document.Add(cDetails);

            #endregion

            #region PO Invoice                   

            var POInvoiceSec = (List<TemplateDetailResult>)invoiceModel.POInvoice;
            if (POInvoiceSec.Count() > 0)
            {
                PdfPTable InvoiceNo = new PdfPTable(POInvoiceSec.Count());

                InvoiceNo.TotalWidth = InvoiceSettings.TotalWidth;
                InvoiceNo.DefaultCell.PaddingTop = 10f;
                InvoiceNo.DefaultCell.PaddingBottom = 10f;
                InvoiceNo.LockedWidth = true;
                InvoiceNo.DefaultCell.Border = Rectangle.BOX;

                string[] arr1 = new string[] { "67110590|Customer_PO_Date", "Date" };
                int i = 0;
                foreach (var item in POInvoiceSec)
                {
                    string fieldValue = item.FieldValue;
                    if (arr1.Contains(item.XmlField))
                    {
                        fieldValue = Helper.ConvertNumberToDate(item.FieldValue, false);
                    }
                    phrase = new Phrase(string.Format(item.DisplayField + ": "), font09Bold);
                    phrase.Add(new Chunk(string.Format("{0}", fieldValue), font09));
                    cell = new PdfPCell(phrase);
                    cell.Border = 0;
                    cell.PaddingTop = InvoiceSettings.CellPadding;
                    cell.PaddingBottom = InvoiceSettings.CellPadding;
                    cell.BorderWidthRight = InvoiceSettings.BoderWidth;
                    if (i == 0)
                        cell.BorderWidthLeft = InvoiceSettings.BoderWidth;
                    InvoiceNo.AddCell(cell);
                    ++i;
                }
                document.Add(InvoiceNo);
            }

            #endregion
        }
        public override void OnEndPage(PdfWriter writer, Document document)
        {

        }

    }
    public class LineaBottom : IPdfPTableEvent
    {


        #region IPdfPTableEvent Members

        void IPdfPTableEvent.TableLayout(PdfPTable table, float[][] widths, float[] heights, int headerRows, int rowStart, PdfContentByte[] canvases)
        {
            int columns;
            Rectangle rect;
            int footer = widths.Length - table.FooterRows;
            int header = table.HeaderRows - table.FooterRows + 1;
            int ultima = footer - 1;
            if (ultima != -1)
            {
                columns = widths[ultima].Length - 1;
                rect = new Rectangle(widths[ultima][0], heights[ultima], widths[footer - 1][columns], heights[ultima + 1]);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = InvoiceSettings.BoderWidth;
                rect.Border = Rectangle.TOP_BORDER;
                //canvases[PdfPTable.TEXTCANVAS].SetFontAndSize(BaseFont.CreateFont(), 9f);
                //canvases[PdfPTable.TEXTCANVAS].NewlineShowText("Continued");
                canvases[PdfPTable.BASECANVAS].Rectangle(rect);


                columns = widths[0].Length - 1;
                rect = new Rectangle(widths[0][0], heights[0], widths[0][columns], heights[0]);
                rect.BorderColor = BaseColor.BLACK;
                rect.BorderWidth = InvoiceSettings.BoderWidth;
                rect.Border = Rectangle.TOP_BORDER;
                canvases[PdfPTable.BASECANVAS].Rectangle(rect);
            }
        }

        #endregion
    }

}