﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SumaxHeader.Master" AutoEventWireup="true" CodeBehind="QRCodeMgmt.aspx.cs" Inherits="Sumax_Hyundai.QRCodeMgmt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .pageUndercons {
            top: 38%;
            left: 35%;
            position: absolute;
        }
        hr {
    margin: auto;
    width: 40%;

}
    </style>
    <div class="pageUndercons">
        <h1>Page Under Construction</h1>
        <hr>
        <p></p>
    </div>



    <%--<div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <ul class="customer-list" id="">
            <li class="ripple">
                <a id="" data-toggle="collapse" data-target="#ri">Regular QR Code&nbsp;&nbsp;&nbsp;<i class="fas fa-minus-circle"></i>
                </a>
                <ul id="ri" class="collapse in submenu">
                    <li><a href="#">Add / Remove Field</a> </li>
                </ul>
            </li>

            <li class="ripple">
                <a id="" data-toggle="collapse" data-target="#hynd">Hyundai QR Code&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                </a>
                <ul id="hynd" class="collapse submenu">
                    <li><a href="#">Add / Remove Field</a> </li>
                </ul>
            </li>
            <li class="ripple">
                <a id="">TATA QR Code&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                </a>
            </li>
            <li class="ripple">
                <a id="">Nissan QR Code&nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i>
                </a>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
    <section id="main-content">
        <section class="content-wrapper">
            <div id="">


                <section class="panel">
                    <header class="panel-heading">
                        <b><span id="">Add New Field</span></b>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="row style-select">
                                <div class="col-md-12">
                                    <div class="subject-info-box-1">
                                        <label class="fields-label">Available Fields</label>
                                        <select multiple class="form-control" id="lstBox1">
                                            <option value="">Purchase Order Number</option>
                                            <option value="">PO Item Serial</option>
                                            <option value="">Part Number / Item number</option>
                                            <option value="">Invoice Number</option>
                                            <option value="">Invoice Date</option>
                                            <option value="">Invoice Quantity</option>
                                            <option value="">UOM</option>
                                            <option value="">Truck Number</option>
                                            <option value="">Net Price</option>
                                            <option value="">Invoice Value</option>
                                            <option value="">Currency</option>
                                            <option value="">Delivery Department</option>
                                            <option value="">Tariff Number</option>
                                            <option value="">Material Cost Value</option>
                                            <option value="">Basic Excise Duty Value</option>
                                            <option value="">Special Excise Duty Value</option>
                                            <option value="">Education Cess Value</option>
                                            <option value="">Secondary Education Cess Value</option>
                                            <option value="">Additional Duty Value</option>
                                            <option value="">LST VAT Value</option>
                                            <option value="">CST VAT Value</option>
                                            <option value="">Batch No</option>
                                            <option value="">GSTN</option>
                                            <option value="">Place of Supply</option>
                                            <option value="">HSN or SAC Code</option>
                                            <option value="">GST Partner</option>
                                            <option value="">Transport Cost</option>
                                            <option value="">Assesable Value (Material + Transport)</option>
                                            <option value="">IGST</option>
                                            <option value="">SGST</option>
                                            <option value="">CGST</option>
                                            <option value="">UGST</option>
                                            <option value="">Remarks</option>


                                        </select>
                                    </div>

                                    <div class="subject-info-arrows text-center">
                                        <br />
                                        <br />
                                        <input type='button' id='btnAllRight' value='>>' class="btn btn-default" /><br />
                                        <input type='button' id='btnRight' value='>' class="btn btn-default" /><br />
                                        <input type='button' id='btnLeft' value='<' class="btn btn-default" /><br />
                                        <input type='button' id='btnAllLeft' value='<<' class="btn btn-default" />
                                    </div>

                                    <div class="subject-info-box-2">
                                        <label class="fields-label">Fields to be displyed on QR Code</label>
                                        <select multiple class="form-control" id="lstBox2">
                                        </select>
                                    </div>
                                    <input type="submit" name="name" value="Save" class="btn btn-success" style="float: right" />
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>



    </section>
    <%-- Modal popup 
    <div id="myModal1" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content" id="modal-image">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="modal-title">Section Image</h4>
                </div>
                <div class="modal-body text-center">
                    <img alt="Alternate Text" id="sectionImage" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>--%>
</asp:Content>
