﻿using sumax.DAL;
using Sumax_Hyundai.BAL;
using Sumax_Hyundai.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sumax_Hyundai
{
    public partial class UserManagement : System.Web.UI.Page
    {
        List<tbl_templates> _Templates = new List<tbl_templates>();
        InvoiceBAL objInvoiceBAL = new InvoiceBAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                bool isAdmin = Convert.ToString(Session["UserType"]).Equals("A", StringComparison.InvariantCultureIgnoreCase);
                if (isAdmin)
                {
                    _Templates = objInvoiceBAL.GetAllTemplates();
                    BindTemplates();
                }
                else
                {
                    Response.Redirect("Default.aspx");
                }
            }
        }

        protected void BindTemplates()
        {
            ddlTemplates.DataSource = _Templates;
            ddlTemplates.DataTextField = "InvoiceTemplate";
            ddlTemplates.DataValueField = "Id";
            ddlTemplates.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Template" });
            ddlTemplates.DataBind();

            ddlTemplates1.DataSource = _Templates;
            ddlTemplates1.DataTextField = "InvoiceTemplate";
            ddlTemplates1.DataValueField = "Id";
            ddlTemplates1.Items.Insert(0, new ListItem() { Value = "-1", Text = "Select Template" });

            ddlTemplates1.DataBind();
        }

        protected string BindUserList()
        {
            StringBuilder sbUsers = new StringBuilder();

            try
            {
                List<tbl_Users> _lstUsers = new EntityDAL().BindUserList();
                if (_lstUsers != null && _lstUsers.Count > 0)
                {
                    foreach (tbl_Users user in _lstUsers)
                    {
                        string Template = "NA";

                        try
                        {
                            long[] TemplateIds = user.TemplateId.Split('|').Select(r => Convert.ToInt64(r)).ToArray();
                            if (TemplateIds != null)
                                Template = string.Join(",", _Templates.Where(r => TemplateIds.Contains(r.Id)).Select(r => r.InvoiceTemplate).ToArray());
                        }
                        catch (Exception ex)
                        {
                        }

                        sbUsers.Append("<tr>");
                        sbUsers.Append("<td>" + user.FirstName + " " + user.LastName + "</td>");
                        sbUsers.Append("<td>" + user.Email + "</td>");
                        sbUsers.Append("<td>" + Template + "</td>");
                        sbUsers.Append("<td>" + user.Status + "</td>");
                        sbUsers.Append("<td>" + user.LastLogin + "</td>");
                        //sbUsers.Append("<td><a href='javascript:void(0)' onclick='DeleteUser(" + user.UserId + ")' class='userdel'><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a></td>");
                        sbUsers.Append("<td><a href='javascript:void(0)' onclick='DeleteUser(" + user.UserId + ")' title='Delete' class='userdel'><i class=\"fa fa-trash\" aria-hidden=\"true\"></i></a>&nbsp;&nbsp; <a href='javascript:void(0)' onclick='Edituser(this)' title='Edit' data-userid='" + user.UserId + "' data-firstname='" + user.FirstName + "' data-lastname='" + user.LastName + "'data-email='" + user.Email + "' data-templateid='" + user.TemplateId + "' data-toggle='modal'  data-target='#Edituser' style='color:blue;font-size:16px;'><i class=\"fa fa-edit\" aria-hidden=\"true\"></i></a></td>");

                        sbUsers.Append("</tr>");
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.WriteLog("Error", "BindUserList", ex.Message + " | " + ex.StackTrace);
            }

            return sbUsers.ToString();
        } 
    }
}