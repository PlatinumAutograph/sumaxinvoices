﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace Sumax_Hyundai.Utilities
{
    public class JSON
    {
        public JSON()
        {

        }

        /// <summary>
        /// Simple JSON return for one key/value.  
        /// </summary>
        /// <param name="key">Name of the key</param>
        /// <param name="value">The value to return</param>
        /// <param name="context">The current HttpContext</param>
        /// <remarks>This is good for returning a simple JSON response such as {"DataError":"The process faild"} or {"Result":"1"}</remarks>
        public static void RespondWithJSON(string key, string value, HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json";
                context.Response.Write("{\"" + key + "\":\"" + value + "\"}");
                context.Response.End();
            }
            catch (ThreadAbortException)
            {
                //Do nothing.
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Allows you to return complex JSON response with strings and/or objects created by JavascriptSerializer.
        /// </summary>
        /// <param name="value">A valid JSON format.</param>
        /// <param name="context">The current Http Context</param>
        /// <remarks>You can validate a JSON string at jsonlint.com</remarks>
        public static void RespondWithJSON(string value, HttpContext context)
        {
            try
            {
                context.Response.ContentType = "application/json";
                context.Response.Write(value);
                context.Response.End();
            }
            catch (ThreadAbortException ex)
            {
                //Do nothing
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void RespondWithJSON(string value, HttpContext context, string html)
        {
            try
            {
                context.Response.Write(value);
                context.Response.End();
            }
            catch (ThreadAbortException ex)
            {
                //Do nothing
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static T Deserialize<T>(string json)
        {
            Newtonsoft.Json.JsonSerializer s = new JsonSerializer();
            return s.Deserialize<T>(new JsonTextReader(new StringReader(json)));
        }
    }
}