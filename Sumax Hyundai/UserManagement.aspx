﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SumaxHeader.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="Sumax_Hyundai.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .p-l-0 {
            padding-left: 0px;
        }

        .add-btn {
            font-size: 25px;
            padding-top: 6px;
        }

        .remove-btn {
            font-size: 25px;
            padding-top: 6px;
            color: brown;
        }

        .required {
            color: #ff0000;
        }

        label {
            text-align: right;
            padding-top: 10px;
        }

        .dropdown-menu > li > a {
            color: black !important;
        }

        .dropdown-menu > .active > a, .dropdown-menu > .active > a:focus, .dropdown-menu > .active > a:hover {
            color: black !important;
        }

        .userdel > i {
            font-size: 16px;
            color: red;
        }

        .modal-dialog {
            width: 720px !important;
        }
    </style>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->

        <ul class="customer-list" id="menuVendors">
            <li class="ripple">
                <a href="UserManagement.aspx">User List&nbsp;&nbsp;&nbsp;<i class="fas fa-user-friends"></i>
                </a>
            </li>
            <li class="ripple">
                <a data-toggle="modal" data-target="#AddUser" href="javascript:void(0)">Add User&nbsp;&nbsp;&nbsp;<i class="fas fa-user-plus"></i>
                </a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
    <section id="main-content">
        <section class="content-wrapper">
            <div id="">


                <section class="panel">
                    <header class="panel-heading">
                        <b><span id="">User List</span></b>
                    </header>
                    <div class="panel-body">
                        <div class="row">
                            <div class="row style-select">
                                <table id="tblUserList" class="table table-striped table-bordered" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Template</th>
                                            <th>Status</th>
                                            <th>Last Login</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%= BindUserList() %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </section>
    </section>

    <div id="AddUser" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div id="errormessage" style="color: red; text-align: center;"></div>

                        <div class="form-group">
                            <label class="control-label col-sm-3 p-l-0" for="firstname">First Name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="firstname" placeholder="Enter Firstname" name="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-3" for="lastname">Last Name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="lastname" placeholder="Enter Lastname" name="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Email <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="email" placeholder="Enter Email" name="" />
                                <%--<small class="form-text text-muted">&emsp</small>--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="userpassword">Password <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="userpassword" placeholder="Enter Password" name="" />
                                <%--<small class="form-text text-muted">&emsp;(Password will send to this email)</small>--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3 p-l-0" for="template">Assign Template <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <asp:DropDownList runat="server" ID="ddlTemplates" CssClass="form-control ddlTemplates">
                                </asp:DropDownList>
                            </div>
                            <%--<div class="col-md-1 p-l-0">
                                <a href="javascript:void(0)" id="AddAnotherTemplate" title="Add Template"><i class="fa fa-plus-circle add-btn" aria-hidden="true"></i></a>
                            </div>--%>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3" style="margin-top: 10px">
                                <a href="javascript:void(0)" onclick="AddUser()" name="cp-submit" id="cp-submit" tabindex="4" class="form-control btn btn-primary">Add</a>
                                <%--<input type="button" name="cp-submit" id="cp-submit" tabindex="4" class="form-control btn btn-success" value="Change" onclick="changePassword()" />--%>
                            </div>
                        </div>
                        <div id="loader" style="display: none">
                            <i class="fa fa-cog fa-spin fa-3x fa-fw" style="font-size: 24px; margin-left: 49%"></i>
                        </div>

                    </div>
                    <%-- end col-md-12 --%>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span runat="server" id="changepasswordclose">Close</span></button>
                </div>
            </div>

        </div>
    </div>


    <div id="Edituser" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit User</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <div id="errormessage1" style="color: red; text-align: center;"></div>

                        <div class="form-group">
                            <label class="control-label col-sm-3 p-l-0" for="firstname">First Name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="Text1" autocomplete="off" placeholder="Enter Firstname" name="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-3" for="lastname">Last Name <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="Text2" autocomplete="off" placeholder="Enter Lastname" name="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3" for="email">Email <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" runat="server" id="Text3" readonly placeholder="Enter Email" name="" />
                                <%--  <small class="form-text text-muted">&emsp;(Password will send to this email)</small>--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-3 p-l-0" for="template">Assign Template <span class="required">*</span></label>
                            <div class="col-sm-8">
                                <asp:DropDownList runat="server" ID="ddlTemplates1" CssClass="form-control ddlEditTemplates">
                                </asp:DropDownList>
                            </div>
                            <%--<div class="col-md-1 p-l-0">
                                <a href="javascript:void(0)" id="AddAnotherTemplate" title="Add Template"><i class="fa fa-plus-circle add-btn" aria-hidden="true"></i></a>
                            </div>--%>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3" style="margin-top: 10px">
                                <%--<a href="javascript:void(0)" name="cp-submit" id="editsub" tabindex="4" class="form-control btn btn-primary editsub">Edit</a>--%>
                                <input type="button" name="cp-submit" id="editsub" tabindex="4" class="form-control btn btn-success editsub" runat="server" value="Edit" />
                                <%--<input type="button" name="cp-submit" id="cp-submit" tabindex="4" class="form-control btn btn-success" value="Change" onclick="changePassword()" />--%>
                            </div>
                        </div>
                        <div id="loader1" style="display: none">
                            <i class="fa fa-cog fa-spin fa-3x fa-fw" style="font-size: 24px; margin-left: 49%"></i>
                        </div>

                    </div>
                    <%-- end col-md-12 --%>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span runat="server" id="Span1">Close</span></button>
                </div>
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#tblUserList').DataTable();

            $(".ddlTemplates").attr("multiple", "multiple").multiselect({
                filterPlaceholder: "Search Template",
                nonSelectedText: "Select Template",
                allSelectedText: "All Templates Selected"
            });


        });

        function DeleteUser(userid) {
            if (userid != undefined || userid == "") {
                $.confirm({
                    draggable: true,
                    dragWindowGap: 100,
                    autoClose: 'Ok|7000',
                    content: function () {
                        var self = this;
                        return $.ajax({
                            url: 'ASHX/AjaxHdlr.ashx',
                            data: "deluserid=" + userid,
                            method: 'get'
                        }).done(function (response) {
                            //if (response.data == "success") {
                            self.setContent("User Deleted Successfully");
                            self.setTitle("");
                            // }
                            // else {
                            //  self.setTitle("");
                            //self.setContentAppend('<div>There was an error in Deleting User. Try again</div>');
                            //}
                        }).fail(function () {
                            self.setTitle("");
                            self.setContentAppend('<div>There was an error in Deleting User. Try again</div>');
                        });
                    },
                    buttons: {
                        Ok: function () {
                            setTimeout(function () {
                                window.location.href = "UserManagement.aspx";
                            }, 300);
                        }
                    }
                });
            }
            else {
                showInfoalert(data1.data, "red", false);

            }

        }

        //$("#AddAnotherTemplate").click(function () {
        //    var $select = $('<select>');
        //    $select.addClass('form-control').addClass('ddlTemplates').html($('.ddlTemplates').html());

        //    var $Innerdiv = $('<div>').addClass("col-sm-8 col-sm-offset-3");
        //    $Innerdiv.append($select);

        //    var $RemoveDiv = $('<div>').addClass('col-md-1 p-l-0');
        //    $RemoveDiv.append('<a href="javascript:void(0)" title="Remove Template" class="removeTemplate"><i class="fa fa-minus-circle remove-btn" aria-hidden="true"></i></a>');

        //    var $Outerdiv = $('<div>').addClass('form-group');
        //    $Outerdiv.append($Innerdiv);
        //    $Outerdiv.append($RemoveDiv);
        //    $(this).parent().parent().append($Outerdiv);

        //})
        //$('body').on('click', '.removeTemplate', function () {
        //    $(this).parent().parent().remove();
        //})

        function AddUser() {
            $("div#errormessage").html("");
            var patt = new RegExp("^[a-zA-Z ]+$");
            var $fname = $('#<%=firstname.ClientID %>')
            var $lname = <%=lastname.ClientID %>;
            var $email = <%=email.ClientID %>;
            var $userpassword = <%=userpassword.ClientID %>;
            var $Template = <%=ddlTemplates.ClientID %>;

            //$('.ddlTemplates').each(function () {


            //})

            var FirstName = $('#<%=firstname.ClientID %>').val();
            var LastName = $('#<%=lastname.ClientID %>').val();
            var email = $('#<%= email.ClientID %>').val();
            var userpassword = $('#<%= userpassword.ClientID %>').val();
            var TemplateId = $('#<%= ddlTemplates.ClientID %>').val();
            var atpos = email.indexOf("@");
            var dotpos = email.lastIndexOf(".");



            if (FirstName == '') {
                $("div#errormessage").html("Please Enter FirstName");
                $($fname).focus();
                return false;
            }
            else if (name.length > 20) {
                $("div#errormessage").html("FirstName shouldn't greater than 20");
                $($fname).focus();
                return false;
            }

            else if (LastName == '') {
                $("div#errormessage").html("Please Enter LastName");
                $($lname).value = '';
                $($lname).focus();
                return false;
            }
            else if (LastName.length > 20) {
                $("div#errormessage").html("LastName shouldn't greater than 20");
                $($lname).focus();
                return false;
            }
            if (email == '') {
                $("div#errormessage").html("Please Enter email");
                $($email).focus();
                return false;
            }
            else if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
                $("div#errormessage").html("Not a valid email address");
                $($email).focus();
                return false;
            }
            else if (userpassword == "") {
                $("div#errormessage").html("Please Enter Password");
                $($userpassword).focus();
                return false;
            }
            else if (userpassword.length < 8) {
                $("div#errormessage").html("LastName shouldn't be less than 8 characters");
                $($userpassword).focus();
                return false;
            }
            else if (TemplateId == undefined || TemplateId == '') {
                $("div#errormessage").html("Please Select Template for user");
                $($Template).focus();
                return false;
            }
            else {
                var data = "FirstName=" + FirstName + "&LastName=" + LastName + "&Email=" + email + "&TemplateId=" + TemplateId + "&UserPassword=" + userpassword
                var pageUrl = 'ASHX/AjaxHdlr.ashx?' + data;
                $("#loader").css("display", "block");
                //return false;
                $.ajax({
                    type: "post",
                    url: pageUrl,
                    //async: false,
                    dataType: "text",
                    success: function (data) {
                        var data1 = JSON.parse(data);
                        if (data1.data == "success")
                            showInfoalert("User added Successfully", "blue", true);
                        else if (data1.data == "exists") {
                            $("#loader").css("display", "none");
                            showInfoalert("User Email already exits. Please enter new email.", "red", false);
                        }
                        else {
                            $("#loader").css("display", "none");
                            showInfoalert(data1.data, "red", false);
                        }

                        //window.location = "/AddUser.aspx";
                    },
                    error: function (x, e) {
                        showInfoalert(e, "red", false);
                    }
                });
            }
            return false;
        }
        function showInfoalert(msg, type, isReload) {
            $.alert({
                title: '',
                type: type,
                content: msg,
                buttons: {
                    Ok: function () {
                        if (isReload)
                            window.location = "UserManagement.aspx";
                    }
                }
            });
        }

        function Edituser(thisbtn) {
            $("div#errormessage1").html("");
            var patt = new RegExp("^[a-zA-Z ]+$");
            var $fname = $('#<%=Text1.ClientID %>')
            var $lname = <%=Text2.ClientID %>;
            var $email = <%=Text3.ClientID %>;
            var $Template = <%=ddlTemplates1.ClientID %>;


            $this = $(thisbtn)
            var firstname = $this.data("firstname");
            var userid = $this.data("userid");
            var lastname = $this.data("lastname");
            var email = $this.data("email");
            var Template = $this.data("templateid");

            Template = Template.toString();

            var $fname = $('#<%= Text1.ClientID %>');
            $fname.val(firstname);
            var $lname = $('#<%= Text2.ClientID %>');
            $lname.val(lastname);
            var $email = $('#<%= Text3.ClientID %>');
            $email.val(email);
            var $Template = $('#<%=ddlTemplates1.ClientID %>');

            $Template.find('option:selected').prop('selected', false)

            if (Template.includes('|')) {
                $.each(Template.split('|'), function (i, e) {
                    $Template.find('option[value="' + e + '"]').prop('selected', true);
                })
            }
            else
                $Template.find('option[value="' + Template + '"]').prop('selected', true);

            $Template.attr("multiple", "multiple").multiselect('refresh');

            $("#editsub").attr("data-userid", userid)
            var FirstName = $('#<%=Text1.ClientID %>').val();
            var LastName = $('#<%=Text2.ClientID %>').val();
            var email = $('#<%= Text3.ClientID %>').val();
            var TemplateId = $('#<%= ddlTemplates.ClientID %>').val();
            return false;
        }
        $('.editsub').click(function () {

            var userid = $this.data("userid");
            var fName = $('#<%=Text1.ClientID %>').val();
            var LName = $('#<%=Text2.ClientID %>').val();
            var email = $('#<%= Text3.ClientID %>').val();
            var TemplateId = $('#<%= ddlTemplates1.ClientID %>').val();


            $.ajax({

                url: 'ASHX/AjaxHdlr.ashx',
                //data:'fName=' + fName  "&LName=" + LName "&email=" + email "&TemplateId=" + TemplateId "&userid=" + userid,
                data: "fName=" + fName + "&LName=" + LName + "&userid=" + userid + "&Templateid=" + TemplateId,

                contentType: "application/json; charset=utf-8",
                datatype: "json",
                method: "get",
                statusCode: {
                    404: function () {
                        alert('Method not found!');
                    }
                },
                success: function (msg) {
                    $.alert('Updated Succesfully');
                    $('#Edituser').modal('hide');
                    window.location = "UserManagement.aspx";


                },
                error: function (msg) {
                    alert('Error!!!')
                }
            });
        });


        function showInfoalert(msg, type, isReload) {
            $.alert({
                title: '',
                type: type,
                content: msg,
                buttons: {
                    Ok: function () {
                        if (isReload)
                            window.location = "UserManagement.aspx";
                    }
                }
            });
        }
    </script>
</asp:Content>
