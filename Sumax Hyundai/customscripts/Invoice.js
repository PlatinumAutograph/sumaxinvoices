﻿function BindmenuVendors() {
    var url = "/ASHX/Ajaxhdlr.ashx";
    var datastring = "vendors=1";
    $.ajax({
        type: "POST",
        url: url,
        data: datastring,
        async: false,
        success: function (msg) {
            if (msg.Data != null) {
                var res = msg.Data;
                var inputSection = $('#menuVendors');
                for (var i = 0; i < res.length; i++) {
                    inputSection.append('<li class="ripple"> <a id="" data-toggle="collapse" data-target="#' + res[i].Id + '"> ' + res[i].VendorName + ' Invoice &nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i> </a> ');
                    inputSection.append('<ul id="' + res[i].Id + '" class="collapse submenu"> <li><a href="#">Add / Remove Field</a> </li> <li> <a href="/RenameFields.aspx">Rename Fields</a></li> </ul> ');
                    inputSection.append('</li>');
                }
            }
            else {

            }
        }
    });
}
function BindmenuRenameVendors() {
    var url = "/ASHX/Ajaxhdlr.ashx";
    var datastring = "vendors=1";
    $.ajax({
        type: "POST",
        url: url,
        data: datastring,
        async: false,
        success: function (msg) {
            if (msg.Data != null) {
                var res = msg.Data;
                var inputSection = $('#menuRenameVendors');
                for (var i = 0; i < res.length; i++) {
                    inputSection.append('<li class="ripple"> <a id="" data-toggle="collapse" data-target="#' + res[i].Id + '"> ' + res[i].VendorName + ' Invoice &nbsp;&nbsp;&nbsp;<i class="fas fa-plus-circle"></i> </a> ');
                    inputSection.append('<ul id="' + res[i].Id + '" class="collapse submenu"> <li><a href="#">Add / Remove Field</a> </li> <li> <a href="/RenameFields.aspx">Rename Fields</a></li> </ul> ');
                    inputSection.append('</li>');
                }
            }
            else {

            }
        }
    });
}
function BindSections() {

    var url = "/ASHX/Ajaxhdlr.ashx";
    var datastring = "sections=1";
    $.ajax({
        type: "POST",
        url: url,
        data: datastring,
        async: false,
        success: function (msg) {
            if (msg.Data != null) {
                var res = msg.Data;
                var inputSection = $('#chooseSection');
                inputSection.append("<option value='-1' > Select Section </option>");
                for (var i = 0; i < res.length; i++) {
                    inputSection.append("<option value='" + res[i].SectionImgUrl + "' data-id='" + res[i].Id + "' >" + res[i].Section + "</option>");
                }
            }
            else {

            }
        }
    });
}



$(function () {

    
});

